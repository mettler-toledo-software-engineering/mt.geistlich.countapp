﻿using System;

namespace mt.geistlich.countapp.peripherals.Usb
{
    public interface IUsbEventProvider
    {
        event Action<string> DeviceInsertedEvent;
        event Action DeviceRemovedEvent;
        void RegisterForUsbEvents();
    }
}