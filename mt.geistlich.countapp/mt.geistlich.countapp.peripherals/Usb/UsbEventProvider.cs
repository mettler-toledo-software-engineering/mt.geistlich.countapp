﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace mt.geistlich.countapp.peripherals.Usb
{
    public class UsbEventProvider : IUsbEventProvider
    {
        public event Action<string> DeviceInsertedEvent;
        public event Action DeviceRemovedEvent;


        public UsbEventProvider()
        {
            RegisterForUsbEvents();
        }
        public  void RegisterForUsbEvents()
        {

            WqlEventQuery insertQuery = new WqlEventQuery("SELECT * FROM __InstanceCreationEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_USBHub'");

            ManagementEventWatcher insertWatcher = new ManagementEventWatcher(insertQuery);
            insertWatcher.EventArrived += DeviceInserted;

            insertWatcher.Start();

            //Win32_USBHub als target reagiert nur auf usb drives
            //Win32_PnPEntity als target reagiert auf alle geräte
            WqlEventQuery removeQuery = new WqlEventQuery("SELECT * FROM __InstanceDeletionEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_USBHub'");
            ManagementEventWatcher removeWatcher = new ManagementEventWatcher(removeQuery);
            removeWatcher.EventArrived += DeviceRemoved;
            removeWatcher.Start();


        }

        private void DeviceRemoved(object sender, EventArrivedEventArgs e)
        {
            DeviceRemovedEvent?.Invoke();
        }

        private void DeviceInserted(object sender, EventArrivedEventArgs e)
        {
            string driveLetter = DriveInfo.GetDrives().Where(d => d.DriveType == DriveType.Removable)?.FirstOrDefault()?.ToString();
            if (Directory.Exists(driveLetter))
            {
                DeviceInsertedEvent?.Invoke(driveLetter);
            }
        }
        
    }
}
