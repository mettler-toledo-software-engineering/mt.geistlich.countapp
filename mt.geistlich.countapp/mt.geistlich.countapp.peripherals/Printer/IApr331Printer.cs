﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.geistlich.countapp.peripherals.Printer
{
    public enum PrintResult
    {
        PrintOk,
        SerialChannelException

    }
    public interface IApr331Printer
    {
        event EventHandler<PrintResult> PrintExecutedEvent;
        ISerialPrinterConfiguration PrinterConfiguration { get; }
        Task<bool> Print(List<string> template);
    }
}
