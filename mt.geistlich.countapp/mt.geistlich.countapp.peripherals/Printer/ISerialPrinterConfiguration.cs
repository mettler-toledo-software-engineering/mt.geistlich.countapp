﻿using MT.Singularity.IO;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;

namespace mt.geistlich.countapp.peripherals.Printer
{
    public interface ISerialPrinterConfiguration
    {
        int CodePage { get; set; }
        string PrintContrast { get; set; }
        int ResistanceClass { get; set; }
        ISerialInterface SerialInterface { get; set; }
        StringSerializer StringSerializer { get; set; }
        int TimeDelayBetweenPrintingLines { get; set; }
        int PrinterPort { get; set; }
    }
}