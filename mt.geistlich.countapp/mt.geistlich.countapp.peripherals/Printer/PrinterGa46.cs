﻿using System;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;

namespace mt.geistlich.countapp.peripherals.Printer
{
    [Export(typeof(IPrinterGa46))]
    [InjectionBehavior(IsSingleton = true)]
    public class PrinterGa46 : IPrinterGa46
    {
        private List<string> _outBuffer = new List<string>();
        private List<string> _tmpBuffer = new List<string>();
        private Timer _timeBetweenLines;
        private ISerialPrinterConfiguration _printerConfiguration;
        private const string Esc = "\x1b";
        private const string StartWidePrint = "\x0e";
        private const string StopWidePrint = "\x0f";
        private const string NormalHeight = "\x1bH2";
        private const string BigHeight = "\x1bH3";
        private bool _canOpenChannel;
        private const int TotalLabelWidth = 24;

        public PrinterGa46(ISerialPrinterConfiguration printerConfiguration)
        {
            _printerConfiguration = printerConfiguration;
            SetupTimer();
            _outBuffer.Clear();


        }

        private void OnPrinterSetupError(Task setupTask)
        {
            if (setupTask.Exception != null)
            {
                Log4NetManager.ApplicationLogger.ErrorEx(setupTask.Exception);
            }
        }

        private void SetupTimer()
        {
            _timeBetweenLines = new Timer();
            _timeBetweenLines.Interval = _printerConfiguration.TimeDelayBetweenPrintingLines;
            _timeBetweenLines.Elapsed += timeBetweenLines_Tick;
            _timeBetweenLines.Enabled = true;
            _timeBetweenLines.AutoReset = true;
            _timeBetweenLines.Start();
        }

        public async Task SetupPrinter()
        {

            if (_printerConfiguration.SerialInterface != null && _printerConfiguration.StringSerializer != null)
            {

                _canOpenChannel = true;
                try
                {
                    await _printerConfiguration.StringSerializer.OpenAsync();
                }
                catch (Exception e)
                {
                    await _printerConfiguration.StringSerializer.CloseAsync();
                    Log4NetManager.ApplicationLogger.ErrorEx(e);

                }
            }

        }

        public void Dispose()
        {
            _timeBetweenLines.Enabled = false;
            _timeBetweenLines.Dispose();
        }
        private void timeBetweenLines_Tick(object sender, EventArgs e)
        {

            if (_outBuffer.Count > 0 && _printerConfiguration.StringSerializer != null && _printerConfiguration.StringSerializer.IsOpen)
            {

                PrintToSerialChannel(_outBuffer[0]);
                
            }

            //if (_outBuffer.Count > 0 && _printerConfiguration.StringSerializer != null && _printerConfiguration.StringSerializer.IsOpen == false)
            //{
            //    _printerConfiguration.StringSerializer.OpenAsync();
            //}

            //if (_outBuffer.Count == 0 && _printerConfiguration.StringSerializer != null && _printerConfiguration.StringSerializer.IsOpen)
            //{
            //    _printerConfiguration.StringSerializer.Close();
            //}
        }

        private void PrintToSerialChannel(string dataline)
        {
            try
            {
                _printerConfiguration.StringSerializer.WriteAsync(dataline).ContinueWith(task =>
                {
                    _outBuffer.RemoveAt(0);
                } );

            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.ErrorEx(ex);
            }
        }

        string ConvertASCII(string s)
        {
            s = s.Replace("ä", "{");
            s = s.Replace("ö", "|");
            s = s.Replace("ü", "}");
            return s;
        }

        public void WritePrinterSettings()
        {
            _tmpBuffer.Add($"{Esc}{_printerConfiguration.PrintContrast}{Esc}^{_printerConfiguration.CodePage}");
        }



        public void PrintLn(string text)
        {
            _tmpBuffer.Add(text.PadRight(TotalLabelWidth));
        }
        public void PrintLnRight(string text)
        {
            _tmpBuffer.Add($"{text.PadLeft(TotalLabelWidth)}");
        }
        public void PrintLnWideMarginRight(string text)
        {
            _tmpBuffer.Add($"{StartWidePrint}{text.PadLeft(TotalLabelWidth)}{StopWidePrint}");
        }
        public void PrintLnWide(string text)
        {
            _tmpBuffer.Add($"{StartWidePrint}{text.PadRight(TotalLabelWidth)}{StopWidePrint}");
        }
        public void PrintLnWideRight(string text)
        {
            _tmpBuffer.Add($"{StartWidePrint}{text.PadLeft(TotalLabelWidth)}{StopWidePrint}");
        }
        public void PrintLnWide(string text, bool doubleHeight)
        {
            if (doubleHeight)
                _tmpBuffer.Add($"{StartWidePrint}{BigHeight}{text.PadRight(TotalLabelWidth)}{StopWidePrint}{NormalHeight}");
            else
                PrintLnWide(text);
        }
        public void PrintLnWideRight(string text, bool doubleHeight)
        {
            if (doubleHeight)
                _tmpBuffer.Add($"{StartWidePrint}{BigHeight}{text.PadLeft(TotalLabelWidth)}{StopWidePrint}{NormalHeight}");
            else
                PrintLnWide(text);
        }
        public void Print(string text)
        {
            _tmpBuffer.Add(text);
        }
        public void PrintWide(string text)
        {
            _tmpBuffer.Add($"{StartWidePrint}{text}{StopWidePrint}");
        }
        public void PrintWide(string text, bool doubleWidth)
        {
            if (doubleWidth)
                _tmpBuffer.Add($"{StartWidePrint}{BigHeight}{text}{StopWidePrint}{NormalHeight}");
            else
                PrintWide(text);
        }
        public void PrintEmptyLine()
        {
            _tmpBuffer.Add("");
        }

        public string CombineTitleAndValue(string title, string value)
        {
            int titleLength = title.Length;
            string output = $"{title}{value.PadLeft(TotalLabelWidth - titleLength)}";

            return output;
        }

        public void ExecutePrint()
        {
            _outBuffer = new List<string>(_tmpBuffer);
            _tmpBuffer.Clear();
        }
    }
}
