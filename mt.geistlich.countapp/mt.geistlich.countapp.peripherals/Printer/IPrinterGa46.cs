﻿using System.Threading.Tasks;

namespace mt.geistlich.countapp.peripherals.Printer
{
    public interface IPrinterGa46
    {
        Task SetupPrinter();
        void Dispose();
        void Print(string text);
        void PrintEmptyLine();
        void PrintLn(string text);
        void PrintLnRight(string text);
        void PrintLnWideRight(string text);
        void PrintLnWide(string text);
        void PrintLnWide(string text, bool doubleHeight);
        void PrintLnWideRight(string text, bool doubleHeight);
        void PrintLnWideMarginRight(string text);
        void PrintWide(string text);
        void PrintWide(string text, bool doubleWidth);
        void WritePrinterSettings();
        string CombineTitleAndValue(string title, string value);
        void ExecutePrint();
    }
}