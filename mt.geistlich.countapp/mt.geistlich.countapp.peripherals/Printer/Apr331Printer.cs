﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Serialization;

namespace mt.geistlich.countapp.peripherals.Printer
{
    [Export(typeof(IApr331Printer))]
    public class Apr331Printer : IApr331Printer
    {

        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(Apr331Printer);
        public ISerialPrinterConfiguration  PrinterConfiguration { get; }

        public event EventHandler<PrintResult> PrintExecutedEvent;


        public Apr331Printer(ISerialPrinterConfiguration printerConfiguration)
        {
            PrinterConfiguration = printerConfiguration;

        }

        public async Task<bool> Print(List<string> template)
        {

            try
            {
                

                await PrinterConfiguration.StringSerializer.OpenAsync();

                await WriteBufferToChannel(template);

                await PrinterConfiguration.StringSerializer.CloseAsync();
                PrintExecutedEvent?.Invoke(this, PrintResult.PrintOk);

                return true;
            }
            catch (Exception e)
            {
                Logger.ErrorEx("printing exception", SourceClass, e);
                PrintExecutedEvent?.Invoke(this, PrintResult.SerialChannelException);
                return false;
            }
        }



        private async Task WriteBufferToChannel(List<string> template)
        {
            foreach (string line in template)
            {
                await PrinterConfiguration.StringSerializer.WriteAsync($"{line}");
                Thread.Sleep(PrinterConfiguration.TimeDelayBetweenPrintingLines);
            }
        }
    }
}
