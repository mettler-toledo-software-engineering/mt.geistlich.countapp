﻿using System;

using System.Collections.Generic;
using System.Text;

namespace mt.geistlich.countapp.application.Models
{
    public class Message
    {

        public Messages MessageText { get; set; }
        public MessageColors MessageColor { get; set; }

        public bool WithWeightWindow { get; set; }

        public Message(Messages messageText, MessageColors messageColor, bool withWeightWindow)
        {
            MessageText = messageText;
            MessageColor = messageColor;
            WithWeightWindow = withWeightWindow;
        }

        private Message(){}

        public static Message DefaultMessage()
        {
            var message = new Message
            {
                MessageText = Messages.ScaleReady,
                MessageColor = MessageColors.LightBlue,
                WithWeightWindow =  false
            };

            return message;
        }

    }
}
