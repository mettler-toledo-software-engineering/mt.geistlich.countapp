﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.geistlich.countapp.application.Models
{
    public class Order : DomainObject
    {
        public string Charge { get; set; }

        public Recipe Recipe { get; set; }

        public double MinWeight { get; set; }
        public double MaxWeight { get; set; }
        public double AverageWeight { get; set; }
        public double StandardDeviation { get; set; }
        public DateTime OrderStartTime { get; set; }
        public int MeasurementsInTolerance { get; set; }

        public Order Clone()
        {
            var order = new Order();
            order.Id = Id;
            order.Charge = Charge;
            order.Recipe = Recipe.Clone();

            return order;
        }

    }
}
