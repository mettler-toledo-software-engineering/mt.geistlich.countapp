﻿namespace mt.geistlich.countapp.application.Models
{
    public interface IUser
    {
        string UserName { get; set; }
    }
}