﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.Singularity.Localization;

namespace mt.geistlich.countapp.application.Models
{
    public enum MessageColors
    {
        Red,
        Yellow,
        Green,
        LightBlue
    }

    public enum Messages
    {
        
        ScaleReady,
        ScaleWaiting,
        NextMeasurementIn,
        MeasurementDone,
        OverTolerance,
        UnderTolerance,
        PdfSaved,
        PrintFinished,
        ProcessFinished,
        ErrorPrint,
        ErrorPdf,
        ErrorMeasurement,
        PleaseWait
    }

    public enum ProcessSteps
    {
        //scale waits for motion 
        ScaleReady,
        //motion has been detected and now waits for silence so it can start countdown
        ScaleWaitingForStabilisation,
        //2 sek countdown runs, but can be resetted by motion
        ScaleCountingDown,
        Measuring,
        //countdown finished and measurement is done -> multiple measurement results possible, afterwards return to scale ready
        MeasurementDone,
        //all timers are stopped, no new items can be measured
        ProcessFinished
        
    }
}
