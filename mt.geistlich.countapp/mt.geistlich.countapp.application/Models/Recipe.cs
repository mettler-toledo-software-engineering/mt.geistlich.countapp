﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.geistlich.countapp.application.Models
{
    public class Recipe : DomainObject
    {
        public string RecipeNumber { get; set; }
        public string RecipeName { get; set; }
        public double UpperTolerance { get; set; }
        public double LowerTolerance { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastModified { get; set; }
        public string LastModifiedFormatted => $"{LastModified:dd.MM.yyyy HH:mm}";
        public string RecipeRange => $"{LowerTolerance}mg - {UpperTolerance}mg";
        public string RecipeNameOnButton => RecipeName.Replace(" ", "\n");


        public Recipe Clone()
        {
            var recipe = new Recipe();
            recipe.Id = Id;
            recipe.RecipeNumber = RecipeNumber;
            recipe.RecipeName = RecipeName;
            recipe.UpperTolerance = UpperTolerance;
            recipe.LowerTolerance = LowerTolerance;
            recipe.DateCreated = DateCreated;
            recipe.LastModified = LastModified;
            
            return recipe;
        }
    }
}
