﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.geistlich.countapp.application.Models
{
    public class Measurement : DomainObject
    {
        public Order Order { get; set; }
        public double NetWeight { get; set; }
        public DateTime TimeStamp { get; set; }

        public Measurement Clone()
        {
            var measurement = new Measurement();
            measurement.Id = Id;
            measurement.Order = Order.Clone();
            measurement.NetWeight = NetWeight;
            measurement.TimeStamp = TimeStamp;

            return measurement;
        }
    }
}
