﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.geistlich.countapp.application.Models
{
    public class OperatorUser : IUser
    {
        public string UserName { get; set; }
    }
}
