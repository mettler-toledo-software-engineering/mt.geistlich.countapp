﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.geistlich.countapp.application.Services.DataExport
{
    public class PdfExportService : IPdfExportService
    {
        private readonly string _pdfSourceFolder;
        private const string Exported = "exported";
        private string _targetDirectory;
        public PdfExportService(string pdfSourceFolder)
        {
            _pdfSourceFolder = pdfSourceFolder;
        }
        /// <summary>
        /// only exports pdf filest to an attached usb drive, which have not been exported before
        /// </summary>
        /// <returns>a list of filenames that have been sucessfully exported </returns>
        public async Task<int> ExportPdfsToUsbDrive()
        {

            var usbPresent = CheckForUsbDrive();

            if (!usbPresent)
            {
                throw new IOException("Kein Usb Stick gefunden.");
            }

            var files = await FindUnExportedPdfFiles();

            var copiedFiles = await CopyFilesToUsbDrive(files);

            var count = await RenameExportedFiles(copiedFiles);

            return count;
        }
        public async Task<int> ExportPdfToUsbDrive(string pdf)
        {

            var usbPresent = CheckForUsbDrive();

            if (!usbPresent)
            {
                throw new IOException("Kein Usb Stick gefunden.");
            }

            var pdfs = new List<string> { pdf };
            var copiedFiles = await CopyFilesToUsbDrive(pdfs);

            var count = await RenameExportedFiles(copiedFiles);

            return count;
        }

        private bool CheckForUsbDrive()
        {
            bool result = true;
            string usbdrive = DriveInfo.GetDrives().Where(d => d.DriveType == DriveType.Removable)?.FirstOrDefault()?.ToString() ?? string.Empty;

            if (string.IsNullOrEmpty(usbdrive))
            {
                return false;
            }

            _targetDirectory = Path.Combine(usbdrive, $"PDFExport_{DateTime.Now:yyyyMMdd}");

            if (Directory.Exists(_targetDirectory) == false)
            {
                result = CreateDirectory(_targetDirectory);
            }

            return result;
        }

        private async Task<List<string>> FindUnExportedPdfFiles()
        {
            List<string> files = await Task.Run(() => { return Directory.EnumerateFiles(_pdfSourceFolder, "*.pdf").Select(Path.GetFileName).Where(p => p.StartsWith(Exported) == false).ToList();}) ;

            return files;
        }

        private async Task<List<string>> CopyFilesToUsbDrive(List<string> files)
        {
            var outputList = new List<string>();

            outputList = await Task.Run(() =>
            {

                foreach (var file in files)
                {
                    string sourcefile = Path.Combine(_pdfSourceFolder, file);
                    string destinationfile = Path.Combine(_targetDirectory, file);
                    File.Copy(sourcefile, destinationfile, true);
                    outputList.Add(file);
                }
                return outputList;
            });


            return outputList;
        }


        private async Task<int> RenameExportedFiles(List<string> files)
        {
            int counter = 0;
            counter = await Task.Run(() =>
            {
                foreach (var file in files)
                {
                    string oldfilename = Path.Combine(_pdfSourceFolder, file);
                    string newFilename = Path.Combine(_pdfSourceFolder, $"{Exported}_{file}");
                    File.Move(oldfilename, newFilename);
                    counter++;
                }

                return counter;
            });
            
            return counter;
        }

        private bool CreateDirectory(string path)
        {
            bool result = false;
            try
            {
                Directory.CreateDirectory(path);
                result = true;
            }
            catch (IOException)
            {
                result = false;
            }

            return result;
        }

    }
}
