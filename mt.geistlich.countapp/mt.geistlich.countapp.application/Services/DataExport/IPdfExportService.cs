﻿using System.Threading.Tasks;

namespace mt.geistlich.countapp.application.Services.DataExport
{

    public interface IPdfExportService
    {
        Task<int> ExportPdfsToUsbDrive();
        Task<int> ExportPdfToUsbDrive(string pdf);
    }
}