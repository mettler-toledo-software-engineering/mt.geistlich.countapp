﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace mt.geistlich.countapp.application.Services.HouseKeeping
{
    public class PdfHouseKeepingService :IPdfHousekeepingService
    {
        private readonly string _pdfSourceFolder;
        private const string Exported = "exported";
        private const int OneHour = 3600000;
        private readonly int _months;
        private readonly Timer _houseKeepingTimer = new Timer(OneHour);

        public PdfHouseKeepingService(string pdfSourceFolder, int months)
        {
            _pdfSourceFolder = pdfSourceFolder;
            _months = months * -1;
            _houseKeepingTimer.AutoReset = true;
            _houseKeepingTimer.Enabled = true;
            _houseKeepingTimer.Start();

            _houseKeepingTimer.Elapsed += HouseKeepingTimerOnElapsed;
        }

        private async void HouseKeepingTimerOnElapsed(object sender, ElapsedEventArgs e)
        {
            var files = await FindExportedPdfFiles();

            await DeleteFiles(files);
        }


        private Task<IEnumerable<string>> FindExportedPdfFiles()
        {
            //get list with all entries older then 2 years
            IEnumerable<string> files = Directory.EnumerateFiles(_pdfSourceFolder, "*.pdf").Where(p => p.StartsWith(Exported) == true && File.GetCreationTime(p) < DateTime.Now.AddMonths(_months)).Select(Path.GetFileName);

            return Task.FromResult(files);
        }

        private Task DeleteFiles(IEnumerable<string> files)
        {
            foreach (var file in files)
            {
                File.Delete(Path.Combine(_pdfSourceFolder, file));
            }

            return Task.CompletedTask;
        }
    }
}
