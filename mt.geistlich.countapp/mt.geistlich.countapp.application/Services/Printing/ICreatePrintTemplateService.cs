﻿using System.Collections.Generic;
using mt.geistlich.countapp.application.Models;

namespace mt.geistlich.countapp.application.Services.Printing
{
    public interface ICreatePrintTemplateService
    {
        List<string> CreateBodyTemplate(Order order);
        List<string> CreateHeaderTemplate(Order order);
        List<string> CreateReprint(Order order);
        List<string> GetPdfTemplate(bool isReprint,Order order);
    }
}