﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iText.IO.Font;
using iText.Kernel.Font;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Pdfa;

namespace mt.geistlich.countapp.application.Services.Printing
{
    public class PdfTemplate : ICreatePdfTemplateService
    {
        private string _pdfFolder;
        private string _intent;

        private List<string> _linesToPrint;

        private const string Esc = "\x1b";
        private const string StartWidePrint = "\x0e";
        private const string StopWidePrint = "\x0f";
        private const string NormalHeight = "\x1bH2";
        private const string BigHeight = "\x1bH3";
        private const string Configuration = "K6^6";

        public PdfTemplate(string pdfSourceFolder)
        {
            _pdfFolder = pdfSourceFolder;
            _intent = $@"{pdfSourceFolder}\sRGB_CS_profile.icm";
        }

        public async Task<string> CreatePdfDocument(List<string> linesToPrint, string charge)
        {
            string result = await Task.Run(() =>
            {
                _linesToPrint = new List<string>(linesToPrint);

                string pdf = $"{charge}_{DateTime.Now:yyyyMMddHHmmss}.pdf";

                string pdffile = Path.Combine(_pdfFolder, pdf);
                var pdfadocument = new PdfADocument(new PdfWriter(pdffile), PdfAConformanceLevel.PDF_A_1B, new PdfOutputIntent("Custom", "", "https://www.color.org", "sRGB IEC61966-2.1", new FileStream(_intent, FileMode.Open, FileAccess.Read
                )));

                var document = new Document(pdfadocument);

                pdfadocument.SetTagged();
                //Fonts need to be embedded
                PdfFont font = PdfFontFactory.CreateFont(@"c:\windows\fonts\cour.ttf", PdfFontFactory.EmbeddingStrategy.PREFER_EMBEDDED);
                Style normalStyle = new Style();
                normalStyle.SetHeight(10);
                Paragraph paragraph = new Paragraph();
                normalStyle.SetFont(font).SetFontSize(10);
                foreach (var line in _linesToPrint)
                {

                    paragraph.Add(new Text($"{line}\n").AddStyle(normalStyle));
                }

                document.Add(paragraph);
                //Set alt text


                document.Close();
                return pdf;
            });


            return result;

        }

    }
}
