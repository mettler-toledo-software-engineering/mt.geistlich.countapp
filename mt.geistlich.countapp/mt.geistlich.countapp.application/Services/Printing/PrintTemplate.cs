﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;

namespace mt.geistlich.countapp.application.Services.Printing
{
    public class PrintTemplate : ICreatePrintTemplateService
    {
        private const string Height2 = "\x1B\x48" + "2";
        private const string Height3 = "\x1B\x48" + "3";
        private const string DoubleWidth = "\x0E";
        private const string SingleWidth = "\x0F";

        private const int MaxLine = 48;
        private const string DateFormat = "dd.MM.yy";
        private const string TimeFormat = "HH:mm:ss";
        private const string WeightFormat = "0.000 g";
        private List<string> _headertemplate = new List<string>();
        private List<string> _bodytemplate = new List<string>();

        private List<string> _reprinttemplate = new List<string>();
        private List<string> _pdftemplate = new List<string>();
        private int _printcounter = 0;

        public List<string> CreateHeaderTemplate(Order order)
        {
            _headertemplate.Clear();
            SetHeader(false, order);
            return _headertemplate;
        }

        public List<string> CreateBodyTemplate(Order order)
        {
            _printcounter = 0;
            _bodytemplate.Clear();
            SetBody(order);
            return _bodytemplate;
        }

        public List<string> GetPdfTemplate(bool isReprint, Order order)
        {
            _pdftemplate.Clear();
            _headertemplate.Clear();
            _bodytemplate.Clear();
            SetHeader(isReprint, order);
            SetBody(order);
            return _pdftemplate;
        }

        public List<string> CreateReprint(Order order)
        {
            _printcounter++;
            _headertemplate.Clear();
            _reprinttemplate.Clear();
            _bodytemplate.Clear();
            _pdftemplate.Clear();

            SetHeader(true,order);
            SetBody(order);

            _reprinttemplate.AddRange(_headertemplate);
            _reprinttemplate.AddRange(_bodytemplate);

            return _reprinttemplate;
        }


        private void SetHeader(bool isReprint, Order order)
        {

            AddLine(Height3, string.Empty, _headertemplate);
            AddLine(DoubleWidth , "     Waegeprotokoll     ", _headertemplate);
            AddLine(SingleWidth + Height2, string.Empty, _headertemplate);
            if (isReprint)
            {
                AddLine_Center(string.Empty, $"{_printcounter}. Reprint", _headertemplate);
            }
                
            PrintCharLine( '*', _headertemplate);
            AddLine(string.Empty, " ", _headertemplate);
            AddLine_LeftRight(string.Empty, "DATUM", order.OrderStartTime.ToString(DateFormat), _headertemplate);
            AddLine_LeftRight(string.Empty, "ZEIT", order.OrderStartTime.ToString(TimeFormat), _headertemplate);
            AddLine(string.Empty, " ", _headertemplate);
            AddLine(string.Empty, order.Recipe.RecipeName, _headertemplate);
            AddLine(string.Empty, order.Recipe.RecipeNumber, _headertemplate);
            AddLine_LeftRight(string.Empty,"Chargennummer: ", order.Charge, _headertemplate);
            AddLine_LeftRight(string.Empty,"Akzeptanzkriterium min.:", $"{order.Recipe.LowerTolerance}mg", _headertemplate);
            AddLine_LeftRight(string.Empty, "Akzeptanzkriterium max.:", $"{order.Recipe.UpperTolerance}mg", _headertemplate);
            AddLine(string.Empty, " ", _headertemplate);
            PrintCharLine( '-', _headertemplate);
            AddLine(string.Empty," ", _headertemplate);
            AddLine(string.Empty," ", _headertemplate);
            AddLine(string.Empty," ", _headertemplate);
            AddLine(string.Empty, " ", _headertemplate);
        }

        private void SetBody(Order order)
        {
            AddLine(Height3, string.Empty, _bodytemplate);
            AddLine_Center(DoubleWidth, "STATISTIK", _bodytemplate);
            AddLine(SingleWidth + Height2, string.Empty, _bodytemplate);
            PrintCharLine('-', _bodytemplate);
            AddLine(string.Empty, " ", _bodytemplate);
            AddLine_LeftRight(string.Empty,"Mittelwert Gewicht:", $"{order.AverageWeight:F}mg", _bodytemplate);
            AddLine_LeftRight(string.Empty, "Standardabweichung:", $"{order.StandardDeviation:F}", _bodytemplate);
            AddLine_LeftRight(string.Empty, "Min. Wert Gewicht:", $"{order.MinWeight:F}mg", _bodytemplate);
            AddLine_LeftRight(string.Empty, "Max. Wert Gewicht:", $"{order.MaxWeight:F}mg", _bodytemplate);
            AddLine(string.Empty, " ", _bodytemplate);
            PrintCharLine('-', _bodytemplate);
            AddLine(string.Empty, " ", _bodytemplate);
            AddLine_LeftRight(string.Empty, "Anzahl Messungen:", $"{order.MeasurementsInTolerance} Stk", _bodytemplate);
            AddLine(string.Empty, " ", _bodytemplate);
            PrintCharLine('*', _bodytemplate);
            AddLine(string.Empty, " ", _bodytemplate);
            AddLine(string.Empty, " ", _bodytemplate);
            AddLine(string.Empty," ", _bodytemplate);
            AddLine(string.Empty, " ", _bodytemplate);
            AddLine(string.Empty, " ", _bodytemplate);
        }

        private void PrintCharLine(char c, List<string> template)
        {
            template.Add("".PadRight(MaxLine, c));
            _pdftemplate.Add("".PadRight(MaxLine, c));
        }

        private void AddLine_LeftRight(string formatting, string textL, string textR, List<string> template)
        {
            int padding = MaxLine - textL.Length - textR.Length;
            template.Add(textL + "".PadRight(padding, ' ') + textR);
            _pdftemplate.Add(textL + "".PadRight(padding, ' ') + textR);
        }

        private void AddLine_Center(string formatting, string textC, List<string> template)
        {
            int padding = (MaxLine - textC.Length) / 2;
            string filling = "".PadRight(padding, ' ');
            string pdffilling = "".PadRight(padding, ' ');
            template.Add(filling + textC + filling);
            _pdftemplate.Add(pdffilling + textC + pdffilling);
        }

        private void AddLine(string formatting, string item, List<string> template)
        {
            template.Add(formatting+item);
            _pdftemplate.Add(item);
        }
    }
}
