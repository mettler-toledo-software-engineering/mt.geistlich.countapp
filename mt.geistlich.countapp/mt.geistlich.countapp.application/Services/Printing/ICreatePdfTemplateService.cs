﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace mt.geistlich.countapp.application.Services.Printing
{
    public interface ICreatePdfTemplateService
    {
        Task<string> CreatePdfDocument(List<string> linesToPrint, string charge);
    }
}