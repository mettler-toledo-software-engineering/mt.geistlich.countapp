﻿using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;

namespace mt.geistlich.countapp.application.Services.weighing
{
    public interface ICalculateOrderStatistikService
    {
        Task<Order> CalculateStatistik(Order order);
    }
}