﻿using System;
using mt.geistlich.countapp.application.Models;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.geistlich.countapp.application.Services.weighing
{
    public interface IWeighingProcessService
    {
        WeightInformation Weight{ get; set; }

        event Action<double> CountDownChanged;
        event Action<WeightInformation> DoMeasurement;
        event Action<Message> StateChanged;

        void FinishProcess();
        void StartProcess();
        void MeasurementDone();
    }
}