﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.DataAccess;

namespace mt.geistlich.countapp.application.Services.weighing
{
    public class CalculateOrderStatistikService : ICalculateOrderStatistikService
    {

        private readonly IMeasurementDataAccess _measurementDataAccess;
        public CalculateOrderStatistikService(IMeasurementDataAccess measurementDataAccess)
        {
            _measurementDataAccess = measurementDataAccess;
        }

        public async Task<Order> CalculateStatistik(Order order)
        {
            var output = order;
            var enumerable = await _measurementDataAccess.GetAllMeasurementsByOrder(order.Id);

            var measurements = enumerable.ToList();
            output.AverageWeight = measurements.Average(m => m.NetWeight);

            double variance = 0;


            double temp = measurements.Sum(m => Math.Pow((m.NetWeight - output.AverageWeight), 2));

            variance = temp / (measurements.Count);

            double standardDeviation = Math.Sqrt(variance);

            output.StandardDeviation = standardDeviation;

            return output;
        }
    }
}
