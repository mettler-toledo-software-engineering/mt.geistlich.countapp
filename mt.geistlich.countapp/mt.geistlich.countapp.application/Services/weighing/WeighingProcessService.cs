﻿using System;
using System.Timers;
using mt.geistlich.countapp.application.Models;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;


namespace mt.geistlich.countapp.application.Services.weighing
{
    [InjectionBehavior(IsSingleton = true)]
    public class WeighingProcessService : IWeighingProcessService
    {
        private readonly double _stableWeightDuration;
        private readonly Timer _waitingForMotionTimer = new Timer();
        private readonly Timer _waitingForStabilisationTimer = new Timer();
        private readonly Timer _resultTimer = new Timer();

        private double _countDown;
        public event Action<double> CountDownChanged;
        public event Action<WeightInformation> DoMeasurement;
        public event Action<Message> StateChanged;
        public WeightInformation Weight { get; set; }

        private ProcessSteps _processStep;
        private bool _isStable => Weight != null && Weight.NetStable;

        public WeighingProcessService(double stableWeightDuration, double measureTextResultDuration)
        {
            _stableWeightDuration = stableWeightDuration;

            _waitingForMotionTimer.AutoReset = true;
            _waitingForMotionTimer.Interval = 100;
            _waitingForMotionTimer.Elapsed += CheckMotion;

            _waitingForStabilisationTimer.AutoReset = false;
            _waitingForStabilisationTimer.Interval = _stableWeightDuration * 1000;
            _waitingForStabilisationTimer.Elapsed += WaitingForStabilisation;

            _resultTimer.AutoReset = false;
            _resultTimer.Interval = measureTextResultDuration * 1000;
            _resultTimer.Elapsed += ResultHasBeenShownLongEnough;
        }

        private void ResultHasBeenShownLongEnough(object sender, ElapsedEventArgs e)
        {
            _processStep = ProcessSteps.MeasurementDone;
            _countDown = _stableWeightDuration;
        }

        public void StartProcess()
        {
            _processStep = ProcessSteps.ScaleReady;
            _waitingForMotionTimer.Start();
            
        }

        private void CheckMotion(object sender, ElapsedEventArgs e)
        {
            if (_isStable == false && _processStep == ProcessSteps.ScaleReady)
            {
                _processStep = ProcessSteps.ScaleWaitingForStabilisation;
                _countDown = _stableWeightDuration;
                StateChanged?.Invoke(new Message(Messages.ScaleWaiting, MessageColors.LightBlue, false));
                _waitingForStabilisationTimer.Stop();
            }

            if (_isStable == true && _processStep == ProcessSteps.ScaleWaitingForStabilisation)
            {
                _processStep = ProcessSteps.ScaleCountingDown;
                StateChanged?.Invoke(new Message(Messages.NextMeasurementIn, MessageColors.LightBlue, false));
                _waitingForStabilisationTimer.Start();
            }

            if (_isStable == true && _processStep == ProcessSteps.ScaleCountingDown)
            {
                _countDown = (_countDown * 1000 - 100) / 1000;
                CountDownChanged?.Invoke(_countDown);
            }

            if (_isStable == false && _processStep == ProcessSteps.ScaleCountingDown)
            {
                _countDown = _stableWeightDuration;
                CountDownChanged?.Invoke(_stableWeightDuration);
                _processStep = ProcessSteps.ScaleWaitingForStabilisation;
                StateChanged?.Invoke(new Message(Messages.ScaleWaiting, MessageColors.LightBlue, false));
                _waitingForStabilisationTimer.Stop();
            }

            if (_isStable == true && _processStep == ProcessSteps.MeasurementDone)
            {
                _processStep = ProcessSteps.ScaleReady;
                StateChanged?.Invoke(new Message(Messages.ScaleReady, MessageColors.LightBlue, true));

            }
        }

        private void WaitingForStabilisation(object sender, ElapsedEventArgs e)
        {
            DoMeasurement?.Invoke(Weight);
            _processStep = ProcessSteps.Measuring;
        }

        public void MeasurementDone()
        {
            _resultTimer.Start();
        }
        
        public void FinishProcess()
        {
            _processStep = ProcessSteps.ProcessFinished;
            _waitingForMotionTimer.Stop();
        }
    }
}
