﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;

namespace mt.geistlich.countapp.application.Services
{
    public static class RecipeValidation
    {
        public static bool IsVaild(this Recipe recipe)
        {
            if (string.IsNullOrWhiteSpace(recipe.RecipeName) == false && string.IsNullOrWhiteSpace(recipe.RecipeNumber) == false && recipe.UpperTolerance > recipe.LowerTolerance)
            {
                return true;
            }

            return false;
        }
    }
}
