﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;

namespace mt.geistlich.countapp.application.Services.DataAccess
{
    public interface IRecipeDataAccess
    {
        Task<IEnumerable<Recipe>> GetAllRecipes();
        Task<bool> UpdateRecipe(Recipe recipe);
        Task<bool> DeleteRecipe(int recipeId);
        Task<Recipe> CreateRecipe(Recipe recipe);
    }
}
