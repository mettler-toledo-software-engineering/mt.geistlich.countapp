﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;

namespace mt.geistlich.countapp.application.Services.DataAccess
{
    public interface IOrderDataAccess
    {
        Task<IEnumerable<Order>> GetAllOrders();
        Task<bool> UpdateOrder(Order order);
        Task<bool> DeleteOrder(int order);
        Task<bool> DeleteAllOrders();
        Task<Order> CreateOrder(Order order);
    } 
}
