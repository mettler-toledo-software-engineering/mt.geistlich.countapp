﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;

namespace mt.geistlich.countapp.application.Services.DataAccess
{
    public interface IMeasurementDataAccess
    {
        Task<IEnumerable<Measurement>> GetAllMeasurements();
        Task<IEnumerable<Measurement>> GetAllMeasurementsByOrder(int orderId);

        Task<bool> UpdateMeasurement(Measurement measurement);
        Task<bool> DeleteMeasurement(int measurementId);
        Task<Measurement> CreateMeasurement(Measurement measurement);

        Task<bool> DeleteAllMeasureMents();
        Task<bool> DeleteAllMeasurementsByOrder(int orderId);


    }
}
