﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.application.Services.Authentification
{
    [InjectionBehavior(IsSingleton = true)]
    public class AutoLogoutService : IAutoLogoutService
    {
        private const int CheckIntervall = 10000;
        private readonly double _activityTimeInMinutes;
        private DateTime _lastActivity;

        private readonly Timer _logOutTimer = new Timer(CheckIntervall);


        public AutoLogoutService(int activityTimeInMinutes)
        {
            _activityTimeInMinutes = activityTimeInMinutes * -1;
            _logOutTimer.AutoReset = true;
            _logOutTimer.Elapsed += CheckIfUserNeedsToLogOff;
            
        }

        private void CheckIfUserNeedsToLogOff(object sender, ElapsedEventArgs e)
        {
            var checktime = DateTime.Now.AddMinutes(_activityTimeInMinutes);
            if (_lastActivity < checktime)
            {
                LogOutUser();
            }
        }

        public void UserLoggedIn()
        {
            _lastActivity = DateTime.Now;
            _logOutTimer.Start();
        }

        public event EventHandler LogOutEvent;

        private void LogOutUser()
        {
            LogOutEvent?.Invoke(this, EventArgs.Empty);
            _logOutTimer.Stop();
        }

        public void ResetLogOffTimer()
        {
            _lastActivity = DateTime.Now;
        }

        public void StopLogoffTimer()
        {
            _logOutTimer.Stop();
        }

        public void RestartLogoffTimer()
        {
            _lastActivity = DateTime.Now;
            _logOutTimer.Start();
        }
    }
}
