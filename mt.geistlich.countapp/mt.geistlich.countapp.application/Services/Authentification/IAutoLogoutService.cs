﻿using System;

namespace mt.geistlich.countapp.application.Services.Authentification
{
    public interface IAutoLogoutService
    {
        event EventHandler LogOutEvent;
        void UserLoggedIn();
        void ResetLogOffTimer();
        void RestartLogoffTimer();
        void StopLogoffTimer();
    }
}