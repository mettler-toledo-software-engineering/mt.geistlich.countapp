﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace mt.geistlich.countapp.application.Services.AudioPlayBack
{
    public class AudioPlayBackService : IAudioPlayBackService
    {
        private const string PathToSound = @"C:\temp\ding.wav";
        private SoundPlayer _player = new SoundPlayer(PathToSound);

        public void PlaySound()
        {
            _player.Play();
        }

    }

    
}
