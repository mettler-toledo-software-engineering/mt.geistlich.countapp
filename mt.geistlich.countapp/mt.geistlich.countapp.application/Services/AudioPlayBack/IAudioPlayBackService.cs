﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mt.geistlich.countapp.application.Services.AudioPlayBack
{
    public interface IAudioPlayBackService
    {
        void PlaySound();
    }
}
