﻿namespace mt.geistlich.countapp.application.Services.SystemUpdate
{
    public interface ISystemUpdateService
    {
        bool ExecuteUpdate();
    }
}