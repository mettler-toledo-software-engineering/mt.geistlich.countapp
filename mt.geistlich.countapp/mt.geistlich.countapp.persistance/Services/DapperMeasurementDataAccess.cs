﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.DataAccess;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.persistance.Services
{
    [InjectionBehavior(IsSingleton = true)]
    public class DapperMeasurementDataAccess : IMeasurementDataAccess
    {

        private const string SELECTALL_MEASUREMENT_SQL = @"SELECT m.*, o.*, r.* FROM Measurements m, Orders o, Recipes r where o.Id=m.Order_Id and r.Id = o.Recipe_id";
        private const string SELECTALL_MEASUREMENT_BYORDERID_SQL = @"SELECT m.*, o.*, r.* FROM Measurements m, Orders o, Recipes r where o.Id=m.Order_Id and r.Id = o.Recipe_id and Order_id=@OrderId";
        private const string DELETE_MEASUREMENT_SQL = @"DELETE FROM Measurements WHERE Id = @Id";
        private const string DELETEALL_MEASUREMENT_SQL = @"DELETE FROM Measurements";
        private const string DELETEALL_MEASUREMENT_BYORDERID_SQL = @"DELETE FROM Measurements WHERE Order_id = @OrderId";
        private const string INSERT_MEASUREMENT_SQL = @"INSERT INTO 
                                                Measurements (Order_id, NetWeight, TimeStamp)
                                                VALUES (@OrderId, @NetWeight, @TimeStamp);
                                                select last_insert_rowid();";

        private const string UPDATE_MEASUREMENT_SQL = @"UPDATE Measurements SET Order_id = @OrderId,
                                                                      NetWeight = @NetWeight,  
                                                                      TimeStamp = @TimeStamp  
                                                                      WHERE Id = @Id";

        private readonly IDbConnectionFactory _connectionFactory;

        public DapperMeasurementDataAccess(IDbConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public async Task<Measurement> CreateMeasurement(Measurement measurement)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {
                var parameters = new DynamicParameters();
                //parameters.Add("@Id", 0, DbType.Int32, ParameterDirection.Output);
                parameters.Add("@OrderId", measurement.Order?.Id);
                parameters.Add("@NetWeight", measurement.NetWeight);
                parameters.Add("@TimeStamp", measurement.TimeStamp);


                var id = await database.ExecuteScalarAsync<int>(INSERT_MEASUREMENT_SQL, parameters);

                measurement.Id = id;

                return measurement;

            }
        }

        public async Task<bool> DeleteAllMeasureMents()
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {


                var rowsaffected = await database.ExecuteAsync(DELETEALL_MEASUREMENT_SQL);

                return rowsaffected > 0;
            }
        }

        public async Task<bool> DeleteAllMeasurementsByOrder(int orderId)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@OrderId", orderId);

                var rowsaffected = await database.ExecuteAsync(DELETEALL_MEASUREMENT_BYORDERID_SQL, parameters);

                return rowsaffected > 0;
            }
        }

        public async Task<bool> DeleteMeasurement(int measurementId)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@Id", measurementId);


                var rowsaffected = await database.ExecuteAsync(DELETE_MEASUREMENT_SQL, parameters);

                return rowsaffected == 1;
            }
        }

        public async Task<IEnumerable<Measurement>> GetAllMeasurements()
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {
                var result = await database.QueryAsync<Measurement, Order, Recipe, Measurement>(SELECTALL_MEASUREMENT_SQL,
                    (measurement, order, recipe) => { order.Recipe = recipe; measurement.Order = order; return measurement; });

                return result;
            }
        }

        public async Task<IEnumerable<Measurement>> GetAllMeasurementsByOrder(int orderId)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@OrderId", orderId);


                var result = await database.QueryAsync<Measurement, Order, Recipe, Measurement>(SELECTALL_MEASUREMENT_BYORDERID_SQL,
                    (measurement, order, recipe) => { order.Recipe = recipe; measurement.Order = order; return measurement; }, parameters);

                return result;
            }
        }

        public async Task<bool> UpdateMeasurement(Measurement measurement)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@Id", measurement.Id);
                parameters.Add("@NetWeight", measurement.NetWeight);
                parameters.Add("@TimeStamp", measurement.TimeStamp);
                parameters.Add("@OrderId", measurement.Order?.Id);


                var rowsaffected = await database.ExecuteAsync(UPDATE_MEASUREMENT_SQL, parameters);

                return rowsaffected == 1;

            }
        }
    }
}
