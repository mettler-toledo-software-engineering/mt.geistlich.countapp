﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.DataAccess;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.persistance.Services
{
    [InjectionBehavior(IsSingleton = true)]
    public class DapperRecipeDataAccess : IRecipeDataAccess
    {
        private const string SELECT_RECIPES_SQL = @"SELECT * FROM Recipes";
        private const string Delete_RECIPE_SQL = @"DELETE FROM Recipes WHERE Id = @Id";
        private const string INSERT_RECIPE_SQL = @"INSERT INTO 
                                                Recipes (RecipeNumber, RecipeName, UpperTolerance, LowerTolerance, DateCreated, LastModified)
                                                VALUES (@RecipeNumber, @RecipeName, @UpperTolerance, @LowerTolerance, @DateCreated, @LastModified);
select last_insert_rowid();";

        private const string UPDATE_RECIPE_SQL = @"UPDATE Recipes SET RecipeNumber = @RecipeNumber,
                                                                      RecipeName = @RecipeName,  
                                                                      UpperTolerance = @UpperTolerance,  
                                                                      LowerTolerance = @LowerTolerance,  
                                                                      DateCreated = @DateCreated,  
                                                                      LastModified = @LastModified
                                                                      WHERE Id = @Id";

        private readonly IDbConnectionFactory _connectionFactory;

        public DapperRecipeDataAccess(IDbConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public async Task<Recipe> CreateRecipe(Recipe recipe)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@RecipeNumber", recipe.RecipeNumber);
                parameters.Add("@RecipeName", recipe.RecipeName);
                parameters.Add("@UpperTolerance", recipe.UpperTolerance);
                parameters.Add("@LowerTolerance", recipe.LowerTolerance);
                parameters.Add("@DateCreated", recipe.DateCreated);
                parameters.Add("@LastModified", recipe.LastModified);

                var id = await database.ExecuteScalarAsync<int>(INSERT_RECIPE_SQL, parameters);
                recipe.Id = id;
                return recipe;

            }
        }

        public async Task<bool> DeleteRecipe(int recipeId)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@Id", recipeId);

                var rowsaffected = await database.ExecuteAsync(Delete_RECIPE_SQL, parameters);

                return rowsaffected == 1;

            }
        }

        public async Task<IEnumerable<Recipe>> GetAllRecipes()
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {
                var result = await database.QueryAsync<Recipe>(SELECT_RECIPES_SQL);

                return result;
            }
        }

        public async Task<bool> UpdateRecipe(Recipe recipe)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", recipe.Id);
                parameters.Add("@RecipeNumber", recipe.RecipeNumber);
                parameters.Add("@RecipeName", recipe.RecipeName);
                parameters.Add("@UpperTolerance", recipe.UpperTolerance);
                parameters.Add("@LowerTolerance", recipe.LowerTolerance);
                parameters.Add("@DateCreated", recipe.DateCreated);
                parameters.Add("@LastModified", recipe.LastModified);


                var rowsaffected = await database.ExecuteAsync(UPDATE_RECIPE_SQL, parameters);

                return rowsaffected == 1;

            }
        }
    }
}
