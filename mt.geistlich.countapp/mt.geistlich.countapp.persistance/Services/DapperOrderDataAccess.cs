﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.DataAccess;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.persistance.Services
{
    [InjectionBehavior(IsSingleton = true)]
    public class DapperOrderDataAccess : IOrderDataAccess
    {

        private const string SELECT_ORDERS_SQL = @"SELECT o.*, r.* FROM Orders o left join Recipes r on o.Recipe_id = r.Id";
        private const string Delete_ORDER_SQL = @"DELETE FROM Orders WHERE Id = @Id";
        private const string Delete_ALL_ORDER_SQL = @"DELETE FROM Orders";
        private const string INSERT_ORDER_SQL = @"INSERT INTO 
                                                Orders (Charge, Recipe_id, MinWeight, MaxWeight, MeasurementsInTolerance, OrderStartTime)
                                                VALUES (@Charge, @RecipeId, @MinWeight, @MaxWeight, @MeasurementsInTolerance, @OrderStartTime);
                                                select last_insert_rowid();";

        private const string UPDATE_ORDER_SQL = @"UPDATE Orders SET Charge = @Charge,
                                                                      Recipe_id = @RecipeId,
                                                                      MinWeight = @MinWeight,  
                                                                      MaxWeight = @MaxWeight,  
                                                                      MeasurementsInTolerance = @MeasurementsInTolerance,  
                                                                      OrderStartTime = @OrderStartTime  
                                                                      WHERE Id = @Id";

        private readonly IDbConnectionFactory _connectionFactory;

        public DapperOrderDataAccess(IDbConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public async Task<Order> CreateOrder(Order order)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Charge", order.Charge);
                parameters.Add("@RecipeId", order.Recipe.Id);
                parameters.Add("@MinWeight", order.MinWeight);
                parameters.Add("@MaxWeight", order.MaxWeight);
                parameters.Add("@MeasurementsInTolerance", order.MeasurementsInTolerance);
                parameters.Add("@OrderStartTime", order.OrderStartTime);
                
                var id = await database.ExecuteScalarAsync<int>(INSERT_ORDER_SQL, parameters);

                order.Id = id;
                return order;

            }
        }

        public async Task<bool> DeleteOrder(int orderId)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {
                var parameters = new DynamicParameters();
                parameters.Add("@Id", orderId);


                var rowsaffected = await database.ExecuteAsync(Delete_ORDER_SQL, parameters);

                return rowsaffected == 1;

            }
        }

        public async Task<bool> DeleteAllOrders()
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {
                
                var rowsaffected = await database.ExecuteAsync(Delete_ALL_ORDER_SQL);

                return rowsaffected >= 1;

            }
        }

        public async Task<IEnumerable<Order>> GetAllOrders()
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {
                var result = await database.QueryAsync<Order, Recipe, Order>(SELECT_ORDERS_SQL,
                    (order, recipe) => { order.Recipe = recipe; return order; } );

                return result;
            }
        }

        public async Task<bool> UpdateOrder(Order order)
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {

                var parameters = new DynamicParameters();
                parameters.Add("@Id", order.Id);
                parameters.Add("@Charge", order.Charge);
                parameters.Add("@RecipeId", order.Recipe.Id);
                parameters.Add("@MinWeight", order.MinWeight);
                parameters.Add("@MaxWeight", order.MaxWeight);
                parameters.Add("@MeasurementsInTolerance", order.MeasurementsInTolerance);
                parameters.Add("@OrderStartTime", order.OrderStartTime);


                var rowsaffected = await database.ExecuteAsync(UPDATE_ORDER_SQL, parameters);

                return rowsaffected == 1;

            }
        }
    }
}
