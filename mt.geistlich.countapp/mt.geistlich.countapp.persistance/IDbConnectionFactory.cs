﻿using System.Data;

namespace mt.geistlich.countapp.persistance
{
    public interface IDbConnectionFactory
    {
        IDbConnection Connect();
    }
}