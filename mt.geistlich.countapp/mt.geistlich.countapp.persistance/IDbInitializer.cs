﻿namespace mt.geistlich.countapp.persistance
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}