﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.persistance
{
    [InjectionBehavior(IsSingleton = true)]
    public class SqliteDbInitializer : IDbInitializer
    {
        private const string CREATE_RECIPES_TABLE_SQL = @"CREATE TABLE IF NOT EXISTS Recipes(
                                Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                                RecipeNumber TEXT,
                                RecipeName TEXT,
                                UpperTolerance NUMERIC NOT NULL DEFAULT 0,
                                LowerTolerance NUMERIC NOT NULL DEFAULT 0,
                                DateCreated TEXT,
                                LastModified TEXT
                                )";
        private const string CREATE_ORDERS_TABLE_SQL = @"CREATE TABLE IF NOT EXISTS Orders(
                                Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                                Charge	TEXT,
                                Recipe_id	INTEGER NOT NULL,
                                MinWeight	NUMERIC NOT NULL DEFAULT 0,
                                MaxWeight	NUMERIC NOT NULL DEFAULT 0,
                                MeasurementsInTolerance	INTEGER NOT NULL DEFAULT 0,
                                OrderStartTime	TEXT,
                                FOREIGN KEY(Recipe_id) REFERENCES Recipes(Id)
                                )";
        private const string CREATE_MEASUREMENTS_TABLE_SQL = @"CREATE TABLE IF NOT EXISTS Measurements(
                                	Id	INTEGER PRIMARY KEY AUTOINCREMENT,
                                    Order_id	INTEGER NOT NULL,
                                    NetWeight	NUMERIC NOT NULL DEFAULT 0,
                                    TimeStamp	Text,
                                    FOREIGN KEY(Order_id) REFERENCES Orders(Id)
                                )";
        private readonly IDbConnectionFactory _connectionFactory;

        public SqliteDbInitializer(IDbConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }
        public void Initialize()
        {
            using (IDbConnection database = _connectionFactory.Connect())
            {
                database.Execute(CREATE_RECIPES_TABLE_SQL);
                database.Execute(CREATE_ORDERS_TABLE_SQL);
                database.Execute(CREATE_MEASUREMENTS_TABLE_SQL);
            }
        }
    }
}
