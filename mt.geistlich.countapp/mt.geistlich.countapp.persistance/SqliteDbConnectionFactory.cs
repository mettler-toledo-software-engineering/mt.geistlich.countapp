﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.persistance
{
    [InjectionBehavior(IsSingleton = true)]
    public class SqliteDbConnectionFactory : IDbConnectionFactory
    {
        private string _connectionString;

        public SqliteDbConnectionFactory(string connectionString)
        {
            _connectionString = connectionString;
        }
        public IDbConnection Connect()
        {
            return new SQLiteConnection(_connectionString);
        }
    }
}
