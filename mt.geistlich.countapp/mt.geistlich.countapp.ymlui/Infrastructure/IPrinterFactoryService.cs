﻿using System;
using System.Threading.Tasks;
using mt.geistlich.countapp.peripherals.Printer;

namespace mt.geistlich.countapp.ymlui.Infrastructure
{
    public interface IPrinterFactoryService
    {
        event Action<IApr331Printer> PrinterChanged;
        Task<IApr331Printer> CreateApr331Printer();


    }
}