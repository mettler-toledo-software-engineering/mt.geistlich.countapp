﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.geistlich.countapp.application.Services.Authentification;
using mt.geistlich.countapp.ymlui.Configuration;
using mt.geistlich.countapp.ymlui.State;

namespace mt.geistlich.countapp.ymlui.Infrastructure
{
    public class AutoLogoutServiceFactory : IAutoLogoutServiceFactory
    {
        private readonly ConfigurationStore _configurationStore;
        public event Action<IAutoLogoutService> LogoutServiceChanged;
        private int _logoutTime;
        public AutoLogoutServiceFactory(ConfigurationStore configurationStore)
        {
            _configurationStore = configurationStore;
            _logoutTime = _configurationStore.CurrentConfiguration.LogoutTime;
            _configurationStore.ConfigurationChanged += ConfigurationStoreOnConfigurationChanged;
        }

        private void ConfigurationStoreOnConfigurationChanged(GeistlichConfiguration configuration)
        {
            if (configuration.LogoutTime != _logoutTime)
            {
                _logoutTime = configuration.LogoutTime;
                var logoutService = CreateAutoLogoutService();
                LogoutServiceChanged?.Invoke(logoutService);
            }
        }

        public IAutoLogoutService CreateAutoLogoutService()
        {
            IAutoLogoutService logoutService = new AutoLogoutService(_logoutTime);

            return logoutService;
        }
    }
}
