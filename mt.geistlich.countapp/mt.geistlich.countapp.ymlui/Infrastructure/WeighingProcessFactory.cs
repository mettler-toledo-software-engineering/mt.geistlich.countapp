﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.geistlich.countapp.application.Services.weighing;
using mt.geistlich.countapp.ymlui.Configuration;
using mt.geistlich.countapp.ymlui.State;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.ymlui.Infrastructure
{
    [InjectionBehavior(IsSingleton = true)]
    public class WeighingProcessFactory : IWeighingProcessFactory
    {
        private readonly ConfigurationStore _configurationStore;
        private double _stableWeightTime;
        private double _textduration;
        public event Action<IWeighingProcessService> WeighingProcessChanged;
        public WeighingProcessFactory(ConfigurationStore configurationStore)
        {
            _configurationStore = configurationStore;
            _stableWeightTime = _configurationStore.CurrentConfiguration.StableWeightTime;
            _textduration = _configurationStore.CurrentConfiguration.MessageDuration;
            _configurationStore.ConfigurationChanged += ConfigurationStoreOnConfigurationChanged;

            WeighingProcessService = CreateWeighingProcessService();
        }

        private void ConfigurationStoreOnConfigurationChanged(GeistlichConfiguration configuration)
        {
            if (configuration.StableWeightTime.Equals(_stableWeightTime) || configuration.MessageDuration.Equals(_textduration))
            {
                _stableWeightTime = configuration.StableWeightTime;
                _textduration = configuration.MessageDuration;
                WeighingProcessService = CreateWeighingProcessService();
                WeighingProcessChanged?.Invoke(WeighingProcessService);
            }
        }

        private IWeighingProcessService CreateWeighingProcessService()
        {
            return new WeighingProcessService(_stableWeightTime, _textduration);
        }

        public IWeighingProcessService WeighingProcessService { get; private set; }

    }
}
