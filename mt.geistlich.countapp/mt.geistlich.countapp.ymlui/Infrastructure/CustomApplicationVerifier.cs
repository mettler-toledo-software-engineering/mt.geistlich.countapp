﻿using MT.Singularity.Platform.Infrastructure;

namespace mt.geistlich.countapp.ymlui.Infrastructure
{
    /// <summary>
    /// Implementation of <see cref="ICustomApplicationVerifier"/> for the custom application.
    /// Gives the possibility of protecting your own application by custom application password with dongle.
    /// </summary>
    // TODO: Uncomment the following line if a password is needed to activate this custom application.
    //[Export(typeof(ICustomApplicationVerifier))]
    public class CustomApplicationVerifier : ICustomApplicationVerifier
    {
        /// <summary>
        /// Verifies the custom application password for the implementing application.
        /// </summary>
        /// <param name="password">The password to verify.</param>
        /// <returns>
        /// <c>true</c> if the custom application password is valid for this custom application
        /// and the application can be run. Otherwise, <c>false</c>.
        /// </returns>
        public bool VerifyCustomApplicationPassword(string password)
        {
            // This method will be called when you start your own application,
            // It will read the custom application password from Singularity Dongle provided with the terminal.
            // This custom application password is written by VerifyCustomApplicationPasswordWriteCode method right here.
            // Put your validation algorithm, and return value to indicate the if you allow to run your application.
            // if you return false, you will see such information "Custom application in not unlocked by the dongle." in your home screen.
            // For example: you could use following code

            // return (password == "MySpecialPassword");

            // if you don't need to protect your application by the custom application password in the dongle, you could use following code;
            return true;
        }

        /// <summary>
        /// Verifies the custom application password write code and will return the password to write.
        /// </summary>
        /// <param name="code">The code to verify.</param>
        /// <returns>
        /// The password to write on the dongle or <c>null</c> if the code was invalid.
        /// </returns>
        public string VerifyCustomApplicationPasswordWriteCode(string code)
        {
            // This method will be called when you go to Setup->Application->Custom Application Password page and click the run button to write
            // the password, the parameter code is what you input in the password text box, you could use any algorithm to get the real password
            // you want to write into the dongle for protection perspective. 
            // Take care the return value should not be longer than 20 bytes. if it is longer than 20 bytes, the exceeding parts will be cut.
            // There is the simplest method, just return the code directly without any transforming.

            return code;
        }
    }
}
