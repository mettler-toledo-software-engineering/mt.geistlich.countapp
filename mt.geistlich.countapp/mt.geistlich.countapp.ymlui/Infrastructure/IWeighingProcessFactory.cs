﻿using System;
using mt.geistlich.countapp.application.Services.weighing;

namespace mt.geistlich.countapp.ymlui.Infrastructure
{
    public interface IWeighingProcessFactory
    {
        IWeighingProcessService WeighingProcessService { get; }

        event Action<IWeighingProcessService> WeighingProcessChanged;
    }
}