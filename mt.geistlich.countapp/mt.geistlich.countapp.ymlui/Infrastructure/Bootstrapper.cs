﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using mt.geistlich.countapp.application.Services.AudioPlayBack;
using mt.geistlich.countapp.application.Services.Authentification;
using mt.geistlich.countapp.application.Services.DataAccess;
using mt.geistlich.countapp.application.Services.DataExport;
using mt.geistlich.countapp.application.Services.HouseKeeping;
using mt.geistlich.countapp.application.Services.Printing;
using mt.geistlich.countapp.application.Services.SystemUpdate;
using mt.geistlich.countapp.application.Services.weighing;
using mt.geistlich.countapp.peripherals.Printer;
using mt.geistlich.countapp.peripherals.Usb;
using mt.geistlich.countapp.persistance;
using mt.geistlich.countapp.persistance.Services;
using mt.geistlich.countapp.ymlui.Configuration;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using mt.geistlich.countapp.ymlui.Views;
// ReSharper disable once RedundantUsingDirective
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Platform.UserManagement;

namespace mt.geistlich.countapp.ymlui.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(Bootstrapper);
        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }

        protected override async void InitializeApplication()
        {
            await InitializeCustomerService();
            base.InitializeApplication();
        }

        private async Task InitializeCustomerService()
        {
            try
            {
                CompositionContainer.EnableRecursiveResolution();
                //configuration
                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();
                var geistlichComponent = new GeistlichComponents(configurationStore, securityService, CompositionContainer);
                CompositionContainer.AddInstance<IGeistlichComponents>(geistlichComponent);
                //database
                string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["sqlite"].ConnectionString;
                CompositionContainer.AddInstance<IDbConnectionFactory>(new SqliteDbConnectionFactory(connectionstring));
                CompositionContainer.AddInstance<IDbInitializer>(new SqliteDbInitializer(CompositionContainer.Resolve<IDbConnectionFactory>()));
                
                IDbInitializer dbInitializer = CompositionContainer.Resolve<IDbInitializer>();
                dbInitializer.Initialize();
                CompositionContainer.AddInstance<IRecipeDataAccess>(new DapperRecipeDataAccess(CompositionContainer.Resolve<IDbConnectionFactory>()));
                CompositionContainer.AddInstance<IOrderDataAccess>(new DapperOrderDataAccess(CompositionContainer.Resolve<IDbConnectionFactory>()));
                CompositionContainer.AddInstance<IMeasurementDataAccess>(new DapperMeasurementDataAccess(CompositionContainer.Resolve<IDbConnectionFactory>()));

                GeistlichConfiguration configuration = geistlichComponent.GetConfigurationAsync().Result;
                State.ConfigurationStore myConfigurationStore = new State.ConfigurationStore(configuration);

                //services
                CompositionContainer.AddInstance<IPrinterFactoryService>(new PrinterFactory(engine, myConfigurationStore));
                //CompositionContainer.AddInstance<IAutoLogoutService>(new AutoLogoutService(myConfigurationStore.CurrentConfiguration.LogoutTime));
                //CompositionContainer.AddInstance<IWeighingProcessService>(new WeighingProcessService(myConfigurationStore.CurrentConfiguration.StableWeightTime, myConfigurationStore.CurrentConfiguration.MessageDuration));
                CompositionContainer.AddInstance<IWeighingProcessFactory>(new WeighingProcessFactory(myConfigurationStore));
                CompositionContainer.AddInstance<IAutoLogoutServiceFactory>(new AutoLogoutServiceFactory(myConfigurationStore));
                CompositionContainer.AddInstance<ICreatePrintTemplateService>(new PrintTemplate());
                CompositionContainer.AddInstance<IPdfExportService>(new PdfExportService(Globals.PDFSourceDirectory));
                CompositionContainer.AddInstance<ISystemUpdateService>(new SystemUpdateService());
                CompositionContainer.AddInstance<IUsbEventProvider>(new UsbEventProvider());

                CompositionContainer.AddInstance<ICreatePdfTemplateService>(new PdfTemplate(Globals.PDFSourceDirectory));
                CompositionContainer.AddInstance<IPdfHousekeepingService>(new PdfHouseKeepingService(Globals.PDFSourceDirectory, Globals.PdfHouseKeepingMonths));
                CompositionContainer.AddInstance<ICalculateOrderStatistikService>(new CalculateOrderStatistikService(CompositionContainer.Resolve<IMeasurementDataAccess>()));
                CompositionContainer.AddInstance<IAudioPlayBackService>(new AudioPlayBackService());

                //stores
                CompositionContainer.AddInstance(new OrderStore(CompositionContainer.Resolve<IOrderDataAccess>(), CompositionContainer.Resolve<ICalculateOrderStatistikService>()));
                CompositionContainer.AddInstance(new PrinterStore(
                    myConfigurationStore,
                    CompositionContainer.Resolve<IPrinterFactoryService>(), 
                    CompositionContainer.Resolve<ICreatePrintTemplateService>(),
                    CompositionContainer.Resolve<ICreatePdfTemplateService>(),
                    CompositionContainer.Resolve<IPdfExportService>()
                    ));
                CompositionContainer.AddInstance(new MeasurementStore(CompositionContainer.Resolve<IMeasurementDataAccess>()));
                CompositionContainer.AddInstance(new RecipeStore(CompositionContainer.Resolve<IRecipeDataAccess>()));
                CompositionContainer.AddInstance(new UserStore(CompositionContainer.Resolve<IAutoLogoutServiceFactory>(), CompositionContainer.Resolve<ISecurityService>(), myConfigurationStore));
                CompositionContainer.AddInstance(new MessageStore());
                CompositionContainer.AddInstance(new WeighingProcessStore(
                    CompositionContainer.Resolve<MessageStore>(),
                    CompositionContainer.Resolve<OrderStore>(),
                    CompositionContainer.Resolve<MeasurementStore>(),
                    CompositionContainer.Resolve<IScaleService>(),
                    CompositionContainer.Resolve<IWeighingProcessFactory>(),
                    CompositionContainer.Resolve<IAudioPlayBackService>(),
                    myConfigurationStore
                    ));

                //viewmodels
                CompositionContainer.AddInstance(new HomeScreenViewModel(
                    CompositionContainer.Resolve<UserStore>(), 
                    CompositionContainer.Resolve<OrderStore>(),
                    CompositionContainer.Resolve<IPdfExportService>(),
                    CompositionContainer.Resolve<IUsbEventProvider>()
                    ));
                CompositionContainer.AddInstance(new WeightButtonsViewModel(CompositionContainer.Resolve<IScaleService>()));
                CompositionContainer.AddFactory(() => new ManageRecipesViewModel(CompositionContainer.Resolve<RecipeStore>()));
                CompositionContainer.AddFactory(() => new PickRecipeViewModel(CompositionContainer.Resolve<RecipeStore>(), CompositionContainer.Resolve<OrderStore>()));
                CompositionContainer.AddFactory(() => new WeighingProcessViewModel(
                    CompositionContainer.Resolve<OrderStore>(), 
                    CompositionContainer.Resolve<MeasurementStore>(), 
                    CompositionContainer.Resolve<MessageStore>(), 
                    CompositionContainer.Resolve<WeighingProcessStore>(),
                    CompositionContainer.Resolve<PrinterStore>()
                    ));
                CompositionContainer.AddInstance(new MessageWindowViewModel(CompositionContainer.Resolve<MessageStore>()));
                CompositionContainer.AddInstance(new WeightWindowViewModel(CompositionContainer.Resolve<MessageStore>(), CompositionContainer.Resolve<IScaleService>()));

                //controls
                CompositionContainer.AddInstance(new WeightButtonsControl(CompositionContainer.Resolve<WeightButtonsViewModel>()));
                CompositionContainer.AddInstance(new WeightWindowControl(CompositionContainer.Resolve<WeightWindowViewModel>()));
                CompositionContainer.AddInstance(new MessageWindowControl(CompositionContainer.Resolve<MessageWindowViewModel>()));

                //views
                CompositionContainer.AddFactory(() => new ManageRecipesView(CompositionContainer.Resolve<ManageRecipesViewModel>()));
                CompositionContainer.AddFactory(() => new PickRecipeView(CompositionContainer.Resolve<PickRecipeViewModel>()));
                CompositionContainer.AddFactory(() => new WeighingProcessView(
                    CompositionContainer.Resolve<WeighingProcessViewModel>(), 
                    CompositionContainer.Resolve<WeightWindowControl>(), 
                    CompositionContainer.Resolve<WeightButtonsControl>(), 
                    CompositionContainer.Resolve<MessageWindowControl>()
                    ));
                

                //Preparation
                var orderData = CompositionContainer.Resolve<OrderStore>();
                await orderData.LoadData();
                //var usbevents = CompositionContainer.Resolve<IUsbEventProvider>();
                //usbevents.RegisterForUsbEvents();
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
        }
    }
}
