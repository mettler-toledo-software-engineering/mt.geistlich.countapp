﻿using mt.geistlich.countapp.ymlui.State;
using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Platform.Infrastructure;
using log4net;
using mt.geistlich.countapp.peripherals.Printer;
using mt.geistlich.countapp.ymlui.Configuration;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;


namespace mt.geistlich.countapp.ymlui.Infrastructure
{
    public class PrinterFactory : IPrinterFactoryService
    {
        private readonly ConfigurationStore _configurationStore;
        private readonly IPlatformEngine _platformEngine;
        public event Action<IApr331Printer> PrinterChanged;
        private IApr331Printer _currentPrinter;

        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(PrinterFactory);

        public PrinterFactory(IPlatformEngine platformEngine, ConfigurationStore configurationStore)
        {
            _platformEngine = platformEngine;
            _configurationStore = configurationStore;
            _configurationStore.ConfigurationChanged += ConfigurationStoreOnConfigurationChanged;
            CreateApr331Printer().ContinueWith(HandleException);
        }

        private void ConfigurationStoreOnConfigurationChanged(GeistlichConfiguration configuration)
        {
            if (_currentPrinter?.PrinterConfiguration.PrintContrast != configuration.Apr331Kontrast ||
                _currentPrinter?.PrinterConfiguration.ResistanceClass != configuration.Apr331Widerstand ||
                _currentPrinter?.PrinterConfiguration.PrinterPort != configuration.PrinterPort)
            {
                CreateApr331Printer().ContinueWith(HandleException);
            }
        }
        private void HandleException(Task<IApr331Printer> obj)
        {
            if (obj.Exception != null)
            {
                Logger.ErrorEx("Printer could not be initialized", SourceClass, obj.Exception);
                return;
            }

            _currentPrinter = obj.Result;
            PrinterChanged?.Invoke(_currentPrinter);
        }

        public async Task<IApr331Printer> CreateApr331Printer()
        {
            var interfaceservice = await _platformEngine.GetInterfaceServiceAsync();
            var allinterfaces = await interfaceservice.GetAllInterfacesAsync();
            var allserialinterfaces = allinterfaces.OfType<ISerialInterface>().ToList();
            var serialInterface = allserialinterfaces.FirstOrDefault(serial => serial.LogicalPort == _configurationStore.CurrentConfiguration.PrinterPort);

            IConnectionChannel<DataSegment> serialConnectionChannel = await serialInterface.CreateConnectionChannelAsync();
            var delimiterSerializer = new DelimiterSerializer(serialConnectionChannel, CommonDataSegments.Newline);
            var serializer = new StringSerializer(delimiterSerializer);
            serializer.CodePage = CodePage.CP1250;
            var printerConfiguration = new SerialPrinterConfiguration();
            printerConfiguration.StringSerializer = serializer;
            printerConfiguration.PrintContrast = _configurationStore.CurrentConfiguration.Apr331Kontrast;
            printerConfiguration.ResistanceClass = _configurationStore.CurrentConfiguration.Apr331Widerstand;
            printerConfiguration.PrinterPort = _configurationStore.CurrentConfiguration.PrinterPort;

            Apr331Printer printer = new Apr331Printer(printerConfiguration);

            return printer;
        }
    }
}
