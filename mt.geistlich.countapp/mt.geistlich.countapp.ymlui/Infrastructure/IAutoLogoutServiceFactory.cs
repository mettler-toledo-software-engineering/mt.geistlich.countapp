﻿using System;
using mt.geistlich.countapp.application.Services.Authentification;

namespace mt.geistlich.countapp.ymlui.Infrastructure
{
    public interface IAutoLogoutServiceFactory
    {
        event Action<IAutoLogoutService> LogoutServiceChanged;

        IAutoLogoutService CreateAutoLogoutService();
    }
}