﻿using System;

using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using MT.Singularity.Platform;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Drawing;

namespace mt.geistlich.countapp.ymlui.Infrastructure
{
    public static class Globals
    {
        public static readonly SingularityEnvironment Environment = new SingularityEnvironment("mt.geistlich.countapp.ymlui");

        public static string ProjectNumber = "P21110102";
        public static int PdfHouseKeepingMonths;
        public static string ProgVersionStr(bool withRevision)
        {
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            string vers = "V" + version.Major.ToString() + "." + version.Minor.ToString() + "." + version.Build.ToString();
            if (withRevision)
            {
                vers += "." + version.Revision.ToString();
#if DEBUG
                vers += " [DEBUG]";
#endif
            }

            return vers;
        }
        public static string ProgVersionStr()
        {
            Version version = Assembly.GetExecutingAssembly().GetName().Version;
            string vers = "V" + version.Major + "." + version.Minor+ "." + version.Build;


            return vers;
        }

        public static string PDFSourceDirectory = @"C:\PDFBackup";

        public static readonly SolidColorBrush LightGray = Colors.LightGrayBrush;
        public static readonly Color Red = Colors.Red;
        public static readonly Color Green = Colors.MediumGreen;
        public static readonly Color Yellow = Colors.Yellow;
        //"#FF00386B" rgba(0, 56, 107, 1)
        public static Color DarkBlue = new Color(1, 0, 56, 107);
        public static readonly SolidColorBrush DarkBlueBrush = new SolidColorBrush(DarkBlue);
        public static readonly Color White = Colors.White;

        public static Color LightBlue = new Color(1, 248,252,248);

        

        public static readonly SolidColorBrush RedBrush = new SolidColorBrush(Red);
        public static readonly SolidColorBrush YellowBrush = new SolidColorBrush(Yellow);
        public static readonly SolidColorBrush GreenBrush = new SolidColorBrush(Green);
        public static readonly SolidColorBrush LightBlueBrush = new SolidColorBrush(LightBlue);
        //Color.TryParse("FFF8FCF8", out LightBlue) FF = 1, F8 = 248, FC = 252, F8 = 248 http://www.hex2rgba.devoth.com/

        public static readonly int TextBlockHeight = 50;
        public static readonly int TextBlockWidth = 500;
        public static readonly int TextBoxWidth = 500;
        public static readonly int TextBoxHeight = 50;
        public static readonly int HeaderFontSize = 28;
        public static readonly int BodyFontSize = 24;
        public static readonly int ButtonFontSize = 16;
        public static readonly int InputFontSize = 36;
        public static readonly int ButtonWidth = 160;

        public static readonly Thickness DefaultPadding = new Thickness(5);
        public static readonly Thickness BottomMargin5 = new Thickness(0, 0, 0, 5);
        public static readonly Thickness BottomMargin10 = new Thickness(0, 0, 0, 10);
        public static readonly Thickness BottomMargin15 = new Thickness(0, 0, 0, 15);

        public static readonly string FlashDiskImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.FlashDisk.al8";
        public static readonly string RestartImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Change.al8";
        public static readonly string StartImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Start.al8";
        public static readonly string StopImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Stop.al8";
        public static readonly string ClearImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.clear_all.al8";
        public static readonly string ExitImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Exit.al8";
        public static readonly string PrintImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Print.al8";
        public static readonly string GeistlichImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.logo.png";
        public static readonly string OkImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.ok.al8";
        public static readonly string CancelImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Cancel.al8";
        public static readonly string ZeroImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Zero.al8";
        public static readonly string TareImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Tare.al8";
        public static readonly string ClearTareImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.ClearTare.al8";
        public static readonly string ArrowUpImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.ArrowUp.al8";
        public static readonly string ArrowDownImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.ArrowDown.al8";
        public static readonly string ArrowLeftImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.ArrowLeft.al8";
        public static readonly string DeleteImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Delete.al8";
        public static readonly string SaveImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Save.al8";
        public static readonly string AddImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Add.al8";
        public static readonly string RecipeImage = "embedded://mt.geistlich.countapp.ymlui/mt.geistlich.countapp.ymlui.images.Beaker New.al8";


    }
}
