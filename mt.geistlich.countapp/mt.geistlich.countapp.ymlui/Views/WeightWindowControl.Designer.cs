﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
using mt.geistlich.countapp.ymlui.Infrastructure;
namespace mt.geistlich.countapp.ymlui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class WeightWindowControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow myWeightWindow;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.Grid.Grid internal2;
            MT.Singularity.Presentation.Controls.TextBlock internal3;
            internal3 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal3.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal3.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal3.Margin = new MT.Singularity.Presentation.Thickness(0, 40, 0, 0);
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.Text,() =>  _viewModel.WeightInMg,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3.FontSize = ((System.Nullable<System.Int32>)90);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.Background,() => _viewModel.BackGround,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal3.Foreground = new MT.Singularity.Presentation.Color(4278204523u);
            internal3.GridColumn = 0;
            internal3.GridRow = 0;
            myWeightWindow = new MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow();
            myWeightWindow.GridColumn = 1;
            myWeightWindow.GridRow = 0;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => myWeightWindow.Background,() => _viewModel.BackGround,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal2 = new MT.Singularity.Presentation.Controls.Grid.Grid(internal3, myWeightWindow);
            internal2.RowDefinitions = GridDefinitions.Create("1*");
            internal2.ColumnDefinitions = GridDefinitions.Create("1*", "1*");
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal2.Background,() => _viewModel.BackGround,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
            this.Height = 400;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[4];
    }
}
