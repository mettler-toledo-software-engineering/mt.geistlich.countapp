﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using mt.geistlich.countapp.application.Services.Authentification;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Input;

namespace mt.geistlich.countapp.ymlui.Views
{
    /// <summary>
    /// Interaction logic for HomeScreen
    /// </summary>
    [Export(typeof(IHomeScreenFactoryService))]
    public partial class HomeScreen : IHomeScreenFactoryService
    {
        private readonly HomeScreenViewModel _homeScreenViewModel;
        private readonly WeightWindowControl _weightWindow;
        private readonly WeightButtonsControl _weightButtons;
        

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeScreen"/> class.
        /// </summary>
        public HomeScreen(HomeScreenViewModel homeScreenViewModel, WeightWindowControl weightWindow, WeightButtonsControl weightButtons)
        {
            _homeScreenViewModel = homeScreenViewModel;
            _weightWindow = weightWindow;
            _weightButtons = weightButtons;
            

            InitializeComponents();
        }

        public void OnPointerDown(object sender, PointerEventArgs pointerEventArgs)
        {
            _homeScreenViewModel.ResetLogoffTimerCommand.Execute(null);
        }

        /// <summary>
        /// Gets the home screen page.
        /// </summary>
        /// <value>
        /// The home screen page.
        /// </value>
        public INavigationPage HomeScreenPage
        {
            get { return this; }
        }

        /// <summary>
        /// This method is called before the home screen is shown.
        /// </summary>
        public void BeforeStart(IRootVisualProvider rootVisualProvider)
        {
        }



        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {
            _weightWindowControl.Add(_weightWindow);
            _weightButtonsControl.Add(_weightButtons);

            _homeScreenViewModel.RegisterEventsForViewModel();
            _homeScreenViewModel.ParentPage = this;
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            _homeScreenViewModel.UnregisterEventsForViewModel();
            _homeScreenViewModel.StopLogoffTimerCommand.Execute(null);
            if (result == NavigationResult.Proceed)
            {
                _weightWindowControl.Remove(_weightWindow);
                _weightButtonsControl.Remove(_weightButtons);
            }
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _homeScreenViewModel.RegisterEventsForViewModel();
            _homeScreenViewModel.RestartLogoffTimerCommand.Execute(null);
            _homeScreenViewModel.UpdateProperties();
            _weightWindowControl.Add(_weightWindow);
            _weightButtonsControl.Add(_weightButtons);
            _homeScreenViewModel.ParentPage = this;

        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _homeScreenViewModel.UnregisterEventsForViewModel();

            return base.OnNavigatingBack(nextPage);
        }

        /// <summary>
        /// Gets a value indicating whether the cursor should be hidden.
        /// </summary>
        /// <value>
        ///   <c>true</c> to hide the cursor; otherwise, <c>false</c>.
        /// </value>
        public bool HideCursor => false;
    }
}
