﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.geistlich.countapp.ymlui.Views
{
    /// <summary>
    /// Interaction logic for ManageRecipesView
    /// </summary>
    public partial class ManageRecipesView
    {
        private readonly ManageRecipesViewModel _manageRecipesViewModel;

        public ManageRecipesView(ManageRecipesViewModel manageRecipesViewModel)
        {
            _manageRecipesViewModel = manageRecipesViewModel;
            InitializeComponents();
        }

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {


            _manageRecipesViewModel.UpdateProperties();
            _manageRecipesViewModel.RegisterEventsForViewModel();
            _manageRecipesViewModel.ParentPage = this;
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            _manageRecipesViewModel.UnregisterEventsForViewModel();

            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _manageRecipesViewModel.RegisterEventsForViewModel();
            _manageRecipesViewModel.UpdateProperties();
            _manageRecipesViewModel.ParentPage = this;

        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _manageRecipesViewModel.UnregisterEventsForViewModel();
            return base.OnNavigatingBack(nextPage);
        }
    }
}
