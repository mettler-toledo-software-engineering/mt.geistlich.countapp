﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.geistlich.countapp.ymlui.Views
{
    /// <summary>
    /// Interaction logic for PickRecipeView
    /// </summary>
    public partial class PickRecipeView
    {
        private readonly PickRecipeViewModel _pickRecipeViewModel;
        public PickRecipeView(PickRecipeViewModel pickRecipeViewModel)
        {
            _pickRecipeViewModel = pickRecipeViewModel;
            InitializeComponents();
        }

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {


            _pickRecipeViewModel.UpdateProperties();
            _pickRecipeViewModel.RegisterEventsForViewModel();
            _pickRecipeViewModel.ParentPage = this;
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            _pickRecipeViewModel.UnregisterEventsForViewModel();

            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _pickRecipeViewModel.RegisterEventsForViewModel();
            _pickRecipeViewModel.UpdateProperties();
            //back wird hier aufgerufen um beim beenden des weighing process und zurückkehren auf diese seite weiter auf den homescreen zu navigieren.
            _pickRecipeViewModel.ParentPage.NavigationService.Back();
            _pickRecipeViewModel.ParentPage = this;

        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _pickRecipeViewModel.UnregisterEventsForViewModel();
            return base.OnNavigatingBack(nextPage);
        }


    }
}
