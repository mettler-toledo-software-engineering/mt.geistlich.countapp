﻿using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Platform.CommonUX.Controls;
using mt.geistlich.countapp.ymlui.Infrastructure;
using mt.geistlich.countapp.application.Models;
namespace mt.geistlich.countapp.ymlui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class PickRecipeView : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        public void Update_backButtonWidth()
        {
            _backButton.Width = Globals.ButtonWidth;
        }
        public void Update_backButtonBorderBrush()
        {
            _backButton.BorderBrush = Globals.LightGray;
        }
        public void Update_backButtonBackground()
        {
            _backButton.Background = Globals.LightGray;
        }
        public void Update_goToProcessButtonWidth()
        {
            _goToProcessButton.Width = Globals.ButtonWidth;
        }
        public void Update_goToProcessButtonBorderBrush()
        {
            _goToProcessButton.BorderBrush = Globals.LightGray;
        }
        public void Update_goToProcessButtonBackground()
        {
            _goToProcessButton.Background = Globals.LightGray;
        }
        private MT.Singularity.Presentation.Controls.Button _listUpButton;
        private MT.Singularity.Presentation.Controls.Button _listDownButton;
        private MT.Singularity.Presentation.Controls.Navigation.NavigationButton _backButton;
        private MT.Singularity.Presentation.Controls.Button _goToProcessButton;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.ListBox internal5;
            MT.Singularity.Presentation.ItemsPanelTemplate internal6;
            MT.Singularity.Presentation.DataTemplate<mt.geistlich.countapp.application.Models.Recipe> internal8;
            MT.Singularity.Presentation.Controls.StackPanel internal17;
            MT.Singularity.Presentation.Controls.GroupPanel internal18;
            MT.Singularity.Presentation.Controls.Image internal19;
            MT.Singularity.Presentation.Controls.GroupPanel internal20;
            MT.Singularity.Presentation.Controls.Image internal21;
            MT.Singularity.Presentation.Controls.StackPanel internal22;
            MT.Singularity.Presentation.Controls.TextBlock internal23;
            MT.Singularity.Presentation.Controls.TextBlock internal24;
            MT.Singularity.Presentation.Controls.TextBlock internal25;
            MT.Singularity.Presentation.Controls.TextBox internal26;
            MT.Singularity.Presentation.Controls.TextBlock internal27;
            MT.Singularity.Presentation.Controls.TextBox internal28;
            MT.Singularity.Presentation.Controls.DockPanel internal29;
            MT.Singularity.Presentation.Controls.GroupPanel internal30;
            MT.Singularity.Presentation.Controls.StackPanel internal31;
            MT.Singularity.Presentation.Controls.GroupPanel internal32;
            MT.Singularity.Presentation.Controls.Image internal33;
            MT.Singularity.Presentation.Controls.TextBlock internal34;
            MT.Singularity.Presentation.Controls.StackPanel internal35;
            MT.Singularity.Presentation.Controls.GroupPanel internal36;
            MT.Singularity.Presentation.Controls.Image internal37;
            MT.Singularity.Presentation.Controls.TextBlock internal38;
            internal5 = new MT.Singularity.Presentation.Controls.ListBox();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.ItemsSource,() =>  _pickRecipeViewModel.Recipes,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5.Width = 880;
            internal5.Height = 520;
            internal5.Background = Globals.LightBlueBrush;
            internal6 = new MT.Singularity.Presentation.ItemsPanelTemplate(0, (localBindings1) => {
                MT.Singularity.Presentation.Controls.DynamicWrapPanel internal7;
                internal7 = new MT.Singularity.Presentation.Controls.DynamicWrapPanel();
                internal7.MaxWidth = 900;
                internal7.Height = 520;
                internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
                internal7.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
                return internal7;
            });
            internal5.ItemsPanel = internal6;
            internal8 = new MT.Singularity.Presentation.DataTemplate<mt.geistlich.countapp.application.Models.Recipe>(4, (localBindings1, arg1_0) => {
                MT.Singularity.Presentation.Controls.Button internal9;
                MT.Singularity.Presentation.Controls.GroupPanel internal10;
                MT.Singularity.Presentation.Controls.DockPanel internal11;
                MT.Singularity.Presentation.Controls.TextBlock internal12;
                MT.Singularity.Presentation.Controls.DockPanel internal13;
                MT.Singularity.Presentation.Controls.StackPanel internal14;
                MT.Singularity.Presentation.Controls.TextBlock internal15;
                MT.Singularity.Presentation.Controls.TextBlock internal16;
                internal9 = new MT.Singularity.Presentation.Controls.Button();
                internal9.Margin = new MT.Singularity.Presentation.Thickness(10);
                internal9.Width = 150;
                internal9.Height = 150;
                internal9.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
                internal9.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
                internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
                internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
                internal12 = new MT.Singularity.Presentation.Controls.TextBlock();
                localBindings1[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Text,(Item) =>  Item.RecipeNameOnButton,MT.Singularity.Expressions.BindingMode.OneWay,arg1_0,false);
                internal12.FontSize = ((System.Nullable<System.Int32>)20);
                internal12.Margin = new MT.Singularity.Presentation.Thickness(5);
                internal11 = new MT.Singularity.Presentation.Controls.DockPanel(internal12);
                internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
                internal15 = new MT.Singularity.Presentation.Controls.TextBlock();
                localBindings1[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal15.Text,(Item) =>  Item.RecipeRange,MT.Singularity.Expressions.BindingMode.OneWay,arg1_0,false);
                internal15.FontSize = ((System.Nullable<System.Int32>)14);
                internal16 = new MT.Singularity.Presentation.Controls.TextBlock();
                localBindings1[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Text,(Item) =>  Item.RecipeNumber,MT.Singularity.Expressions.BindingMode.OneWay,arg1_0,false);
                internal16.FontSize = ((System.Nullable<System.Int32>)14);
                internal14 = new MT.Singularity.Presentation.Controls.StackPanel(internal15, internal16);
                internal14.Margin = new MT.Singularity.Presentation.Thickness(5);
                internal14.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
                internal13 = new MT.Singularity.Presentation.Controls.DockPanel(internal14);
                internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
                internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
                internal10 = new MT.Singularity.Presentation.Controls.GroupPanel(internal11, internal13);
                internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
                internal10.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
                internal9.Content = internal10;
                localBindings1[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.CommandParameter,(Item) =>  Item,MT.Singularity.Expressions.BindingMode.OneWay,arg1_0,false);
                internal9.Command = _pickRecipeViewModel.SelectRecipeCommand;
                return internal9;
            });
            internal5.ItemTemplate = internal8;
            _listUpButton = new MT.Singularity.Presentation.Controls.Button();
            _listUpButton.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 10, 5);
            _listUpButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _listUpButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal19 = new MT.Singularity.Presentation.Controls.Image();
            internal19.Source = Globals.ArrowUpImage;
            internal19.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal19.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal18 = new MT.Singularity.Presentation.Controls.GroupPanel(internal19);
            _listUpButton.Content = internal18;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _listUpButton.Command,() =>  _pickRecipeViewModel.ScrollUpCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _listDownButton = new MT.Singularity.Presentation.Controls.Button();
            _listDownButton.Margin = new MT.Singularity.Presentation.Thickness(10, 5, 5, 5);
            _listDownButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _listDownButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal21 = new MT.Singularity.Presentation.Controls.Image();
            internal21.Source = Globals.ArrowDownImage;
            internal21.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal21.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal20 = new MT.Singularity.Presentation.Controls.GroupPanel(internal21);
            _listDownButton.Content = internal20;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _listDownButton.Command,() =>  _pickRecipeViewModel.ScrollDownCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal17 = new MT.Singularity.Presentation.Controls.StackPanel(_listUpButton, _listDownButton);
            internal17.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal17.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5, internal17);
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal23 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal23.Text = "Gewähltes Rezept:";
            internal23.FontSize = Globals.HeaderFontSize;
            internal23.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 10);
            internal24 = new MT.Singularity.Presentation.Controls.TextBlock();
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal24.Text,() => _pickRecipeViewModel.SelectedRecipeNumber,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal24.FontSize = Globals.BodyFontSize;
            internal24.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 20);
            internal25 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal25.Text = "Rezept Nummer bestätigen:";
            internal25.FontSize = Globals.BodyFontSize;
            internal25.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 10);
            internal26 = new MT.Singularity.Presentation.Controls.TextBox();
            internal26.Width = 300;
            internal26.Height = Globals.TextBoxHeight;
            internal26.FontSize = Globals.BodyFontSize;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal26.Text,() => _pickRecipeViewModel.RecipeNumber,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal26.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal27 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal27.Text = "Chargen Nummer:";
            internal27.FontSize = Globals.BodyFontSize;
            internal27.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 10);
            internal28 = new MT.Singularity.Presentation.Controls.TextBox();
            internal28.Width = 300;
            internal28.Height = Globals.TextBoxHeight;
            internal28.FontSize = Globals.BodyFontSize;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal28.Text,() => _pickRecipeViewModel.Charge,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal28.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal22 = new MT.Singularity.Presentation.Controls.StackPanel(internal23, internal24, internal25, internal26, internal27, internal28);
            internal22.Margin = new MT.Singularity.Presentation.Thickness(10, 5, 0, 0);
            internal22.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal22);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(internal3);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _backButton = new MT.Singularity.Presentation.Controls.Navigation.NavigationButton();
            _backButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_backButtonWidth();
            _backButton.Height = 90;
            _backButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_backButtonBorderBrush();
            _backButton.BorderThickness = 5;
            this.Update_backButtonBackground();
            _backButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _backButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal33 = new MT.Singularity.Presentation.Controls.Image();
            internal33.Source = Globals.ArrowLeftImage;
            internal33.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal33.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal33.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal34 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal34.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal34.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal34.Text = "Zurück";
            internal34.FontSize = Globals.ButtonFontSize;
            internal34.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal32 = new MT.Singularity.Presentation.Controls.GroupPanel(internal33, internal34);
            _backButton.Content = internal32;
            _backButton.Back = true;
            internal31 = new MT.Singularity.Presentation.Controls.StackPanel(_backButton);
            internal31.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal31.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            _goToProcessButton = new MT.Singularity.Presentation.Controls.Button();
            _goToProcessButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_goToProcessButtonWidth();
            _goToProcessButton.Height = 90;
            _goToProcessButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_goToProcessButtonBorderBrush();
            _goToProcessButton.BorderThickness = 5;
            this.Update_goToProcessButtonBackground();
            _goToProcessButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _goToProcessButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal37 = new MT.Singularity.Presentation.Controls.Image();
            internal37.Source = Globals.StartImage;
            internal37.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal37.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal37.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal38 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal38.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal38.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal38.Text = "Wiegen";
            internal38.FontSize = Globals.ButtonFontSize;
            internal38.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal36 = new MT.Singularity.Presentation.Controls.GroupPanel(internal37, internal38);
            _goToProcessButton.Content = internal36;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _goToProcessButton.Command,() =>  _pickRecipeViewModel.GoToWeighingProcessViewCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal35 = new MT.Singularity.Presentation.Controls.StackPanel(_goToProcessButton);
            internal35.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal35.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal35.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal30 = new MT.Singularity.Presentation.Controls.GroupPanel(internal31, internal35);
            internal29 = new MT.Singularity.Presentation.Controls.DockPanel(internal30);
            internal29.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal29.Height = 100;
            internal29.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal29.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2, internal29);
            internal1.Background = Globals.LightBlueBrush;
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[7];
    }
}
