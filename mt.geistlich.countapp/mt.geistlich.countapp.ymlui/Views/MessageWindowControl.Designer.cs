﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
namespace mt.geistlich.countapp.ymlui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class MessageWindowControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.TextBlock internal2;
            internal2 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal2.Text,() =>  _viewModel.MessageText,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal2.FontSize = ((System.Nullable<System.Int32>)40);
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            internal1.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal1.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal1.Background,() =>  _viewModel.BackGround,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[2];
    }
}
