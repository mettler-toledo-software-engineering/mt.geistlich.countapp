﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace mt.geistlich.countapp.ymlui.Views
{
    /// <summary>
    /// Interaction logic for MessageWindowControl
    /// </summary>
    [InjectionBehavior(IsSingleton = true)]
    public partial class MessageWindowControl
    {
        private readonly MessageWindowViewModel _viewModel;
        public MessageWindowControl(MessageWindowViewModel viewModel)
        {
            _viewModel = viewModel;
            InitializeComponents();
        }
    }
}
