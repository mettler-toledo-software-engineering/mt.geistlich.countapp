﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
using MT.Singularity.Platform.CommonUX.Controls.DeltaTrac;
using mt.geistlich.countapp.ymlui.Infrastructure;
namespace mt.geistlich.countapp.ymlui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class WeighingProcessView : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        public void Update_testButtonWidth()
        {
            _testButton.Width = Globals.ButtonWidth;
        }
        public void Update_testButtonBorderBrush()
        {
            _testButton.BorderBrush = Globals.LightGray;
        }
        public void Update_testButtonBackground()
        {
            _testButton.Background = Globals.LightGray;
        }
        public void Update_reprintRecipeButtonWidth()
        {
            _reprintRecipeButton.Width = Globals.ButtonWidth;
        }
        public void Update_reprintRecipeButtonBorderBrush()
        {
            _reprintRecipeButton.BorderBrush = Globals.LightGray;
        }
        public void Update_reprintRecipeButtonBackground()
        {
            _reprintRecipeButton.Background = Globals.LightGray;
        }
        public void Update_finishProcessButtonWidth()
        {
            _finishProcessButton.Width = Globals.ButtonWidth;
        }
        public void Update_finishProcessButtonBorderBrush()
        {
            _finishProcessButton.BorderBrush = Globals.LightGray;
        }
        public void Update_finishProcessButtonBackground()
        {
            _finishProcessButton.Background = Globals.LightGray;
        }
        public void Update_leaveProcessButtonWidth()
        {
            _leaveProcessButton.Width = Globals.ButtonWidth;
        }
        public void Update_leaveProcessButtonBorderBrush()
        {
            _leaveProcessButton.BorderBrush = Globals.LightGray;
        }
        public void Update_leaveProcessButtonBackground()
        {
            _leaveProcessButton.Background = Globals.LightGray;
        }
        private MT.Singularity.Presentation.Controls.DynamicDockPanel _weightWindowControl;
        private MT.Singularity.Presentation.Controls.Grid.DynamicGrid _messageWindowControl;
        private MT.Singularity.Presentation.Controls.GroupPanel _orderInformationGroup;
        private MT.Singularity.Presentation.Controls.StackPanel _recipeStackPanel;
        private MT.Singularity.Presentation.Controls.StackPanel _orderStackPanel;
        private MT.Singularity.Presentation.Controls.StackPanel _chargeStackPanel;
        private MT.Singularity.Presentation.Controls.StackPanel _measurementsStackPanel;
        private MT.Singularity.Presentation.Controls.DynamicStackPanel _weightButtonsControl;
        private MT.Singularity.Presentation.Controls.Button _testButton;
        private MT.Singularity.Presentation.Controls.Button _reprintRecipeButton;
        private MT.Singularity.Presentation.Controls.Button _finishProcessButton;
        private MT.Singularity.Presentation.Controls.Button _leaveProcessButton;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.TextBlock internal4;
            MT.Singularity.Presentation.Controls.TextBlock internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.TextBlock internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            MT.Singularity.Presentation.Controls.DockPanel internal11;
            MT.Singularity.Presentation.Controls.GroupPanel internal12;
            MT.Singularity.Presentation.Controls.StackPanel internal13;
            MT.Singularity.Presentation.Controls.GroupPanel internal14;
            MT.Singularity.Presentation.Controls.TextBlock internal15;
            MT.Singularity.Presentation.Controls.GroupPanel internal16;
            MT.Singularity.Presentation.Controls.Image internal17;
            MT.Singularity.Presentation.Controls.TextBlock internal18;
            MT.Singularity.Presentation.Controls.GroupPanel internal19;
            MT.Singularity.Presentation.Controls.Image internal20;
            MT.Singularity.Presentation.Controls.TextBlock internal21;
            MT.Singularity.Presentation.Controls.GroupPanel internal22;
            MT.Singularity.Presentation.Controls.Image internal23;
            MT.Singularity.Presentation.Controls.TextBlock internal24;
            _weightWindowControl = new MT.Singularity.Presentation.Controls.DynamicDockPanel();
            _weightWindowControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _weightWindowControl.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            _messageWindowControl = new MT.Singularity.Presentation.Controls.Grid.DynamicGrid();
            _messageWindowControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _messageWindowControl.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _messageWindowControl.Height = 100;
            internal4 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal4.FontSize = Globals.BodyFontSize;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal4.Text,() =>  _viewModel.Recipe.RecipeNumber,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal4.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 5);
            internal5 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal5.FontSize = Globals.BodyFontSize;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal5.Text,() =>  _viewModel.Recipe.RecipeName,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 5);
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.FontSize = Globals.BodyFontSize;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal6.Text,() =>  _viewModel.Recipe.RecipeRange,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal6.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 5);
            _recipeStackPanel = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal5, internal6);
            _recipeStackPanel.Margin = new MT.Singularity.Presentation.Thickness(100, 10, 0, 10);
            _recipeStackPanel.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            _recipeStackPanel.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal7.FontSize = Globals.BodyFontSize;
            internal7.Text = "Chargennummer:";
            internal7.Width = 350;
            internal8 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal8.FontSize = Globals.BodyFontSize;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal8.Text,() =>  _viewModel.Charge,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal8.Width = 150;
            _chargeStackPanel = new MT.Singularity.Presentation.Controls.StackPanel(internal7, internal8);
            _chargeStackPanel.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            _chargeStackPanel.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 5);
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal9.FontSize = Globals.BodyFontSize;
            internal9.Text = "Total Messungen in Toleranz:";
            internal9.Width = 350;
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal10.FontSize = Globals.BodyFontSize;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Text,() =>  _viewModel.MeasurementCount,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10.Width = 150;
            _measurementsStackPanel = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal10);
            _measurementsStackPanel.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            _measurementsStackPanel.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 5);
            _orderStackPanel = new MT.Singularity.Presentation.Controls.StackPanel(_chargeStackPanel, _measurementsStackPanel);
            _orderStackPanel.Margin = new MT.Singularity.Presentation.Thickness(10);
            _orderStackPanel.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            _orderStackPanel.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            _orderStackPanel.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 100, 10);
            _orderInformationGroup = new MT.Singularity.Presentation.Controls.GroupPanel(_recipeStackPanel, _orderStackPanel);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(_weightWindowControl, _messageWindowControl, _orderInformationGroup);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(internal3);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _weightButtonsControl = new MT.Singularity.Presentation.Controls.DynamicStackPanel();
            _weightButtonsControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            _testButton = new MT.Singularity.Presentation.Controls.Button();
            _testButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_testButtonWidth();
            _testButton.Height = 90;
            _testButton.Visibility = MT.Singularity.Presentation.Visibility.Collapsed;
            _testButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_testButtonBorderBrush();
            _testButton.BorderThickness = 5;
            this.Update_testButtonBackground();
            _testButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _testButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal15 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal15.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal15.Text = "TEST";
            internal15.FontSize = Globals.ButtonFontSize;
            internal15.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal14 = new MT.Singularity.Presentation.Controls.GroupPanel(internal15);
            _testButton.Content = internal14;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _testButton.Command,() => _viewModel.CreateTestDataCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _reprintRecipeButton = new MT.Singularity.Presentation.Controls.Button();
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _reprintRecipeButton.Visibility,() =>  _viewModel.ProcessFinishedVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _reprintRecipeButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_reprintRecipeButtonWidth();
            _reprintRecipeButton.Height = 90;
            _reprintRecipeButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_reprintRecipeButtonBorderBrush();
            _reprintRecipeButton.BorderThickness = 5;
            this.Update_reprintRecipeButtonBackground();
            _reprintRecipeButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _reprintRecipeButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal17 = new MT.Singularity.Presentation.Controls.Image();
            internal17.Source = Globals.PrintImage;
            internal17.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal17.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal17.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal18 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal18.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal18.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal18.Text = "Reprint";
            internal18.FontSize = Globals.ButtonFontSize;
            internal18.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal16 = new MT.Singularity.Presentation.Controls.GroupPanel(internal17, internal18);
            _reprintRecipeButton.Content = internal16;
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _reprintRecipeButton.Command,() => _viewModel.ReprintCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _finishProcessButton = new MT.Singularity.Presentation.Controls.Button();
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _finishProcessButton.Visibility,() =>  _viewModel.ProcessOngoingVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _finishProcessButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_finishProcessButtonWidth();
            _finishProcessButton.Height = 90;
            _finishProcessButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_finishProcessButtonBorderBrush();
            _finishProcessButton.BorderThickness = 5;
            this.Update_finishProcessButtonBackground();
            _finishProcessButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _finishProcessButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal20 = new MT.Singularity.Presentation.Controls.Image();
            internal20.Source = Globals.StopImage;
            internal20.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal20.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal20.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal21 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal21.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal21.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal21.Text = "Prozess Beenden";
            internal21.FontSize = Globals.ButtonFontSize;
            internal21.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal19 = new MT.Singularity.Presentation.Controls.GroupPanel(internal20, internal21);
            _finishProcessButton.Content = internal19;
            this.bindings[9] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _finishProcessButton.Command,() => _viewModel.FinishWeighingProcessCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _leaveProcessButton = new MT.Singularity.Presentation.Controls.Button();
            this.bindings[10] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _leaveProcessButton.Visibility,() =>  _viewModel.ProcessFinishedVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _leaveProcessButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_leaveProcessButtonWidth();
            _leaveProcessButton.Height = 90;
            _leaveProcessButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_leaveProcessButtonBorderBrush();
            _leaveProcessButton.BorderThickness = 5;
            this.Update_leaveProcessButtonBackground();
            _leaveProcessButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _leaveProcessButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal23 = new MT.Singularity.Presentation.Controls.Image();
            internal23.Source = Globals.ExitImage;
            internal23.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal23.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal23.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal24 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal24.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal24.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal24.Text = "Prozess verlassen";
            internal24.FontSize = Globals.ButtonFontSize;
            internal24.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal22 = new MT.Singularity.Presentation.Controls.GroupPanel(internal23, internal24);
            _leaveProcessButton.Content = internal22;
            this.bindings[11] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _leaveProcessButton.Command,() => _viewModel.LeaveWeighingProcessCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.StackPanel(_testButton, _reprintRecipeButton, _finishProcessButton, _leaveProcessButton);
            internal13.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal12 = new MT.Singularity.Presentation.Controls.GroupPanel(_weightButtonsControl, internal13);
            internal11 = new MT.Singularity.Presentation.Controls.DockPanel(internal12);
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal11.Height = 100;
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal11.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2, internal11);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[12];
    }
}
