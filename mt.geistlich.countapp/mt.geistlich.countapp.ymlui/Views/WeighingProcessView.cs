﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.geistlich.countapp.ymlui.Views
{
    /// <summary>
    /// Interaction logic for WeighingProcessView
    /// </summary>
    public partial class WeighingProcessView
    {
        private readonly WeighingProcessViewModel _viewModel;
        private readonly WeightWindowControl _weightWindow;
        private readonly WeightButtonsControl _weightButtons;
        private readonly MessageWindowControl _messageWindow;

        public WeighingProcessView(WeighingProcessViewModel viewModel, WeightWindowControl weightWindow, WeightButtonsControl weightButtons, MessageWindowControl messageWindow)
        {
            _viewModel = viewModel;
            _weightWindow = weightWindow;
            _weightButtons = weightButtons;
            _messageWindow = messageWindow;
            InitializeComponents();
        }

        /// <summary>
        /// Called when a page is being navigated to for the first time.
        /// </summary>
        protected override void OnFirstNavigation()
        {

            _weightWindowControl.Add(_weightWindow);
            _weightButtonsControl.Add(_weightButtons);
            _messageWindowControl.Add(_messageWindow);
            _viewModel.UpdateProperties();
            _viewModel.RegisterEventsForViewModel();
            _viewModel.ParentPage = this;
            base.OnFirstNavigation();
        }

        /// <summary>
        /// Called when the user is navigating away from the current page to <paramref name="nextPage" />.
        /// </summary>
        /// <param name="nextPage">The next page the user is navigating to.</param>
        /// <returns>
        /// A value how the navigation framework should proceed with the navigation request.
        /// </returns>
        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            _viewModel.UnregisterEventsForViewModel();
            _weightWindowControl.Remove(_weightWindow);
            _weightButtonsControl.Remove(_weightButtons);
            _messageWindowControl.Remove(_messageWindow);
            return result;
        }

        /// <summary>
        /// Called when a page is reactivated when returning from another page.
        /// </summary>
        /// <param name="previousPage">The page that the user is returning from.</param>
        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _viewModel.RegisterEventsForViewModel();
            _viewModel.UpdateProperties();
            _viewModel.ParentPage = this;
            _weightWindowControl.Add(_weightWindow);
            _weightButtonsControl.Add(_weightButtons);
            _messageWindowControl.Add(_messageWindow);
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _viewModel.UnregisterEventsForViewModel();
            _weightWindowControl.Remove(_weightWindow);
            _weightButtonsControl.Remove(_weightButtons);
            _messageWindowControl.Remove(_messageWindow);


            return base.OnNavigatingBack(nextPage);
        }
    }
}
