﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Controls.DataGrid;
using mt.geistlich.countapp.ymlui.Infrastructure;
namespace mt.geistlich.countapp.ymlui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class ManageRecipesView : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        public void Update_backButtonWidth()
        {
            _backButton.Width = Globals.ButtonWidth;
        }
        public void Update_backButtonBorderBrush()
        {
            _backButton.BorderBrush = Globals.LightGray;
        }
        public void Update_backButtonBackground()
        {
            _backButton.Background = Globals.LightGray;
        }
        public void Update_deleteRecipeButtonWidth()
        {
            _deleteRecipeButton.Width = Globals.ButtonWidth;
        }
        public void Update_deleteRecipeButtonBorderBrush()
        {
            _deleteRecipeButton.BorderBrush = Globals.LightGray;
        }
        public void Update_deleteRecipeButtonBackground()
        {
            _deleteRecipeButton.Background = Globals.LightGray;
        }
        public void Update_newRecipeButtonWidth()
        {
            _newRecipeButton.Width = Globals.ButtonWidth;
        }
        public void Update_newRecipeButtonBorderBrush()
        {
            _newRecipeButton.BorderBrush = Globals.LightGray;
        }
        public void Update_newRecipeButtonBackground()
        {
            _newRecipeButton.Background = Globals.LightGray;
        }
        public void Update_updateRecipeButtonWidth()
        {
            _updateRecipeButton.Width = Globals.ButtonWidth;
        }
        public void Update_updateRecipeButtonBorderBrush()
        {
            _updateRecipeButton.BorderBrush = Globals.LightGray;
        }
        public void Update_updateRecipeButtonBackground()
        {
            _updateRecipeButton.Background = Globals.LightGray;
        }
        private MT.Singularity.Presentation.Controls.DataGrid.DataGrid _containerGrid;
        private MT.Singularity.Presentation.Controls.Button _listUpButton;
        private MT.Singularity.Presentation.Controls.Button _listDownButton;
        private MT.Singularity.Presentation.Controls.Navigation.NavigationButton _backButton;
        private MT.Singularity.Presentation.Controls.Button _deleteRecipeButton;
        private MT.Singularity.Presentation.Controls.Button _newRecipeButton;
        private MT.Singularity.Presentation.Controls.Button _updateRecipeButton;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn internal5;
            MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn internal6;
            MT.Singularity.Presentation.Controls.StackPanel internal7;
            MT.Singularity.Presentation.Controls.GroupPanel internal8;
            MT.Singularity.Presentation.Controls.Image internal9;
            MT.Singularity.Presentation.Controls.GroupPanel internal10;
            MT.Singularity.Presentation.Controls.Image internal11;
            MT.Singularity.Presentation.Controls.StackPanel internal12;
            MT.Singularity.Presentation.Controls.TextBlock internal13;
            MT.Singularity.Presentation.Controls.TextBox internal14;
            MT.Singularity.Presentation.Controls.TextBlock internal15;
            MT.Singularity.Presentation.Controls.TextBox internal16;
            MT.Singularity.Presentation.Controls.TextBlock internal17;
            MT.Singularity.Presentation.Controls.TextBox internal18;
            MT.Singularity.Presentation.Controls.TextBlock internal19;
            MT.Singularity.Presentation.Controls.TextBox internal20;
            MT.Singularity.Presentation.Controls.TextBlock internal21;
            MT.Singularity.Presentation.Controls.TextBlock internal22;
            MT.Singularity.Presentation.Controls.DockPanel internal23;
            MT.Singularity.Presentation.Controls.GroupPanel internal24;
            MT.Singularity.Presentation.Controls.StackPanel internal25;
            MT.Singularity.Presentation.Controls.GroupPanel internal26;
            MT.Singularity.Presentation.Controls.Image internal27;
            MT.Singularity.Presentation.Controls.TextBlock internal28;
            MT.Singularity.Presentation.Controls.StackPanel internal29;
            MT.Singularity.Presentation.Controls.GroupPanel internal30;
            MT.Singularity.Presentation.Controls.Image internal31;
            MT.Singularity.Presentation.Controls.TextBlock internal32;
            MT.Singularity.Presentation.Controls.GroupPanel internal33;
            MT.Singularity.Presentation.Controls.Image internal34;
            MT.Singularity.Presentation.Controls.TextBlock internal35;
            MT.Singularity.Presentation.Controls.GroupPanel internal36;
            MT.Singularity.Presentation.Controls.Image internal37;
            MT.Singularity.Presentation.Controls.TextBlock internal38;
            internal5 = new MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn();
            internal5.Header = "Rezept Nummer";
            internal5.ColumnName = "RecipeNumber";
            internal5.Width = 200;
            internal6 = new MT.Singularity.Presentation.Controls.DataGrid.DataGridTextColumn();
            internal6.Header = "Rezept Name";
            internal6.ColumnName = "RecipeName";
            internal6.Width = 400;
            _containerGrid = new MT.Singularity.Presentation.Controls.DataGrid.DataGrid();
            _containerGrid.Columns.Add(internal5);
            _containerGrid.Columns.Add(internal6);
            _containerGrid.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 5, 5);
            _containerGrid.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _containerGrid.RowHeight = 40;
            _containerGrid.ColumnHeaderHeight = 40;
            _containerGrid.Height = 500;
            _containerGrid.FontSize = ((System.Nullable<System.Int32>)24);
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _containerGrid.ItemsSource,() =>  _manageRecipesViewModel.Recipes,MT.Singularity.Expressions.BindingMode.OneWay,false);
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _containerGrid.SelectedIndex,() =>  _manageRecipesViewModel.ListIndex,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _containerGrid.SelectedItem,() =>  _manageRecipesViewModel.SelectedItem,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            _listUpButton = new MT.Singularity.Presentation.Controls.Button();
            _listUpButton.Margin = new MT.Singularity.Presentation.Thickness(5, 5, 10, 5);
            _listUpButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _listUpButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal9 = new MT.Singularity.Presentation.Controls.Image();
            internal9.Source = Globals.ArrowUpImage;
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal8 = new MT.Singularity.Presentation.Controls.GroupPanel(internal9);
            _listUpButton.Content = internal8;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _listUpButton.Command,() =>  _manageRecipesViewModel.ScrollUpCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _listDownButton = new MT.Singularity.Presentation.Controls.Button();
            _listDownButton.Margin = new MT.Singularity.Presentation.Thickness(10, 5, 5, 5);
            _listDownButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            _listDownButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal11 = new MT.Singularity.Presentation.Controls.Image();
            internal11.Source = Globals.ArrowDownImage;
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal10 = new MT.Singularity.Presentation.Controls.GroupPanel(internal11);
            _listDownButton.Content = internal10;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _listDownButton.Command,() =>  _manageRecipesViewModel.ScrollDownCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal7 = new MT.Singularity.Presentation.Controls.StackPanel(_listUpButton, _listDownButton);
            internal7.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal7.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(_containerGrid, internal7);
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal13 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal13.Text = "Rezept Nummer:";
            internal13.FontSize = Globals.BodyFontSize;
            internal13.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 10);
            internal14 = new MT.Singularity.Presentation.Controls.TextBox();
            internal14.Width = Globals.TextBoxWidth;
            internal14.Height = Globals.TextBoxHeight;
            internal14.FontSize = Globals.BodyFontSize;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal14.Text,() => _manageRecipesViewModel.RecipeNumber,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal14.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal15 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal15.Text = "Rezept Name:";
            internal15.FontSize = Globals.BodyFontSize;
            internal15.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 10);
            internal16 = new MT.Singularity.Presentation.Controls.TextBox();
            internal16.Width = Globals.TextBoxWidth;
            internal16.Height = Globals.TextBoxHeight;
            internal16.FontSize = Globals.BodyFontSize;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Text,() => _manageRecipesViewModel.RecipeName,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal17 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal17.Text = "Untere Toleranz:";
            internal17.FontSize = Globals.BodyFontSize;
            internal17.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 10);
            internal18 = new MT.Singularity.Presentation.Controls.TextBox();
            internal18.Width = Globals.TextBoxWidth;
            internal18.Height = Globals.TextBoxHeight;
            internal18.FontSize = Globals.BodyFontSize;
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal18.Text,() => _manageRecipesViewModel.LowerTolerance,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal18.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal19 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal19.Text = "Obere Toleranz:";
            internal19.FontSize = Globals.BodyFontSize;
            internal19.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 10);
            internal20 = new MT.Singularity.Presentation.Controls.TextBox();
            internal20.Width = Globals.TextBoxWidth;
            internal20.Height = Globals.TextBoxHeight;
            internal20.FontSize = Globals.BodyFontSize;
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal20.Text,() => _manageRecipesViewModel.UpperTolerance,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal20.KeyboardLayout = MT.Singularity.Presentation.Input.KeyboardLayout.Numeric;
            internal21 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal21.Text = "Zuletzt Geändert:";
            internal21.FontSize = Globals.BodyFontSize;
            internal21.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 10);
            internal22 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal22.Width = Globals.TextBoxWidth;
            internal22.Height = Globals.TextBoxHeight;
            internal22.FontSize = Globals.BodyFontSize;
            this.bindings[9] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal22.Text,() => _manageRecipesViewModel.SelectedRecipe.LastModifiedFormatted,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal12 = new MT.Singularity.Presentation.Controls.StackPanel(internal13, internal14, internal15, internal16, internal17, internal18, internal19, internal20, internal21, internal22);
            internal12.Margin = new MT.Singularity.Presentation.Thickness(10, 5, 0, 0);
            internal12.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal12);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(internal3);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _backButton = new MT.Singularity.Presentation.Controls.Navigation.NavigationButton();
            _backButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_backButtonWidth();
            _backButton.Height = 90;
            _backButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_backButtonBorderBrush();
            _backButton.BorderThickness = 5;
            this.Update_backButtonBackground();
            _backButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _backButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal27 = new MT.Singularity.Presentation.Controls.Image();
            internal27.Source = Globals.ArrowLeftImage;
            internal27.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal27.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal27.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal28 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal28.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal28.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal28.Text = "Zurück";
            internal28.FontSize = Globals.ButtonFontSize;
            internal28.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal26 = new MT.Singularity.Presentation.Controls.GroupPanel(internal27, internal28);
            _backButton.Content = internal26;
            _backButton.Back = true;
            internal25 = new MT.Singularity.Presentation.Controls.StackPanel(_backButton);
            internal25.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal25.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            _deleteRecipeButton = new MT.Singularity.Presentation.Controls.Button();
            _deleteRecipeButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_deleteRecipeButtonWidth();
            _deleteRecipeButton.Height = 90;
            _deleteRecipeButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_deleteRecipeButtonBorderBrush();
            _deleteRecipeButton.BorderThickness = 5;
            this.Update_deleteRecipeButtonBackground();
            _deleteRecipeButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _deleteRecipeButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal31 = new MT.Singularity.Presentation.Controls.Image();
            internal31.Source = Globals.DeleteImage;
            internal31.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal31.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal31.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal32 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal32.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal32.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal32.Text = "Löschen";
            internal32.FontSize = Globals.ButtonFontSize;
            internal32.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal30 = new MT.Singularity.Presentation.Controls.GroupPanel(internal31, internal32);
            _deleteRecipeButton.Content = internal30;
            this.bindings[10] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _deleteRecipeButton.Command,() =>  _manageRecipesViewModel.DeleteRecipeCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _newRecipeButton = new MT.Singularity.Presentation.Controls.Button();
            _newRecipeButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_newRecipeButtonWidth();
            _newRecipeButton.Height = 90;
            _newRecipeButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_newRecipeButtonBorderBrush();
            _newRecipeButton.BorderThickness = 5;
            this.Update_newRecipeButtonBackground();
            _newRecipeButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _newRecipeButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal34 = new MT.Singularity.Presentation.Controls.Image();
            internal34.Source = Globals.AddImage;
            internal34.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal34.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal34.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal35 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal35.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal35.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal35.Text = "Rezept kopieren";
            internal35.FontSize = Globals.ButtonFontSize;
            internal35.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal33 = new MT.Singularity.Presentation.Controls.GroupPanel(internal34, internal35);
            _newRecipeButton.Content = internal33;
            this.bindings[11] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _newRecipeButton.Command,() =>  _manageRecipesViewModel.CreateNewRecipeCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _updateRecipeButton = new MT.Singularity.Presentation.Controls.Button();
            _updateRecipeButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_updateRecipeButtonWidth();
            _updateRecipeButton.Height = 90;
            _updateRecipeButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_updateRecipeButtonBorderBrush();
            _updateRecipeButton.BorderThickness = 5;
            this.Update_updateRecipeButtonBackground();
            _updateRecipeButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _updateRecipeButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal37 = new MT.Singularity.Presentation.Controls.Image();
            internal37.Source = Globals.SaveImage;
            internal37.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal37.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal37.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal38 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal38.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal38.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal38.Text = "Änderung Speichern";
            internal38.FontSize = Globals.ButtonFontSize;
            internal38.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal36 = new MT.Singularity.Presentation.Controls.GroupPanel(internal37, internal38);
            _updateRecipeButton.Content = internal36;
            this.bindings[12] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _updateRecipeButton.Command,() =>  _manageRecipesViewModel.UpdateRecipeCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal29 = new MT.Singularity.Presentation.Controls.StackPanel(_deleteRecipeButton, _newRecipeButton, _updateRecipeButton);
            internal29.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal29.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal29.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal24 = new MT.Singularity.Presentation.Controls.GroupPanel(internal25, internal29);
            internal23 = new MT.Singularity.Presentation.Controls.DockPanel(internal24);
            internal23.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal23.Height = 100;
            internal23.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal23.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2, internal23);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[13];
    }
}
