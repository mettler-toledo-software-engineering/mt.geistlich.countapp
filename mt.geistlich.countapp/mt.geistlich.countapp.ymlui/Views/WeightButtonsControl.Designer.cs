﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using mt.geistlich.countapp.ymlui.Infrastructure;
namespace mt.geistlich.countapp.ymlui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class WeightButtonsControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            MT.Singularity.Presentation.Controls.Button internal3;
            MT.Singularity.Presentation.Controls.GroupPanel internal4;
            MT.Singularity.Presentation.Controls.Image internal5;
            MT.Singularity.Presentation.Controls.TextBlock internal6;
            MT.Singularity.Presentation.Controls.Button internal7;
            MT.Singularity.Presentation.Controls.GroupPanel internal8;
            MT.Singularity.Presentation.Controls.Image internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            MT.Singularity.Presentation.Controls.Button internal11;
            MT.Singularity.Presentation.Controls.GroupPanel internal12;
            MT.Singularity.Presentation.Controls.Image internal13;
            MT.Singularity.Presentation.Controls.TextBlock internal14;
            internal3 = new MT.Singularity.Presentation.Controls.Button();
            internal3.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal3.Width = Globals.ButtonWidth;
            internal3.Height = 90;
            internal3.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal3.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal5 = new MT.Singularity.Presentation.Controls.Image();
            internal5.Source = Globals.ZeroImage;
            internal5.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal5.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal5.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal6 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal6.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6.Text = mt.geistlich.countapp.ymlui.Localization.Get(mt.geistlich.countapp.ymlui.Localization.Key.Zero);
            internal6.AddTranslationAction(() => {
                internal6.Text = mt.geistlich.countapp.ymlui.Localization.Get(mt.geistlich.countapp.ymlui.Localization.Key.Zero);
            });
            internal6.FontSize = ((System.Nullable<System.Int32>)20);
            internal6.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal4 = new MT.Singularity.Presentation.Controls.GroupPanel(internal5, internal6);
            internal3.Content = internal4;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal3.Command,() =>  _weightButtonsViewModel.ZeroCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal7 = new MT.Singularity.Presentation.Controls.Button();
            internal7.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal7.Width = Globals.ButtonWidth;
            internal7.Height = 90;
            internal7.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal7.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal9 = new MT.Singularity.Presentation.Controls.Image();
            internal9.Source = Globals.TareImage;
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal9.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal10.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal10.Text = mt.geistlich.countapp.ymlui.Localization.Get(mt.geistlich.countapp.ymlui.Localization.Key.Tare);
            internal10.AddTranslationAction(() => {
                internal10.Text = mt.geistlich.countapp.ymlui.Localization.Get(mt.geistlich.countapp.ymlui.Localization.Key.Tare);
            });
            internal10.FontSize = ((System.Nullable<System.Int32>)20);
            internal10.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal8 = new MT.Singularity.Presentation.Controls.GroupPanel(internal9, internal10);
            internal7.Content = internal8;
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal7.Command,() =>  _weightButtonsViewModel.TareCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal11 = new MT.Singularity.Presentation.Controls.Button();
            internal11.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal11.Width = Globals.ButtonWidth;
            internal11.Height = 90;
            internal11.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal11.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal13 = new MT.Singularity.Presentation.Controls.Image();
            internal13.Source = Globals.ClearTareImage;
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal13.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal14 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal14.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal14.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal14.Text = mt.geistlich.countapp.ymlui.Localization.Get(mt.geistlich.countapp.ymlui.Localization.Key.ClearTare);
            internal14.AddTranslationAction(() => {
                internal14.Text = mt.geistlich.countapp.ymlui.Localization.Get(mt.geistlich.countapp.ymlui.Localization.Key.ClearTare);
            });
            internal14.FontSize = ((System.Nullable<System.Int32>)20);
            internal14.Margin = new MT.Singularity.Presentation.Thickness(1);
            internal12 = new MT.Singularity.Presentation.Controls.GroupPanel(internal13, internal14);
            internal11.Content = internal12;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal11.Command,() =>  _weightButtonsViewModel.ClearTareCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(internal3, internal7, internal11);
            internal2.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal2.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[3];
    }
}
