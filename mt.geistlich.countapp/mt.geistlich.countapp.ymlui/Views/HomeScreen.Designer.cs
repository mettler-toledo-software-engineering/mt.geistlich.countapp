﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
using MT.Singularity.Platform.CommonUX.Controls.DeltaTrac;
using mt.geistlich.countapp.ymlui.Infrastructure;
namespace mt.geistlich.countapp.ymlui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class HomeScreen : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        public void Update_goToManageRecipesButtonWidth()
        {
            _goToManageRecipesButton.Width = Globals.ButtonWidth;
        }
        public void Update_goToManageRecipesButtonBorderBrush()
        {
            _goToManageRecipesButton.BorderBrush = Globals.LightGray;
        }
        public void Update_goToManageRecipesButtonBackground()
        {
            _goToManageRecipesButton.Background = Globals.LightGray;
        }
        public void Update_goToPickRecipeButtonWidth()
        {
            _goToPickRecipeButton.Width = Globals.ButtonWidth;
        }
        public void Update_goToPickRecipeButtonBorderBrush()
        {
            _goToPickRecipeButton.BorderBrush = Globals.LightGray;
        }
        public void Update_goToPickRecipeButtonBackground()
        {
            _goToPickRecipeButton.Background = Globals.LightGray;
        }
        private MT.Singularity.Presentation.Controls.DynamicDockPanel _weightWindowControl;
        private MT.Singularity.Presentation.Controls.DynamicStackPanel _weightButtonsControl;
        private MT.Singularity.Presentation.Controls.Button _goToManageRecipesButton;
        private MT.Singularity.Presentation.Controls.Button _goToPickRecipeButton;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.DockPanel internal4;
            MT.Singularity.Presentation.Controls.StackPanel internal5;
            MT.Singularity.Presentation.Controls.Image internal6;
            MT.Singularity.Presentation.Controls.TextBlock internal7;
            MT.Singularity.Presentation.Controls.TextBlock internal8;
            MT.Singularity.Presentation.Controls.TextBlock internal9;
            MT.Singularity.Presentation.Controls.TextBlock internal10;
            MT.Singularity.Presentation.Controls.DockPanel internal11;
            MT.Singularity.Presentation.Controls.GroupPanel internal12;
            MT.Singularity.Presentation.Controls.StackPanel internal13;
            MT.Singularity.Presentation.Controls.GroupPanel internal14;
            MT.Singularity.Presentation.Controls.Image internal15;
            MT.Singularity.Presentation.Controls.TextBlock internal16;
            MT.Singularity.Presentation.Controls.GroupPanel internal17;
            MT.Singularity.Presentation.Controls.Image internal18;
            MT.Singularity.Presentation.Controls.TextBlock internal19;
            _weightWindowControl = new MT.Singularity.Presentation.Controls.DynamicDockPanel();
            _weightWindowControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _weightWindowControl.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            _weightWindowControl.Height = 200;
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(_weightWindowControl);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(internal3);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal6 = new MT.Singularity.Presentation.Controls.Image();
            internal6.Margin = new MT.Singularity.Presentation.Thickness(0, 0, 0, 0);
            internal6.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6.Source = Globals.GeistlichImage;
            internal7 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal7.Text = "Mettler-Toledo (Schweiz) GmbH";
            internal8 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal8.Text = "Custom Engineering";
            internal9 = new MT.Singularity.Presentation.Controls.TextBlock();
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Text,() =>  _homeScreenViewModel.ProjectNumber,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal10 = new MT.Singularity.Presentation.Controls.TextBlock();
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal10.Text,() =>  _homeScreenViewModel.Version,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.StackPanel(internal6, internal7, internal8, internal9, internal10);
            internal4 = new MT.Singularity.Presentation.Controls.DockPanel(internal5);
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal4.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            _weightButtonsControl = new MT.Singularity.Presentation.Controls.DynamicStackPanel();
            _weightButtonsControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            _goToManageRecipesButton = new MT.Singularity.Presentation.Controls.Button();
            _goToManageRecipesButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_goToManageRecipesButtonWidth();
            _goToManageRecipesButton.Height = 90;
            _goToManageRecipesButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_goToManageRecipesButtonBorderBrush();
            _goToManageRecipesButton.BorderThickness = 5;
            this.Update_goToManageRecipesButtonBackground();
            _goToManageRecipesButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _goToManageRecipesButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal15 = new MT.Singularity.Presentation.Controls.Image();
            internal15.Source = Globals.RecipeImage;
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal15.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal15.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal16 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal16.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal16.Text = "Rezepte Verwalten";
            internal16.FontSize = Globals.ButtonFontSize;
            internal16.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal14 = new MT.Singularity.Presentation.Controls.GroupPanel(internal15, internal16);
            _goToManageRecipesButton.Content = internal14;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _goToManageRecipesButton.Command,() => _homeScreenViewModel.GoToManageRecipesViewCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _goToPickRecipeButton = new MT.Singularity.Presentation.Controls.Button();
            _goToPickRecipeButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_goToPickRecipeButtonWidth();
            _goToPickRecipeButton.Height = 90;
            _goToPickRecipeButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_goToPickRecipeButtonBorderBrush();
            _goToPickRecipeButton.BorderThickness = 5;
            this.Update_goToPickRecipeButtonBackground();
            _goToPickRecipeButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _goToPickRecipeButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal18 = new MT.Singularity.Presentation.Controls.Image();
            internal18.Source = Globals.StartImage;
            internal18.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal18.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal18.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal19 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal19.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal19.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal19.Text = "Prozess Starten";
            internal19.FontSize = Globals.ButtonFontSize;
            internal19.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal17 = new MT.Singularity.Presentation.Controls.GroupPanel(internal18, internal19);
            _goToPickRecipeButton.Content = internal17;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _goToPickRecipeButton.Command,() => _homeScreenViewModel.GoToPickRecipeViewCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.StackPanel(_goToManageRecipesButton, _goToPickRecipeButton);
            internal13.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal13.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal13.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal12 = new MT.Singularity.Presentation.Controls.GroupPanel(_weightButtonsControl, internal13);
            internal11 = new MT.Singularity.Presentation.Controls.DockPanel(internal12);
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal11.Height = 100;
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal11.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2, internal4, internal11);
            internal1.PointerDown += OnPointerDown;
            this.Content = internal1;
            this.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            this.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => this.Background,() =>  _homeScreenViewModel.BackGround,MT.Singularity.Expressions.BindingMode.OneWay,false);
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[5];
    }
}
