﻿using System;

using System.Collections.Generic;
using System.Text;
using log4net;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity.Logging;


namespace mt.geistlich.countapp.ymlui.Commands
{
    public class SelectRecipeCommand : CommandBase
    {
        private readonly PickRecipeViewModel _pickRecipeViewModel;
        public SelectRecipeCommand(PickRecipeViewModel pickRecipeViewModel)
        {
            _pickRecipeViewModel = pickRecipeViewModel;
        }

        public override void Execute(object parameter)
        {
            try
            {
                var recipe = parameter as Recipe;
                // wenn ein anderes / neues rezept ausgewählt wird, sollen die formularfelder geleert werden.
                if (_pickRecipeViewModel.SelectedRecipe != null && recipe != _pickRecipeViewModel.SelectedRecipe)
                {
                    _pickRecipeViewModel.Charge = string.Empty;
                    _pickRecipeViewModel.RecipeNumber = string.Empty;
                }

                _pickRecipeViewModel.SelectedRecipe = recipe;
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);

            }

        }
    }
}
