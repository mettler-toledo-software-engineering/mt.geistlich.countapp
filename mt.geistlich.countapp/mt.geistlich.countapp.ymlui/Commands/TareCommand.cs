﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using log4net;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class TareCommand : AsyncCommandBase
    {
        private readonly IScaleService _scaleService;

        public TareCommand(IScaleService scaleService)
        {
            _scaleService = scaleService;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {


                await _scaleService.SelectedScale.TareAsync();
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
        }
    }
}
