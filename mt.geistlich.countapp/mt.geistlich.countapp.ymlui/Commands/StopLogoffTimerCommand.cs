﻿using System;

using System.Collections.Generic;
using System.Text;
using log4net;
using mt.geistlich.countapp.ymlui.State;
using MT.Singularity.Logging;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class StopLogoffTimerCommand : CommandBase
    {

        private readonly UserStore _userStore;
        public StopLogoffTimerCommand(UserStore userStore)
        {
            _userStore = userStore;
        }

        public override void Execute(object parameter)
        {
            try
            {
                _userStore.StopLogoffTimer();
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
            
        }
    }
}
