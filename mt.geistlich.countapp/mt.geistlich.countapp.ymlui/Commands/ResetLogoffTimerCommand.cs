﻿using System;

using System.Collections.Generic;
using System.Text;
using log4net;
using mt.geistlich.countapp.ymlui.State;
using MT.Singularity.Logging;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class ResetLogoffTimerCommand : CommandBase
    {
        private readonly UserStore _userStore;
        public ResetLogoffTimerCommand(UserStore userStore)
        {
            _userStore = userStore;
        }

        public override void Execute(object parameter)
        {
            try
            {
                _userStore.ResetLogoffTimer();
            }
            catch (Exception ex)
            {
                Logger.ErrorEx(ex.Message, SourceClass, ex);
            }
            
        }
    }
}
