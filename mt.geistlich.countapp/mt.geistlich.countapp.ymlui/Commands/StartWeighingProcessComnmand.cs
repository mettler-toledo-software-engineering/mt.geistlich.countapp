﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using log4net;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity.Logging;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class StartWeighingProcessComnmand : AsyncCommandBase
    {

        private readonly WeighingProcessStore _weighingProcessStore;
        private readonly PrinterStore _printerStore;
        private readonly OrderStore _orderStore;

        public StartWeighingProcessComnmand(OrderStore orderStore, WeighingProcessStore weighingProcessStore, PrinterStore printerStore )
        {

            _orderStore = orderStore;
            _weighingProcessStore = weighingProcessStore;
            _printerStore = printerStore;

        }
        protected override async Task ExecuteAsync(object parameter)
        {
            await Task.Run(async () =>
            {
                try
                {
                    _weighingProcessStore.StartweighingProcess();
                    await _printerStore.PrintHeader(_orderStore.CurrentOrder);
                }
                catch (Exception e)
                {
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }

            });

        }
    }
}
