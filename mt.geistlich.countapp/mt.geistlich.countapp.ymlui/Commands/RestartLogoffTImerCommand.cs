﻿using System;

using System.Collections.Generic;
using System.Text;
using log4net;
using mt.geistlich.countapp.ymlui.State;
using MT.Singularity.Logging;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class RestartLogoffTImerCommand : CommandBase
    {

        private readonly UserStore _userStore;
        public RestartLogoffTImerCommand(UserStore userStore)
        {
            _userStore = userStore;
        }

        public override void Execute(object parameter)
        {
            try
            {
                _userStore.RestartLogoffTimer();
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
            
        }
    }
}
