﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using log4net;
using mt.geistlich.countapp.ymlui.Infrastructure;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class ZeroCommand : AsyncCommandBase
    {
        private readonly IScaleService _scaleService;

        public ZeroCommand(IScaleService scaleService)
        {
            _scaleService = scaleService;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                await _scaleService.SelectedScale.ZeroAsync();
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
        }
    }
}
