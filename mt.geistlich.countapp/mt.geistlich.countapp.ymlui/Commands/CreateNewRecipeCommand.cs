﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Services;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity.Logging;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class CreateNewRecipeCommand : AsyncCommandBase
    {
        private readonly RecipeStore _recipeStore;
        private readonly ManageRecipesViewModel _manageRecipesViewModel;
        public CreateNewRecipeCommand(RecipeStore recipeStore, ManageRecipesViewModel manageRecipeViewModel)
        {
            _recipeStore = recipeStore;
            _manageRecipesViewModel = manageRecipeViewModel;
            _manageRecipesViewModel.PropertyChanged += ViewModelOnPropertyChanged;
        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && _manageRecipesViewModel.SelectedRecipe != null && _manageRecipesViewModel.SelectedRecipe.IsVaild();
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                var recipe = _manageRecipesViewModel.SelectedRecipe;
                recipe.DateCreated = DateTime.Now;
                recipe.LastModified = DateTime.Now;
                
                await _recipeStore.CreateRecipe(recipe);
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
            
        }
    }
}
