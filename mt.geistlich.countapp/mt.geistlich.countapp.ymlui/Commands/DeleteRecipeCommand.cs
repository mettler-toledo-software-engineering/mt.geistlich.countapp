﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Services;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class DeleteRecipeCommand : AsyncCommandBase
    {
        private readonly RecipeStore _recipeStore;
        private readonly ManageRecipesViewModel _manageRecipesViewModel;
        public DeleteRecipeCommand(RecipeStore recipeStore, ManageRecipesViewModel manageRecipesViewModel)
        {
            _recipeStore = recipeStore;
            _manageRecipesViewModel = manageRecipesViewModel;

            _manageRecipesViewModel.PropertyChanged += ViewModelOnPropertyChanged; 

        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && _manageRecipesViewModel.SelectedRecipe != null && _manageRecipesViewModel.SelectedRecipe.Id > 0 ;
        }

        protected override async Task ExecuteAsync(object parameter)
        {

            var mb = MessageBox.Show(_manageRecipesViewModel.ParentVisual, "Wollen Sie das Rezept wirklich löschen?", "Achtung", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            mb.Close();
            var result = await mb.ShowAsync(_manageRecipesViewModel.ParentVisual);

            if (result == DialogResult.Yes)
            {
                try
                {
                    await _recipeStore.DeleteRecipe(_manageRecipesViewModel.SelectedRecipe.Id);
                }
                catch (Exception e)
                {
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }

            }

        }

      
    }
}
