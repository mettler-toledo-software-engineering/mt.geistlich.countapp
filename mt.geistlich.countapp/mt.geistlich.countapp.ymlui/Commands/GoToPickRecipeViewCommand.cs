﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using mt.geistlich.countapp.ymlui.Views;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Drawing;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class GoToPickRecipeViewCommand : AsyncCommandBase
    {
        private readonly HomeScreenViewModel _homeScreenViewModel;
        private readonly UserStore _userStore;
        private bool _canExecute;
        public GoToPickRecipeViewCommand(HomeScreenViewModel homeScreenViewModel, UserStore userStore)
        {
            _homeScreenViewModel = homeScreenViewModel;
            _userStore = userStore;
            _canExecute = _userStore.LoggedInUser != null;
            _userStore.UserLoggedIn += UserStoreOnUserLoggedIn;
            _userStore.UserLoggedOut += UserStoreOnUserLoggedIn;
        }

        private void UserStoreOnUserLoggedIn()
        {
            _canExecute = _userStore.LoggedInUser != null;
            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && _canExecute;
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {

                _homeScreenViewModel.ParentPage.NavigationService.NavigateTo(ApplicationBootstrapperBase.CompositionContainer.Resolve<PickRecipeView>());
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
        }
    }
}
