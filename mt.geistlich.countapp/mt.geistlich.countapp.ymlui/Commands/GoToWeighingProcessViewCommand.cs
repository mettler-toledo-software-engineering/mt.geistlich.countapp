﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using mt.geistlich.countapp.ymlui.Views;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class GoToWeighingProcessViewCommand : AsyncCommandBase
    {
        private readonly PickRecipeViewModel _pickRecipeViewModel;
        private readonly OrderStore _orderStore;
        private bool _canExecute;
        public GoToWeighingProcessViewCommand(PickRecipeViewModel pickRecipeViewModel, OrderStore orderStore)
        {
            _pickRecipeViewModel = pickRecipeViewModel;
            _orderStore = orderStore;
            _pickRecipeViewModel.PropertyChanged += PickRecipeViewModelOnPropertyChanged;
        }

        private void PickRecipeViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_pickRecipeViewModel.RecipeNumber) || e.PropertyName == nameof(_pickRecipeViewModel.Charge) || e.PropertyName == nameof(_pickRecipeViewModel.SelectedRecipe))
            {
                OnCanExecuteChanged();
            }
        }

        public override bool CanExecute(object parameter)
        {
            _canExecute = _pickRecipeViewModel?.RecipeNumber == _pickRecipeViewModel?.SelectedRecipe?.RecipeNumber && _pickRecipeViewModel?.Charge?.Length == 8;

            return base.CanExecute(parameter) && _canExecute;
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            var order = new Order();
            order.Recipe = _pickRecipeViewModel.SelectedRecipe;
            order.Charge = _pickRecipeViewModel.Charge;
            order.OrderStartTime = DateTime.Now;

            try
            {
                await _orderStore.CreateOrder(order);

                _pickRecipeViewModel.ParentPage.NavigationService.NavigateTo(ApplicationBootstrapperBase.CompositionContainer.Resolve<WeighingProcessView>());
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }

        }
    }
}
