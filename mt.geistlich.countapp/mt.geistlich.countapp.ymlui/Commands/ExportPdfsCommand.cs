﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Services.DataExport;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class ExportPdfsCommand : AsyncCommandBase
    {
        private readonly HomeScreenViewModel _homeScreenViewModel;
        private readonly IPdfExportService _pdfExportService;
        private bool _boxIsOpen = false;
        public ExportPdfsCommand(HomeScreenViewModel homeScreenViewModel, IPdfExportService pdfExportService)
        {
            _homeScreenViewModel = homeScreenViewModel;
            _pdfExportService = pdfExportService;
        }

        protected override async Task ExecuteAsync(object parameter)
        {

            if (_boxIsOpen)
            {
                return;
            }

            _boxIsOpen = true;
            MessageBox exportMb = MessageBox.Show(_homeScreenViewModel.ParentVisual, "Wollen Sie die Chargenprotokolle exportieren?", "USB Stick gefunden", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            exportMb.Close();

            var result = await exportMb.ShowAsync(_homeScreenViewModel.ParentVisual);

            if (result == DialogResult.Yes)
            {
                MessageBox pleaseWaitMb = MessageBox.Show(_homeScreenViewModel.ParentVisual, "Bitte warten", string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Information);
                
                await _pdfExportService.ExportPdfsToUsbDrive().ContinueWith(task =>
                {
                    if (task.IsCompleted)
                    {
                        pleaseWaitMb?.Close();

                        MessageBox.Show(_homeScreenViewModel.ParentVisual, $"Es wurden {task.Result} Chargenprotokolle exportiert", "Übertragung abgeschlossen", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    }

                    if (task.Exception != null)
                    {
                        MessageBox.Show(_homeScreenViewModel.ParentVisual, $"{task.Exception.Message}", "Chargenprotokoll Export Fehlgeschlagen", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Logger.ErrorEx(task.Exception.Message, SourceClass, task.Exception);
                    }

                    _boxIsOpen = false;

                } );
            }


        }
    }
}
