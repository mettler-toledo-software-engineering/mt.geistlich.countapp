﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class CreateTestDataCommand : AsyncCommandBase
    {
        private readonly MeasurementStore _measurementStore;
        private readonly OrderStore _orderStore;
        public CreateTestDataCommand(OrderStore orderStore, MeasurementStore measurementStore)
        {
            _measurementStore = measurementStore;
            _orderStore = orderStore;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            await Task.Run(async ()  => 
            {
                for (int i = 0; i < 35000; i++)
                {
                    Random rnd = new Random();

                    await _measurementStore.CreateMeasurement(new Measurement
                    {

                        NetWeight = rnd.Next(115, 149),
                        TimeStamp = DateTime.Now,
                        Order = _orderStore.CurrentOrder

                    });
                }
            });

        }
    }
}
