﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using log4net;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity.Logging;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class ReprintCommand : AsyncCommandBase
    {

        private readonly PrinterStore _printerStore;
        private readonly MessageStore _messageStore;
        private readonly OrderStore _orderStore;

        public ReprintCommand(PrinterStore printerStore, OrderStore orderStore, MessageStore messageStore)
        {
            _printerStore = printerStore;
            _orderStore = orderStore;
            _messageStore = messageStore;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                _messageStore.UpdateMessage(new Message(Messages.PleaseWait, MessageColors.LightBlue, false));
                await _printerStore.ReprintCompleteRecipe(_orderStore.CurrentOrder);
                _messageStore.UpdateMessage(new Message(Messages.PrintFinished, MessageColors.LightBlue, false));
            }
            catch (Exception ex)
            {
                _messageStore.UpdateMessage(new Message(Messages.ErrorPrint, MessageColors.Red, false));
                Logger.ErrorEx(ex.Message,SourceClass,ex);
            }

            try
            {
                _messageStore.UpdateMessage(new Message(Messages.PleaseWait, MessageColors.LightBlue, false));
                await _printerStore.CreatePdfDocument(_orderStore.CurrentOrder, true);
                _messageStore.UpdateMessage(new Message(Messages.ProcessFinished, MessageColors.Green, false));
            }
            catch (Exception ex)
            {
                _messageStore.UpdateMessage(new Message(Messages.ErrorPdf, MessageColors.Red, false));
                Logger.ErrorEx(ex.Message, SourceClass, ex);
            }
        }
    }
}
