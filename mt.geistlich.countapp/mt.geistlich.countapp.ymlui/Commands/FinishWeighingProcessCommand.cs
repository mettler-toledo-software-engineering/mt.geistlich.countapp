﻿using System;

using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.weighing;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity.Logging;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Drawing;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class FinishWeighingProcessCommand : AsyncCommandBase
    {
        private readonly WeighingProcessViewModel _weighingProcessViewModel;
        private readonly MessageStore _messageStore;
        private readonly WeighingProcessStore _weighingProcessStore;
        private readonly PrinterStore _printerStore;
        private readonly OrderStore _orderStore;


        public FinishWeighingProcessCommand(WeighingProcessViewModel weighingProcessViewModel, MessageStore messageStore, WeighingProcessStore weighingProcessStore, PrinterStore printerStore, OrderStore orderStore)
        {
            _weighingProcessViewModel = weighingProcessViewModel;
            _weighingProcessStore = weighingProcessStore;
            _messageStore = messageStore;
            _printerStore = printerStore;
            _orderStore = orderStore;
        }

        protected override async Task ExecuteAsync(object parameter)
        {


            if (_weighingProcessViewModel.MeasurementCount == 0)
            {
                try
                {
                    _weighingProcessStore.StopweighingProcess();
                }
                catch (Exception e)
                {
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }

                _weighingProcessViewModel.ProcessFinishedVisibility = Visibility.Visible;
                _weighingProcessViewModel.ProcessOngoingVisibility = Visibility.Collapsed;

                return;
            }

            //MessageBox questionMb = new MessageBox();
            //questionMb.Buttons = new MessageBoxButton[2] { new MessageBoxButton { Content = "Ok", DialogResult = DialogResult.Yes }, new MessageBoxButton() { Content = "No", DialogResult = DialogResult.No } };
            //questionMb.Text = "Sind Sie sicher, dass Sie den Prozess beenden wollen?\nAscnliessend können keine weiteren Messungen erfasst werden.";
            //questionMb.Title = "Prozess beenden?";
            //questionMb.Background = new SolidColorBrush(Colors.White);

            //var result = await questionMb.ShowAsync(_weighingProcessViewModel.ParentVisual);

            var questionMb = MessageBox.Show(_weighingProcessViewModel.ParentVisual, "Sind Sie sicher, dass Sie den Prozess beenden wollen?\nAscnliessend können keine weiteren Messungen erfasst werden.", "Prozess beenden?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            questionMb.Close();


            var result = await questionMb.ShowAsync(_weighingProcessViewModel.ParentVisual);


            if (result == DialogResult.Yes)
            {
                questionMb?.Close();
                _messageStore.UpdateMessage(new Message(Messages.PleaseWait, MessageColors.LightBlue, false));
                //MessageBox pleaseWaitMb = MessageBox.Show(_weighingProcessViewModel.ParentVisual, "Bitte warten", string.Empty, MessageBoxButtons.OK, MessageBoxIcon.Information);
                Order order = new Order();
                string pdf = string.Empty;

                try
                {
                    order = await _orderStore.FinishCurrentOrder();
                }
                catch (Exception e)
                {
                    _messageStore.UpdateMessage(new Message(Messages.ErrorPrint, MessageColors.Red, false));
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }

                try
                {
                    await _printerStore.PrintOrder(order);
                }
                catch (Exception e)
                {
                    _messageStore.UpdateMessage(new Message(Messages.ErrorPrint, MessageColors.Red, false));
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }

                try
                {
                    pdf = await _printerStore.CreatePdfDocument(order,false);
                }
                catch (Exception e)
                {
                    _messageStore.UpdateMessage(new Message(Messages.ErrorPdf, MessageColors.Red, false));
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }

               

                _weighingProcessStore.StopweighingProcess();

                _weighingProcessViewModel.ProcessFinishedVisibility = Visibility.Visible;
                _weighingProcessViewModel.ProcessOngoingVisibility = Visibility.Collapsed;

                //pleaseWaitMb?.Close();

                
            }
        }

    }
}
