﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Logging;
using MT.Singularity.Platform.ChangeLog;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class ClearTareCommand : AsyncCommandBase
    {
        private readonly IScaleService _scaleService;
        
        public ClearTareCommand(IScaleService scaleService)
        {
            _scaleService = scaleService;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                await _scaleService.SelectedScale.ClearTareAsync();
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);

            }
        }
    }
}
