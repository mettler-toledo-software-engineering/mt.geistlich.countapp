﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using log4net;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity.Logging;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class ScrollDownRecipePanelCommand : CommandBase
    {

        private readonly PickRecipeViewModel _pickRecipeViewModel;
        public ScrollDownRecipePanelCommand(PickRecipeViewModel pickRecipeViewModel)
        {
            _pickRecipeViewModel = pickRecipeViewModel;
        }
        public override void Execute(object parameter)
        {
            try
            {
                if (_pickRecipeViewModel.PanelIndex + _pickRecipeViewModel.NumberOfElements >= _pickRecipeViewModel.AllRecipes.Count)
                {
                    return;
                }
                else
                {
                    _pickRecipeViewModel.PanelIndex = _pickRecipeViewModel.PanelIndex + _pickRecipeViewModel.NumberOfElements;
                }
                _pickRecipeViewModel.Recipes = new ObservableCollection<Recipe>(_pickRecipeViewModel.AllRecipes.ToList().GetRange(_pickRecipeViewModel.PanelIndex, _pickRecipeViewModel.NumberOfElements));
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }

          
        }
    }
}
