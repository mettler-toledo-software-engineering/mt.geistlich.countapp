﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using mt.geistlich.countapp.ymlui.Views;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Composition;
using MT.Singularity.Logging;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class GoToManageRecipesViewCommand : CommandBase
    {
        private readonly HomeScreenViewModel _homeScreenViewModel;
        private readonly UserStore _userStore;
        private bool _canExecute;
        public GoToManageRecipesViewCommand(HomeScreenViewModel homeScreenViewModel, UserStore userStore)
        {
            _homeScreenViewModel = homeScreenViewModel;
            _userStore = userStore;
            _canExecute = _userStore.LoggedInUser is AdminUser;
            _userStore.UserLoggedIn += UserStoreOnUserLoggedIn;
            _userStore.UserLoggedOut += UserStoreOnUserLoggedIn;
        }

        private void UserStoreOnUserLoggedIn()
        {
            _canExecute = _userStore.LoggedInUser is AdminUser;
            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && _canExecute;
        }

        public override void Execute(object parameter)
        {
            try
            {
                _homeScreenViewModel.ParentPage.NavigationService.NavigateTo(ApplicationBootstrapperBase.CompositionContainer.Resolve<ManageRecipesView>());
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
            
        }
    }
}
