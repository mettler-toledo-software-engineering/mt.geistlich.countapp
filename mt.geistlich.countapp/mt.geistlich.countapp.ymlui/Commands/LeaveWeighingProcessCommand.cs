﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity.Logging;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class LeaveWeighingProcessCommand : AsyncCommandBase
    {
        private readonly WeighingProcessViewModel _weighingProcessViewModel;
        private readonly OrderStore _orderStore;
        private readonly MeasurementStore _measurementStore;
        private readonly PrinterStore _printerStore;
        public LeaveWeighingProcessCommand(WeighingProcessViewModel weighingProcessViewModel, OrderStore orderStore, MeasurementStore measurementStore, PrinterStore printerStore)
        {
            _weighingProcessViewModel = weighingProcessViewModel;
            _orderStore = orderStore;
            _measurementStore = measurementStore;
            _printerStore = printerStore;
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            int pdfsexported = 0;
            var mb = MessageBox.Show(_weighingProcessViewModel.ParentVisual, "Sind Sie sicher, dass Sie den Prozess verlassen wollen?\nAscnliessend werden alle gesammelten Daten gelöscht.\nEs werden keine weiteren Ausdrucke mehr möglich sein.", "Prozess Verlassen?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            mb.Close();

            var result = await mb.ShowAsync(_weighingProcessViewModel.ParentVisual);

            if (result == DialogResult.Yes)
            {
                try
                {
                    int orderId = _orderStore.CurrentOrder.Id;
                    await _measurementStore.DeleteMeasurementsByOrderId(orderId).ContinueWith(async task =>
                    {
                        if (task.Exception == null)
                        {
                            await _orderStore.DeleteSelectedOrder();
                        }
                        else
                        {
                            Logger.ErrorEx(task.Exception.Message, SourceClass, task.Exception);
                        }

                    });

                }
                catch (Exception e)
                {
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }

                try
                {
                    pdfsexported = await _printerStore.ExportPdfDocuments();
                }
                catch (Exception e)
                {
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }

                _weighingProcessViewModel.ParentPage.NavigationService.Back();

                if (pdfsexported > 0)
                {
                    MessageBox.Show(_weighingProcessViewModel.ParentVisual, $"Es wurden {pdfsexported} PDFs exportiert", "Transfer abgeschlossen", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            }
        }
    }
}
