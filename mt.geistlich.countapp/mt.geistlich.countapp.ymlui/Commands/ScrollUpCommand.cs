﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using log4net;
using mt.geistlich.countapp.ymlui.ViewModels;
using MT.Singularity.Logging;

namespace mt.geistlich.countapp.ymlui.Commands
{
    public class ScrollUpCommand : CommandBase
    {

        private ManageRecipesViewModel _manageRecipesViewModel;
        public ScrollUpCommand(ManageRecipesViewModel manageRecipesViewModel)
        {
            _manageRecipesViewModel = manageRecipesViewModel;
            _manageRecipesViewModel.PropertyChanged += ManageListsViewModelOnPropertyChanged;
        }

        private void ManageListsViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_manageRecipesViewModel.ListIndex) || e.PropertyName == nameof(_manageRecipesViewModel.ListCount))
            {
                
                OnCanExecuteChanged();
            }
        }

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && _manageRecipesViewModel.ListIndex > 0;
        }

        public override void Execute(object parameter)
        {

            try
            {
                if (_manageRecipesViewModel.ListIndex <= 5)
                {
                    _manageRecipesViewModel.ListIndex = 0;
                }
                else
                {
                    _manageRecipesViewModel.ListIndex = _manageRecipesViewModel.ListIndex - 5;
                }
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
        }
    }
}
