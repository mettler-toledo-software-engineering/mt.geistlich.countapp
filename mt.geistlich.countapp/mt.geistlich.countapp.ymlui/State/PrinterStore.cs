﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;

using log4net;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.DataExport;
using mt.geistlich.countapp.application.Services.Printing;
using mt.geistlich.countapp.peripherals.Printer;
using mt.geistlich.countapp.ymlui.Configuration;
using mt.geistlich.countapp.ymlui.Infrastructure;
using MT.Singularity.Logging;

namespace mt.geistlich.countapp.ymlui.State
{
    public class PrinterStore
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(PrinterStore);

        private IApr331Printer _currentPrinter;
        private readonly ICreatePrintTemplateService _printTemplateService;
        private readonly ICreatePdfTemplateService _pdfTemplateService;
        private readonly IPdfExportService _pdfExportService;
        private readonly GeistlichConfiguration _geistlichConfiguration;
        private bool _printHeaderOnOrderResume;

        public PrinterStore(ConfigurationStore geistlichConfiguration, IPrinterFactoryService printerFactory, ICreatePrintTemplateService printTemplateService, ICreatePdfTemplateService pdfTemplateService, IPdfExportService pdfExportService)
        {
            _geistlichConfiguration = geistlichConfiguration.CurrentConfiguration;
            _printHeaderOnOrderResume = _geistlichConfiguration.PrintHeaderOnOrderResume;

            _printTemplateService = printTemplateService;
            _pdfTemplateService = pdfTemplateService;
            _pdfExportService = pdfExportService;

            _geistlichConfiguration.PropertyChanged += GeistlichConfigurationOnPropertyChanged;
            printerFactory.PrinterChanged += PrinterFactoryOnPrinterChanged;
            printerFactory.CreateApr331Printer().ContinueWith(HandleException);
            
        }

        private void GeistlichConfigurationOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(_geistlichConfiguration.PrintHeaderOnOrderResume) && _printHeaderOnOrderResume != _geistlichConfiguration.PrintHeaderOnOrderResume)
            {
                _printHeaderOnOrderResume = _geistlichConfiguration.PrintHeaderOnOrderResume;
            }
        }

        private void HandleException(Task<IApr331Printer> task)
        {
            if (task.Exception != null)
            {
                Logger.ErrorEx(task.Exception.Message, SourceClass, task.Exception);
            }
            else
            {
                _currentPrinter = task.Result;
            }
        }

        private void PrinterFactoryOnPrinterChanged(IApr331Printer obj)
        {
            _currentPrinter = obj;
        }


        public async Task PrintHeader(Order order)
        {
            // der befehl wird nur ausgelöst, wenn auf die Prozess seite navigiert wird.
            // sind schon messungen vorhanden, muss der Prozess unterbrochen worden sein.
            var template = _printTemplateService.CreateHeaderTemplate(order);

            if (_printHeaderOnOrderResume == true && order?.MeasurementsInTolerance > 0 || order?.MeasurementsInTolerance == 0)
            {
                await _currentPrinter.Print(template);
            }
        }

        public async Task PrintOrder(Order order)
        {
            var bodytemplate = _printTemplateService.CreateBodyTemplate(order);
            await _currentPrinter.Print(bodytemplate);

        }

        public async Task ReprintCompleteRecipe(Order order)
        {
            
            var template = _printTemplateService.CreateReprint(order);
            
            await _currentPrinter.Print(template);
            
        }

        public async Task<string> CreatePdfDocument(Order order, bool isReprint)
        {
            var pdfTemplate = _printTemplateService.GetPdfTemplate(isReprint, order);
            var pdf = await _pdfTemplateService.CreatePdfDocument(pdfTemplate, order.Charge);

            return pdf;
        }

        public async Task<int> ExportPdfDocuments()
        {
            int pdfcount = await _pdfExportService.ExportPdfsToUsbDrive();

            return pdfcount;
        }

    }
}
