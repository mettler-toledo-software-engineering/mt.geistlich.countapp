﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using mt.geistlich.countapp.ymlui.Configuration;

namespace mt.geistlich.countapp.ymlui.State
{
    public class ConfigurationStore
    {

        public event Action<GeistlichConfiguration> ConfigurationChanged;
        public ConfigurationStore(GeistlichConfiguration configuration)
        {
            CurrentConfiguration = configuration;
            CurrentConfiguration.PropertyChanged += CurrentConfigurationOnPropertyChanged;
        }

        private void CurrentConfigurationOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ConfigurationChanged?.Invoke(CurrentConfiguration);
        }

        public GeistlichConfiguration CurrentConfiguration { get; }
    }
}
