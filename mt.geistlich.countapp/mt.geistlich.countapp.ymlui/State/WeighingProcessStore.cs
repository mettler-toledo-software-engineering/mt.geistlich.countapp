﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.AudioPlayBack;
using mt.geistlich.countapp.application.Services.weighing;
using mt.geistlich.countapp.ymlui.Configuration;
using mt.geistlich.countapp.ymlui.Converter;
using mt.geistlich.countapp.ymlui.Infrastructure;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.geistlich.countapp.ymlui.State
{
    [InjectionBehavior(IsSingleton = true)]
    public class WeighingProcessStore
    {
        private IWeighingProcessService _weighingProcessService;
        
        private readonly MessageStore _messageStore;
        private readonly OrderStore _orderStore;
        private readonly MeasurementStore _measurementStore;
        private readonly IAudioPlayBackService _audioPlayBackService;
        
        private readonly GeistlichConfiguration _geistlichConfiguration;
        
  
        public WeighingProcessStore(MessageStore messageStore, OrderStore orderStore, MeasurementStore measurementStore, IScaleService scaleService, IWeighingProcessFactory weighingProcessFactory, IAudioPlayBackService audioPlayBackService, ConfigurationStore configurationStore)
        {
            _weighingProcessService = weighingProcessFactory.WeighingProcessService;
            weighingProcessFactory.WeighingProcessChanged += WeighingProcessFactoryOnWeighingProcessChanged;
            _messageStore = messageStore;

            _orderStore = orderStore;
            _measurementStore = measurementStore;
            _audioPlayBackService = audioPlayBackService;
            _geistlichConfiguration = configurationStore.CurrentConfiguration;
            
            
            
            _measurementStore.MeasurementCreated += MeasurementStoreOnMeasurementCreated;

            scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewChangedWeightInDisplayUnit;
            
            SubscribeToProcessEvents();
            
        }


        private void WeighingProcessFactoryOnWeighingProcessChanged(IWeighingProcessService newWeighingProcessService)
        {
            _weighingProcessService.DoMeasurement -= WeighingProcessServiceOnDoMeasurement;
            _weighingProcessService.CountDownChanged -= WeighingProcessServiceOnCountDownChanged;
            _weighingProcessService.StateChanged -= WeighingProcessServiceOnStateChanged;

            _weighingProcessService = newWeighingProcessService;


            SubscribeToProcessEvents();
        }

        private void SubscribeToProcessEvents()
        {
            _weighingProcessService.DoMeasurement += WeighingProcessServiceOnDoMeasurement;
            _weighingProcessService.CountDownChanged += WeighingProcessServiceOnCountDownChanged;
            _weighingProcessService.StateChanged += WeighingProcessServiceOnStateChanged;
        }


        private void WeighingProcessServiceOnDoMeasurement(WeightInformation stableweight)
        {


            if (stableweight.ConvertWeightToMg() < _orderStore.CurrentOrder.Recipe.LowerTolerance)
            {
                _messageStore.UpdateMessage(new Message(Messages.UnderTolerance, MessageColors.Yellow, true));
                _weighingProcessService.MeasurementDone();
                if (_geistlichConfiguration.PlaySoundOutOfRange)
                {
                    _audioPlayBackService.PlaySound();
                }
                
                return;
            }

            if (stableweight.ConvertWeightToMg() > _orderStore.CurrentOrder.Recipe.UpperTolerance)
            {
                _messageStore.UpdateMessage(new Message(Messages.OverTolerance, MessageColors.Red, true));
                _weighingProcessService.MeasurementDone();
                if (_geistlichConfiguration.PlaySoundOutOfRange)
                {
                    _audioPlayBackService.PlaySound();
                }
                
                return;
            }
            try
            {
                Measurement measurement = new Measurement();
                measurement.NetWeight = stableweight.ConvertWeightToMg();
                measurement.Order = _orderStore.CurrentOrder;
                measurement.TimeStamp = DateTime.Now;

                _measurementStore.CreateMeasurement(measurement).ContinueWith(HandleCreateMeasurementException);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void HandleCreateMeasurementException(Task task)
        {
            if(task.Exception != null)
            {
                _messageStore.UpdateMessage(new Message(Messages.ErrorMeasurement, MessageColors.Red, true));
            }
        }

        private async void MeasurementStoreOnMeasurementCreated(Measurement obj)
        {
            try
            {
                await _orderStore.UpdateCurrentOrder(obj.NetWeight);
            }
            catch (Exception)
            {
                _messageStore.UpdateMessage(new Message(Messages.ErrorMeasurement, MessageColors.Red, true));

            }
            _messageStore.UpdateMessage(new Message(Messages.MeasurementDone, MessageColors.Green, true));
            _weighingProcessService.MeasurementDone();
            _audioPlayBackService.PlaySound();
        }

        private void WeighingProcessServiceOnStateChanged(Message message)
        {
            _messageStore.UpdateMessage(message);
        }

        private void WeighingProcessServiceOnCountDownChanged(double countdown)
        {
            _messageStore.UpdateCountDown(countdown);
        }

        private void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            _weighingProcessService.Weight = weight;
        }

        public void StartweighingProcess()
        {
            _weighingProcessService.StartProcess();
            _messageStore.UpdateMessage(new Message(Messages.ScaleReady, MessageColors.LightBlue, true));
        }
        public void StopweighingProcess()
        {
            _weighingProcessService.FinishProcess();
            _messageStore.UpdateMessage(new Message(Messages.ProcessFinished, MessageColors.Green, false));
        }

    }
}
