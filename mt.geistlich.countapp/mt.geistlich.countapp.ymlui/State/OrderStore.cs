﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.DataAccess;
using mt.geistlich.countapp.application.Services.weighing;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.ymlui.State
{
    [InjectionBehavior(IsSingleton = true)]
    public class OrderStore
    {
        private readonly IOrderDataAccess _orderData;
        private readonly ICalculateOrderStatistikService _calculateOrderStatistik;
        private Lazy<Task> _loadOrdersLazy;
        public event Action<Order> CurrentOrderChanged;
        public event Action<Order> OrderCreated;
        public event Action<Order> OrderFinished;
        public event Action OrdersLoaded; 

        public OrderStore(IOrderDataAccess orderData, ICalculateOrderStatistikService calculateOrderStatistik)
        {
            _orderData = orderData;
            _calculateOrderStatistik = calculateOrderStatistik;
            _loadOrdersLazy = CreateOrdersLazy();
        }

        public Order CurrentOrder { get; private set; }



        private Lazy<Task> CreateOrdersLazy()
        {
            return new Lazy<Task>(() => InitializeOrdersLazy());
        }

        private async Task InitializeOrdersLazy()
        {
            IEnumerable<Order> orders = await _orderData.GetAllOrders();
            CurrentOrder = orders.FirstOrDefault();

            OrdersLoaded?.Invoke();
        }

        public async Task CreateOrder(Order order)
        {
            var result = await _orderData.CreateOrder(order);
            CurrentOrder = result;

            OrderCreated?.Invoke(result);
        }

        public async Task UpdateCurrentOrder(double newWeight)
        {

            if (newWeight > CurrentOrder.MaxWeight)
            {
                CurrentOrder.MaxWeight = newWeight;
            }

            if (newWeight < CurrentOrder.MinWeight || CurrentOrder.MinWeight == 0)
            {
                CurrentOrder.MinWeight = newWeight;
            }

            CurrentOrder.MeasurementsInTolerance++;

            await _orderData.UpdateOrder(CurrentOrder);
            CurrentOrderChanged?.Invoke(CurrentOrder);

        }

        public async Task<Order> FinishCurrentOrder()
        {
            CurrentOrder = await _calculateOrderStatistik.CalculateStatistik(CurrentOrder);
            OrderFinished?.Invoke(CurrentOrder);

            return CurrentOrder;
        }

        public async Task DeleteSelectedOrder()
        {
            var result = await _orderData.DeleteOrder(CurrentOrder.Id);

            if (result == true)
            {
                CurrentOrder = null;
            }
        }

        public async Task LoadData()
        {
            await _loadOrdersLazy.Value;
        }


    }
}
