﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.DataAccess;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.ymlui.State
{
    [InjectionBehavior(IsSingleton = true)]
    public class RecipeStore
    {
        private readonly IRecipeDataAccess _recipeData;
        private Lazy<Task> _loadRecipesLazy;
        private List<Recipe> _recipes;
        public IEnumerable<Recipe> Recipes => _recipes;
        public event Action<Recipe> CurrentRecipeChanged;
        public event Action<Recipe> RecipeCreated;
        public event Action RecipesLoaded;
        public event Action RecipeDeleted;

        public RecipeStore(IRecipeDataAccess recipeData)
        {
            _recipeData = recipeData;
            _recipes = new List<Recipe>();
            _loadRecipesLazy = CreateRecipesLazy();
        }
        
        public Recipe CurrentRecipe  { get; private set; }

        public void SetCurrentRecipe(Recipe recipe)
        {
            CurrentRecipe = recipe;
            CurrentRecipeChanged?.Invoke(CurrentRecipe);
        }

        public async Task UpdateRecipe(Recipe recipe)
        {
            await _recipeData.UpdateRecipe(recipe);
            await RefreshRecipes();
        }

        private Lazy<Task> CreateRecipesLazy()
        {
            return new Lazy<Task>(()=> InitializeRecipesLazy());
        }

        private async Task InitializeRecipesLazy()
        {
            IEnumerable<Recipe> recipes = await _recipeData.GetAllRecipes();
            _recipes.Clear();
            _recipes.AddRange(recipes);

            RecipesLoaded?.Invoke();
        }

        public async Task CreateRecipe(Recipe recipe)
        {
            var result = await _recipeData.CreateRecipe(recipe);
            _recipes.Add(result);

            RecipeCreated?.Invoke(result);
        }

        public async Task DeleteRecipe(int recipeId)
        {
            var result = await _recipeData.DeleteRecipe(recipeId);

            if (result == true)
            {
                await RefreshRecipes();
                CurrentRecipe = null;

                RecipeDeleted?.Invoke();
            }
        }

        public async Task LoadData()
        {
            await _loadRecipesLazy.Value;
        }

        public async Task RefreshRecipes()
        {
            _loadRecipesLazy = CreateRecipesLazy();
            await LoadData();
        }
    }
}
