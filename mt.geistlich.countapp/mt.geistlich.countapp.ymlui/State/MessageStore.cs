﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.geistlich.countapp.application.Models;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.ymlui.State
{
    [InjectionBehavior(IsSingleton = true)]
    public class MessageStore
    {
        public event Action<Message> NewWeightMessage;
        public event Action<Message> NewTextMessage;
        public event Action<double> CountDown; 
        public Message CurrentMessage { get; private set; }

        public void UpdateMessage(Message message)
        {
            CurrentMessage = message;
            
            NewTextMessage?.Invoke(message);
            if (message.WithWeightWindow)
            {
                NewWeightMessage?.Invoke(CurrentMessage);
            }
        }


        public void UpdateCountDown(double countdown)
        {
            CountDown?.Invoke(countdown);
        }
    }
}
