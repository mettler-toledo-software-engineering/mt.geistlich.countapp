﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.Authentification;
using mt.geistlich.countapp.ymlui.Configuration;
using mt.geistlich.countapp.ymlui.Infrastructure;
using MT.Singularity.Composition;
using MT.Singularity.Platform.UserManagement;

namespace mt.geistlich.countapp.ymlui.State
{
    [InjectionBehavior(IsSingleton = true)]
    public class UserStore
    {
        private IAutoLogoutService _autoLogoutService;
        private readonly ISecurityService _securityService;
        private readonly ConfigurationStore _configurationStore;

        private readonly OperatorUser _geistlichOperatorUser;
        private readonly AdminUser _geistlichAdmin;

        public event Action UserLoggedOut;
        public event Action UserLoggedIn;

        public UserStore(IAutoLogoutServiceFactory logoutServiceFactory, ISecurityService securityService, ConfigurationStore configurationStore)
        {
            _autoLogoutService = logoutServiceFactory.CreateAutoLogoutService();
            logoutServiceFactory.LogoutServiceChanged += LogoutServiceFactoryOnLogoutServiceChanged;
            _securityService = securityService;
            _configurationStore = configurationStore;

            _geistlichAdmin = new AdminUser {UserName = _configurationStore.CurrentConfiguration.GeistlichAdminName};
            _geistlichOperatorUser = new OperatorUser {UserName = _configurationStore.CurrentConfiguration.GeistlichUserName};

            _configurationStore.ConfigurationChanged += ConfigurationStoreOnConfigurationChanged;
            _securityService.CurrentUserChanged += SecurityServiceOnCurrentUserChanged;
            _autoLogoutService.LogOutEvent += AutoLogoutServiceOnLogOutEvent;

            SetLoggedInUser(_securityService.CurrentUser);
        }

        private void LogoutServiceFactoryOnLogoutServiceChanged(IAutoLogoutService logoutService)
        {
            _autoLogoutService.LogOutEvent -= AutoLogoutServiceOnLogOutEvent;
            _autoLogoutService = logoutService;
            _autoLogoutService.LogOutEvent += AutoLogoutServiceOnLogOutEvent;
        }

        private void ConfigurationStoreOnConfigurationChanged(GeistlichConfiguration configuration)
        {


            if (configuration.GeistlichUserName != _geistlichOperatorUser.UserName)
            {
                _geistlichOperatorUser.UserName = configuration.GeistlichUserName;
            }
            if (configuration.GeistlichAdminName != _geistlichAdmin.UserName)
            {
                _geistlichAdmin.UserName = configuration.GeistlichAdminName;
            }
        }

        private void SetLoggedInUser(User user)
        {
            if (user.Name == _geistlichAdmin.UserName)
            {
                LoggedInUser = _geistlichAdmin;
                _autoLogoutService.UserLoggedIn();
                UserLoggedIn?.Invoke();

            } else if (user.Name == _geistlichOperatorUser.UserName)
            {
                LoggedInUser = _geistlichOperatorUser;
                _autoLogoutService.UserLoggedIn();
                UserLoggedIn?.Invoke();
            }
            else
            {
                LoggedInUser = null;
                UserLoggedOut?.Invoke();
            }
        }

        private void SecurityServiceOnCurrentUserChanged(User user)
        {
            SetLoggedInUser(user);
        }

        private void AutoLogoutServiceOnLogOutEvent(object sender, EventArgs e)
        {
            LoggedInUser = null;
            _securityService.LogOutAsync();
            UserLoggedOut?.Invoke();

        }

        public void StopLogoffTimer()
        {
            _autoLogoutService.StopLogoffTimer();
        }

        public void ResetLogoffTimer()
        {
            _autoLogoutService.ResetLogOffTimer();
        }

        public void RestartLogoffTimer()
        {
            _autoLogoutService.RestartLogoffTimer();
        }
        

        public IUser LoggedInUser { get; private set; }
    }
}
