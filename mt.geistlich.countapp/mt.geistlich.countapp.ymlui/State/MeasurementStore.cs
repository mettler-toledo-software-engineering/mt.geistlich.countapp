﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.DataAccess;
using MT.Singularity.Composition;

namespace mt.geistlich.countapp.ymlui.State
{
    [InjectionBehavior(IsSingleton = true)]
    public class MeasurementStore
    {
        private readonly IMeasurementDataAccess _measurementData;
        public event Action<Measurement> MeasurementCreated;
        

        public MeasurementStore(IMeasurementDataAccess measurementdata)
        {
            _measurementData = measurementdata;
        }


        
        
        public async Task CreateMeasurement(Measurement measurement)
        {
            var result = await _measurementData.CreateMeasurement(measurement);

            MeasurementCreated?.Invoke(result);
        }


        public async Task DeleteMeasurementsByOrderId(int orderId)
        {
            var result = await _measurementData.DeleteAllMeasurementsByOrder(orderId);

        }

        public async Task DeleteAllMeasurements(int orderId)
        {
            var result = await _measurementData.DeleteAllMeasureMents();
        }


    }
}
