﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.Infrastructure;
using MT.Singularity.Presentation.Drawing;

namespace mt.geistlich.countapp.ymlui.Converter
{
    public static class EnumConverter
    {
        public static SolidColorBrush ConvertToSolidColorBrush(this MessageColors messageColors)
        {
            switch (messageColors)
            {
                case MessageColors.Red:
                    return Globals.RedBrush;
                case MessageColors.Yellow:
                    return Globals.YellowBrush;
                case MessageColors.Green:
                    return Globals.GreenBrush;
                case MessageColors.LightBlue:
                    return Globals.LightBlueBrush;
                default:
                    throw new ArgumentOutOfRangeException(nameof(messageColors), messageColors, null);
            }
        }

        public static string ConvertToString(this Messages messages)
        {
            switch (messages)
            {
                case Messages.ScaleReady: return Localization.Get(Localization.Key.MessageScaleReady);
                case Messages.ScaleWaiting: return Localization.Get(Localization.Key.MessageScaleWaiting);
                case Messages.NextMeasurementIn: return Localization.Get(Localization.Key.MessageNextMeasurementIn);
                case Messages.MeasurementDone: return Localization.Get(Localization.Key.MessageMeasurementDone);
                case Messages.OverTolerance: return Localization.Get(Localization.Key.MessageOverTolerance);
                case Messages.UnderTolerance: return Localization.Get(Localization.Key.MessageUnderTolerance);
                case Messages.PdfSaved: return Localization.Get(Localization.Key.MessagePdfSaved);
                case Messages.ProcessFinished: return Localization.Get(Localization.Key.MessageProcessFinished);
                case Messages.ErrorPdf: return Localization.Get(Localization.Key.ErrorMessagePDFCreationFailed);
                case Messages.ErrorPrint: return Localization.Get(Localization.Key.ErrorMessagePrintingFailed);
                case Messages.ErrorMeasurement: return Localization.Get(Localization.Key.ErrorMessageMeasurementFailed);
                case Messages.PrintFinished: return Localization.Get(Localization.Key.MessagePrintFinished);
                case Messages.PleaseWait: return Localization.Get(Localization.Key.MessagePleaseWait);


                default:
                    throw new ArgumentOutOfRangeException(nameof(messages), messages, null);
            }
        }
    }
}
