﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.geistlich.countapp.ymlui.Converter
{
    public static class WeightToMgConverter
    {
        public static double ConvertWeightToMg(this WeightInformation weight)
        {
            if (weight.Unit == WellknownWeightUnit.Kilogram && weight.IsValid)
            {
                return Math.Round(weight.NetWeight * 1000, 2);
            }
            if (weight.Unit == WellknownWeightUnit.Gram && weight.IsValid)
            {
                return Math.Round(weight.NetWeight * 1000, 2);
            }
            if (weight.Unit == WellknownWeightUnit.Milligram && weight.IsValid)
            {
                return Math.Round(weight.NetWeight);
            }
            else
            {
                return 0;
            }
        }
    }
}
