﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.Converter;
using mt.geistlich.countapp.ymlui.Infrastructure;
using mt.geistlich.countapp.ymlui.State;
using MT.Singularity;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Drawing;

namespace mt.geistlich.countapp.ymlui.ViewModels
{
    public class WeightWindowViewModel : ViewModelBase
    {
        private readonly MessageStore _messageStore;
        
        public WeightWindowViewModel(MessageStore messageStore, IScaleService scaleService) : base(scaleService)
        {
            //BackGround = MessageColors.LightBlue.ConvertToSolidColorBrush();
            _messageStore = messageStore;
            
            _messageStore.NewWeightMessage += MessageStoreOnNewMessage;
            scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewChangedWeightInDisplayUnit;
        }

        protected override void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            WeightInMg = $"{weight.ConvertWeightToMg()}mg";
        }

        private void MessageStoreOnNewMessage(Message message)
        {
            BackGround = message.MessageColor.ConvertToSolidColorBrush();
        }



        private string _weightInMg;

        public string WeightInMg
        {
            get { return _weightInMg; }
            set
            {
                _weightInMg = value;
                NotifyPropertyChanged(nameof(WeightInMg));
            }
        }


        public override void Dispose()
        {
            _messageStore.NewWeightMessage -= MessageStoreOnNewMessage;
        }
    }
}
