﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.Commands;
using mt.geistlich.countapp.ymlui.State;
using MT.Singularity;
using MT.Singularity.Presentation.Model;

namespace mt.geistlich.countapp.ymlui.ViewModels
{
    public class PickRecipeViewModel : ViewModelBase
    {
        private readonly RecipeStore _recipeStore;

        public ICommand ScrollUpCommand { get; }
        public ICommand ScrollDownCommand { get; }
        public ICommand SelectRecipeCommand { get; }
        public ICommand GoToWeighingProcessViewCommand { get; }
        public int MaxNumberOfElements => 15;
        public int NumberOfElements => (int) (AllRecipes?.Count < PanelIndex + MaxNumberOfElements ? AllRecipes?.Count - PanelIndex : MaxNumberOfElements);
        public int PanelIndex { get; set; } = 0;

        public PickRecipeViewModel(RecipeStore recipeStore, OrderStore orderStore)
        {
            _recipeStore = recipeStore;
            ScrollUpCommand = new ScrollUpRecipePanelCommand(this);
            ScrollDownCommand = new ScrollDownRecipePanelCommand(this);
            SelectRecipeCommand = new SelectRecipeCommand(this);
            GoToWeighingProcessViewCommand = new GoToWeighingProcessViewCommand(this, orderStore);

            _recipeStore.RecipesLoaded += RecipeStoreOnRecipesLoaded;
            _recipeStore.RecipeCreated += RecipeStoreOnRecipesCreated;
            _recipeStore.RecipeDeleted += RecipeStoreOnRecipeDeleted;

            RecipeStoreOnRecipesLoaded();

        }

        private void RecipeStoreOnRecipeDeleted()
        {
            AllRecipes = new ObservableCollection<Recipe>(_recipeStore.Recipes);
        }

        private void RecipeStoreOnRecipesCreated(Recipe recipe)
        {
            AllRecipes.Insert(0, recipe);
        }

        private void RecipeStoreOnRecipesLoaded()
        {

            AllRecipes = new ObservableCollection<Recipe>(_recipeStore.Recipes);

            Recipes = new ObservableCollection<Recipe>(AllRecipes.ToList().GetRange(PanelIndex,NumberOfElements));

        }

        public override void UpdateProperties()
        {
            _recipeStore.LoadData().ContinueWith(HandleError);
            SelectedRecipe = null;
        }

        private void HandleError(Task obj)
        {
            if (obj.Exception != null)
            {
                //todo error handling
            }
        }

        public override void UnregisterEventsForViewModel()
        {
            Dispose();
        }

        private ObservableCollection<Recipe> _recipes;

        public ObservableCollection<Recipe> Recipes
        {
            get { return _recipes; }
            set
            {
                _recipes = value;
                NotifyPropertyChanged(nameof(Recipes));
            }
        }

        public ObservableCollection<Recipe> AllRecipes { get; set; } = new ObservableCollection<Recipe>();





        private Recipe _selectedRecipe;

        public Recipe SelectedRecipe
        {
            get { return _selectedRecipe; }
            set
            {
                _selectedRecipe = value;
                NotifyPropertyChanged(nameof(SelectedRecipe));
                NotifyPropertyChanged(nameof(SelectedRecipeNumber));

            }
        }

        private string _recipeNumber;

        public string RecipeNumber
        {
            get { return _recipeNumber; }
            set
            {
                _recipeNumber = value;
                NotifyPropertyChanged(nameof(RecipeNumber));
            }
        }

        private string _charge;

        public string Charge
        {
            get { return _charge; }
            set
            {
                _charge = value;
                NotifyPropertyChanged(nameof(Charge));
            }
        }

        public string SelectedRecipeNumber => SelectedRecipe == null ? "bitte Rezept wählen." : SelectedRecipe.RecipeNumber;

        public override void Dispose()
        {
            _recipeStore.RecipesLoaded -= RecipeStoreOnRecipesLoaded;
            _recipeStore.RecipeCreated -= RecipeStoreOnRecipesCreated;
            _recipeStore.RecipeDeleted -= RecipeStoreOnRecipeDeleted;
        }
    }
}
