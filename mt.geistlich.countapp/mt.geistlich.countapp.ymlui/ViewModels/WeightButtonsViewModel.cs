﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.geistlich.countapp.ymlui.Commands;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Model;

namespace mt.geistlich.countapp.ymlui.ViewModels
{
    public class WeightButtonsViewModel : ViewModelBase
    {

        public WeightButtonsViewModel(IScaleService scaleService)
        {
            TareCommand = new TareCommand(scaleService);
            ClearTareCommand = new ClearTareCommand(scaleService);
            ZeroCommand = new ZeroCommand(scaleService);
        }
        
        public ICommand ClearTareCommand { get; }
        public ICommand TareCommand { get; }
        public ICommand ZeroCommand { get; }

        public override void Dispose()
        {
            
        }
    }


}
