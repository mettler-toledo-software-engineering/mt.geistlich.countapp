﻿using System;

using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.application.Services.weighing;
using mt.geistlich.countapp.ymlui.Commands;
using mt.geistlich.countapp.ymlui.Infrastructure;
using mt.geistlich.countapp.ymlui.State;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Model;

namespace mt.geistlich.countapp.ymlui.ViewModels
{
    public class WeighingProcessViewModel : ViewModelBase
    {
        public ICommand FinishWeighingProcessCommand { get; }
        public ICommand LeaveWeighingProcessCommand { get; }
        public ICommand StartWeighingProcessCommand { get; }
        public ICommand ReprintCommand { get; }
        public ICommand CreateTestDataCommand { get; }

        private readonly OrderStore _orderStore;

        
        public WeighingProcessViewModel(OrderStore orderStore, MeasurementStore measurementStore, MessageStore messageStore, WeighingProcessStore weighingProcessStore, PrinterStore printerStore)
        {
            
            FinishWeighingProcessCommand = new FinishWeighingProcessCommand(this, messageStore, weighingProcessStore, printerStore, orderStore);
            LeaveWeighingProcessCommand = new LeaveWeighingProcessCommand(this, orderStore, measurementStore, printerStore);
            StartWeighingProcessCommand = new StartWeighingProcessComnmand(orderStore, weighingProcessStore, printerStore);
            CreateTestDataCommand = new CreateTestDataCommand(orderStore, measurementStore);
            ReprintCommand = new ReprintCommand(printerStore, orderStore, messageStore);
            _orderStore = orderStore;
            _orderStore.CurrentOrderChanged += OrderStoreOnCurrentOrderChanged;
            
        }

        private void OrderStoreOnCurrentOrderChanged(Order obj)
        {
            MeasurementCount = _orderStore.CurrentOrder.MeasurementsInTolerance;
        }

        public override void UpdateProperties()
        {
            ProcessFinishedVisibility = Visibility.Collapsed;
            ProcessOngoingVisibility = Visibility.Visible;
            Recipe = _orderStore.CurrentOrder?.Recipe;
            Charge = _orderStore.CurrentOrder?.Charge;
            MeasurementCount = _orderStore.CurrentOrder?.MeasurementsInTolerance ?? 0;
            StartWeighingProcessCommand.Execute(null);
        }


        private Recipe _recipe;

        public Recipe Recipe
        {
            get { return _recipe; }
            set
            {
                _recipe = value;
                NotifyPropertyChanged(nameof(Recipe));
            }
        }

        private string _charge;

        public string Charge
        {
            get { return _charge; }
            set
            {
                _charge = value; 
                NotifyPropertyChanged(nameof(Charge));
            }
        }

        private int _measurementCount;

        public int MeasurementCount
        {
            get { return _measurementCount; }
            set
            {
                _measurementCount = value;
                NotifyPropertyChanged(nameof(MeasurementCount));
            }
        }

        private Visibility _processFinishedVisibility;

        public Visibility ProcessFinishedVisibility
        {
            get { return _processFinishedVisibility; }
            set
            {
                _processFinishedVisibility = value;
                NotifyPropertyChanged(nameof(ProcessFinishedVisibility));
                
            }
        }

        private Visibility _processOngoingVisibility;

        public Visibility ProcessOngoingVisibility
        {
            get { return _processOngoingVisibility;}
            set
            {
                _processOngoingVisibility = value;
                NotifyPropertyChanged(nameof(ProcessOngoingVisibility));
            }
        }

        public override void Dispose()
        {
            _orderStore.CurrentOrderChanged -= OrderStoreOnCurrentOrderChanged;
        }
    }
}
