﻿using System;

using System.Collections.Generic;
using System.Drawing.Printing;
using System.Text;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.Converter;
using mt.geistlich.countapp.ymlui.Infrastructure;
using mt.geistlich.countapp.ymlui.State;
using MT.Singularity.Presentation.Drawing;

namespace mt.geistlich.countapp.ymlui.ViewModels
{
    public class MessageWindowViewModel : ViewModelBase
    {
        private readonly MessageStore _messageStore;

        private Message _countdownmessage;
        public MessageWindowViewModel(MessageStore messageStore)
        {
            _messageStore = messageStore;
            _messageStore.NewTextMessage += MessageStoreOnNewMessage;
            _messageStore.CountDown += MessageStoreOnCountDown;
            MessageText = Message.DefaultMessage().MessageText.ConvertToString();
            BackGround = Message.DefaultMessage().MessageColor.ConvertToSolidColorBrush();
        }

        private void MessageStoreOnCountDown(double countdown)
        {
            
            MessageText = $"{_countdownmessage.MessageText.ConvertToString()} {countdown} Sek.";
            NotifyPropertyChanged(nameof(MessageText));
        }

        private void MessageStoreOnNewMessage(Message message)
        {
            if (message.MessageText == Messages.NextMeasurementIn)
            {
                _countdownmessage = message;
                
            }
            else
            {
                MessageText = message.MessageText.ConvertToString();
            }
            
            BackGround = message.MessageColor.ConvertToSolidColorBrush();
        }

        

        private string _message;

        public string MessageText
        {
            get { return _message; }
            private set
            {
                _message = value;
                NotifyPropertyChanged(nameof(MessageText));
            }
        }



        private SolidColorBrush _backGround;

        public SolidColorBrush BackGround
        {
            get { return _backGround; }
            private set
            {
                _backGround = value;
                NotifyPropertyChanged(nameof(BackGround));
            }
        }

        public override void Dispose()
        {
            _messageStore.NewTextMessage -= MessageStoreOnNewMessage;
        }
    }
}
