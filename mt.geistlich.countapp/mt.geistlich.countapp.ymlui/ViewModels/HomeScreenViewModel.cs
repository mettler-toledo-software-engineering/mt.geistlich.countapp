﻿// ReSharper disable once RedundantUsingDirective


using System.Diagnostics;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Services.AudioPlayBack;
using mt.geistlich.countapp.application.Services.DataExport;
using mt.geistlich.countapp.peripherals.Usb;
using mt.geistlich.countapp.ymlui.Commands;
using mt.geistlich.countapp.ymlui.Infrastructure;
using mt.geistlich.countapp.ymlui.State;
using MT.Singularity.Presentation.Model;

namespace mt.geistlich.countapp.ymlui.ViewModels
{
    /// <summary>
    /// Home Screen view model implementation
    /// </summary>
    public class HomeScreenViewModel : ViewModelBase
    {
        private readonly UserStore _userStore;
        private readonly IUsbEventProvider _usbEvent;
        public ICommand GoToManageRecipesViewCommand { get; }
        public ICommand GoToPickRecipeViewCommand { get; }
        public ICommand ResetLogoffTimerCommand { get; }
        public ICommand RestartLogoffTimerCommand { get; }
        public ICommand StopLogoffTimerCommand { get; }
        public ICommand ResumeUnfinishedOrderCommand { get; }
        public ICommand ExportPdfsCommand { get; }
        /// <summary>
        /// Constructor Initializes a new instance of the <see cref="HomeScreenViewModel"/> class.
        /// </summary>
        public HomeScreenViewModel(UserStore userStore, OrderStore orderStore, IPdfExportService pdfExportService, IUsbEventProvider usbEventProvider)
        {
            _userStore = userStore;
            _usbEvent = usbEventProvider;
        
            ResetLogoffTimerCommand = new ResetLogoffTimerCommand(userStore);
            RestartLogoffTimerCommand = new RestartLogoffTImerCommand(userStore);
            StopLogoffTimerCommand = new StopLogoffTimerCommand(userStore);
            GoToManageRecipesViewCommand = new GoToManageRecipesViewCommand(this, userStore);
            GoToPickRecipeViewCommand = new GoToPickRecipeViewCommand(this, userStore);
            ResumeUnfinishedOrderCommand = new ResumeUnfinishedOrderCommand(this, orderStore);
            ExportPdfsCommand = new ExportPdfsCommand(this, pdfExportService);
        }


        public override void RegisterEventsForViewModel()
        {
            _userStore.UserLoggedIn += UserStoreOnUserLoggedIn;
            _usbEvent.DeviceInsertedEvent += UsbEventOnDeviceInsertedEvent;
            
        }

        private void UsbEventOnDeviceInsertedEvent(string driveletter)
        {
            ExportPdfsCommand.Execute(null);
        }

        private void UserStoreOnUserLoggedIn()
        {
            ResumeUnfinishedOrderCommand.Execute(null);
        }

        public override void Dispose()
        {
            
        }

        public override void UnregisterEventsForViewModel()
        {
            _userStore.UserLoggedIn -= UserStoreOnUserLoggedIn;
        }

        public string Version => $"Programmversion: {Globals.ProgVersionStr(false)}";
        public string ProjectNumber => $"Projektnummer: {Globals.ProjectNumber}";
    }
}
