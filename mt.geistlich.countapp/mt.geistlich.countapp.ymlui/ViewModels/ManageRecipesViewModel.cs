﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using mt.geistlich.countapp.application.Models;
using mt.geistlich.countapp.ymlui.Commands;
using mt.geistlich.countapp.ymlui.State;
using mt.geistlich.countapp.ymlui.Views;
using MT.Singularity.Presentation.Model;

namespace mt.geistlich.countapp.ymlui.ViewModels
{
    public class ManageRecipesViewModel : ViewModelBase
    {
        private readonly RecipeStore _recipeStore;
        
        public ICommand ScrollUpCommand { get; }
        public ICommand ScrollDownCommand { get; }
        public ICommand CreateNewRecipeCommand { get; }
        public ICommand UpdateRecipeCommand { get; }
        public ICommand DeleteRecipeCommand { get; }

        
        public ManageRecipesViewModel(RecipeStore recipeStore)
        {
            _recipeStore = recipeStore;
            ScrollUpCommand = new ScrollUpCommand(this);
            ScrollDownCommand = new ScrollDownCommand(this);
            UpdateRecipeCommand = new UpdateRecipeCommand(recipeStore, this);
            DeleteRecipeCommand = new DeleteRecipeCommand(recipeStore, this);
            CreateNewRecipeCommand = new CreateNewRecipeCommand(recipeStore, this);

            _recipeStore.RecipesLoaded += RecipeStoreOnRecipesLoaded;
            _recipeStore.RecipeCreated += RecipeStoreOnRecipesCreated;
            _recipeStore.RecipeDeleted += RecipeStoreOnRecipeDeleted;


        }

        public override void RegisterEventsForViewModel()
        {
            if (_recipeStore.Recipes != null)
            {
                Recipes = new ObservableCollection<Recipe>(_recipeStore.Recipes);
            }
        }

        private void RecipeStoreOnRecipeDeleted()
        {
            SelectedItem = null;
        }

        private void RecipeStoreOnRecipesCreated(Recipe recipe)
        {
            Recipes.Insert(0,recipe);
            ListIndex = 0;
        }

        private void RecipeStoreOnRecipesLoaded()
        {
            Recipes = new ObservableCollection<Recipe>(_recipeStore.Recipes.OrderBy(r => r.RecipeName));
            
            if (Recipes.Count == 0)
            {
                SelectedRecipe = new Recipe();
            }
        }

        public override void UpdateProperties()
        {
            _recipeStore.LoadData().ContinueWith(HandleError);
        }

        private void HandleError(Task obj)
        {
            if (obj.Exception != null)
            {
                //todo error handling
            }
        }

        public override void UnregisterEventsForViewModel()
        {
            Dispose();
        }

        private ObservableCollection<Recipe> _recipes;

        public ObservableCollection<Recipe> Recipes
        {
            get { return _recipes; }
            set
            {
                _recipes = value;
                NotifyPropertyChanged(nameof(Recipes));
                NotifyPropertyChanged(nameof(ListCount));
            }
        }

        private int _listIndex;

        public int ListIndex
        {
            get { return _listIndex; }
            set
            {
                _listIndex = value;
                NotifyPropertyChanged(nameof(ListIndex));
            }
        }

        public int ListCount => Recipes == null ? 0 : Recipes.Count;

        private Recipe _selectedItem;

        public object SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = (Recipe) value;
                SelectedRecipe = _selectedItem == null ? new Recipe() : _selectedItem.Clone();
                NotifyPropertyChanged(nameof(SelectedItem));
            }
        }

        private Recipe _selectedRecipe;

        public Recipe SelectedRecipe
        {
            get { return _selectedRecipe; }
            set
            {
                _selectedRecipe = value;
                NotifyPropertyChanged(nameof(SelectedRecipe));
                NotifyPropertyChanged(nameof(RecipeNumber));
                NotifyPropertyChanged(nameof(RecipeName));
                NotifyPropertyChanged(nameof(UpperTolerance));
                NotifyPropertyChanged(nameof(LowerTolerance));
            }
        }

        

        public string RecipeNumber
        {
            get { return SelectedRecipe?.RecipeNumber ?? ""; }
            set
            {
                SelectedRecipe.RecipeNumber = value;
                NotifyPropertyChanged(nameof(SelectedRecipe));
                NotifyPropertyChanged(nameof(RecipeNumber));
            }
        }
        public string RecipeName
        {
            get { return SelectedRecipe?.RecipeName ?? ""; }
            set
            {
                SelectedRecipe.RecipeName = value;
                NotifyPropertyChanged(nameof(SelectedRecipe));
                NotifyPropertyChanged(nameof(RecipeName));
            }
        }

        public double UpperTolerance
        {
            get { return SelectedRecipe?.UpperTolerance ?? 0; }
            set
            {
                SelectedRecipe.UpperTolerance = value;
                NotifyPropertyChanged(nameof(SelectedRecipe));
                NotifyPropertyChanged(nameof(UpperTolerance));
            }
        }

        public double LowerTolerance
        {
            get { return SelectedRecipe?.LowerTolerance ?? 0; }
            set
            {
                SelectedRecipe.LowerTolerance = value;
                NotifyPropertyChanged(nameof(SelectedRecipe));
                NotifyPropertyChanged(nameof(LowerTolerance));

            }
        }

        public override void Dispose()
        {
            _recipeStore.RecipesLoaded -= RecipeStoreOnRecipesLoaded;
            _recipeStore.RecipeCreated -= RecipeStoreOnRecipesCreated;
            _recipeStore.RecipeDeleted -= RecipeStoreOnRecipeDeleted;
        }
    }
}
