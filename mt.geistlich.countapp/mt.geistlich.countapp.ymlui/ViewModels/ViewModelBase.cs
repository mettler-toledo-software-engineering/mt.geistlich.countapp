﻿using System;

using System.Collections.Generic;
using System.Text;
using mt.geistlich.countapp.ymlui.Infrastructure;
using MT.Singularity.Data;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Drawing;

namespace mt.geistlich.countapp.ymlui.ViewModels
{
    public abstract class ViewModelBase : ObservableObject, IDisposable
    {

        protected IScaleService _scaleService;


        public INavigationPage ParentPage { get; set; }

        private SolidColorBrush _backGround;

        public SolidColorBrush BackGround
        {
            get { return _backGround; }
            set
            {
                _backGround = value;
                NotifyPropertyChanged(nameof(BackGround));
            }
        }

        public Visual ParentVisual => (Visual)ParentPage;
        public ChildWindow ParentChildWindow { get; set; }

        protected ViewModelBase()
        {
            BackGround = Globals.LightBlueBrush;
        }

        protected ViewModelBase(IScaleService scaleService)
        {
            _scaleService = scaleService;
            BackGround = Globals.LightBlueBrush;
        }

        public virtual void RegisterEventsForViewModel()
        {
            if (_scaleService != null)
            {
                _scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScaleOnNewChangedWeightInDisplayUnit;
            }
        }



        public virtual void UnregisterEventsForViewModel()
        {
            if (_scaleService != null)
            {
                _scaleService.SelectedScale.NewChangedWeightInDisplayUnit -= SelectedScaleOnNewChangedWeightInDisplayUnit;
            }

        }

        protected virtual void SelectedScaleOnNewChangedWeightInDisplayUnit(WeightInformation weight)
        {

        }

        public virtual void UpdateProperties()
        {

        }

        public abstract void Dispose();
    }
}
