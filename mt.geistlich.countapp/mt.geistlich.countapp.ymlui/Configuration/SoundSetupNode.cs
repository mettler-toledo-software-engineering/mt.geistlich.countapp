﻿using System;

using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Collections;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;

namespace mt.geistlich.countapp.ymlui.Configuration
{
    public class SoundSetupNode : GroupSetupMenuItem
    {
        private readonly GeistlichConfiguration _configuration;
        public SoundSetupNode(SetupMenuContext context, IGeistlichComponents customerComponent, GeistlichConfiguration configuration) : base (context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.SoundSettings), configuration, customerComponent)
        {
            _configuration = configuration;
        }
        public override Task ShowChildrenAsync()
        {
            try
            {
                var playSoundOutOfRangeTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PlaySoundOutOfRange);
                var playSoundOutOfRangeTarget = new CheckBoxSetupMenuItem(_context, playSoundOutOfRangeTitle, _configuration, "PlaySoundOutOfRange");

                var rangeGroup = new GroupedSetupMenuItems(_context, playSoundOutOfRangeTarget);
                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return TaskEx.CompletedTask;
        }
    }
}
