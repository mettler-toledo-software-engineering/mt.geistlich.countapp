﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Services.SystemUpdate;
using mt.geistlich.countapp.ymlui;
using mt.geistlich.countapp.ymlui.Infrastructure;
using MT.Singularity.Collections;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;

namespace mt.geistlich.countapp.ymlui.Configuration
{
    public class GeneralSettingsSubNode : GroupSetupMenuItem
    {
        private readonly GeistlichConfiguration _configuration;
        private readonly ISystemUpdateService _systemUpdate;
        private readonly IPlatformEngine _engine;
        public GeneralSettingsSubNode(SetupMenuContext context, IGeistlichComponents geistlichComponent, GeistlichConfiguration configuration, ISystemUpdateService systemUpdate, IPlatformEngine engine) 
            : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.GeneralSettingsSubNode), configuration, geistlichComponent)
        {
            _configuration = configuration;
            _systemUpdate = systemUpdate;
            _engine = engine;
        }

        public override Task ShowChildrenAsync()
        {
            try
            {
                var geistlichUserTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.GeistlichUserName);
                var geistlichUser = new TextSetupMenuItem(_context, geistlichUserTitle, _configuration, "GeistlichUserName");

                var geistlichAdminTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.GeistlichAdminName);
                var geistlichAdmin = new TextSetupMenuItem(_context, geistlichAdminTitle, _configuration, "GeistlichAdminName");

                var stableWeightTimeTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.StableWeightTime);
                var stableWeightTime = new TextSetupMenuItem(_context, stableWeightTimeTitle, _configuration, "StableWeightTime");

                var messageDurationTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.MessageDuration);
                var messageDuration = new TextSetupMenuItem(_context, messageDurationTitle, _configuration, "MessageDuration");

                var logoutTimeTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.LogoutTime);
                var logoutTime = new TextSetupMenuItem(_context, logoutTimeTitle, _configuration, "LogoutTime");

                var printHeaderOnOrderResumeTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PrintHeaderOnOrderResume);
                var printHeaderOnOrderResume = new CheckBoxSetupMenuItem(_context, printHeaderOnOrderResumeTitle, _configuration, "PrintHeaderOnOrderResume");


                var testResultTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.TestResult);
                var testResultTarget = new TextBlockSetupMenuItem(_context, testResultTitle, _configuration, "TestResult");

                var updateButton = new ImageButtonSetupMenuItem(_context, Globals.FlashDiskImage, new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Update, 20, SystemUpdate);
                var restartButton = new ImageButtonSetupMenuItem(_context, Globals.RestartImage, new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Restart, 20, RestartSystem);

                var userGroup = new GroupedSetupMenuItems(_context, geistlichUser, geistlichAdmin, logoutTime);
                var processGroup = new GroupedSetupMenuItems(_context, stableWeightTime, messageDuration, printHeaderOnOrderResume);

                var rangeGroup3 = new GroupedSetupMenuItems(_context, testResultTarget);


                Children = Indexable.ImmutableValues<SetupMenuItem>(userGroup, processGroup, rangeGroup3, restartButton, updateButton);
            } 
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error BalancerDataServiceSubnode.ShowChildrenAsync", ex);
            }



            return TaskEx.CompletedTask;
        }

        private void RestartSystem()
        {
            _engine.RebootAsync();
        }

        private void SystemUpdate()
        {


            var success = _systemUpdate.ExecuteUpdate();

            if (success == true)
            {
                _configuration.TestResult = "Update copied.";
            }
            else
            {
                _configuration.TestResult = "Update failed!";
            }
        }
    }
}
