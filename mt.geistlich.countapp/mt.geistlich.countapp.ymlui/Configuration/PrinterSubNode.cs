﻿using System;

using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Collections;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;

namespace mt.geistlich.countapp.ymlui.Configuration
{
    public class PrinterSubNode : GroupSetupMenuItem
    {
        private readonly GeistlichConfiguration _configuration;

        public PrinterSubNode(SetupMenuContext context, IGeistlichComponents customerComponent, GeistlichConfiguration configuration)
            : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PrinterSubNode), configuration, customerComponent)
        {
            _configuration = configuration;
        }

        public override Task ShowChildrenAsync()
        {
            try
            {
                var printerPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PrinterPort);
                var printerPortTarget = new TextSetupMenuItem(_context, printerPortTitle, _configuration, "PrinterPort");

                // Titles
                var apr331KontrastTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.APR331Kontrast);
                var apr331WiderstandTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.APR331Widerstand);

                // values
                var apr331KontrastTarget = new DropDownSetupMenuItem(_context, apr331KontrastTitle, _configuration, "KontrastItems", "Apr331Kontrast");
                var apr331WiderstandTarget = new DropDownSetupMenuItem(_context, apr331WiderstandTitle, _configuration, "WiderstandItems", "Apr331Widerstand");

                var rangeGroup = new GroupedSetupMenuItems(_context, printerPortTarget, apr331KontrastTarget, apr331WiderstandTarget);
                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }

            return TaskEx.CompletedTask;
        }
    }
}
