﻿using System;

using System.Collections.Generic;
using System.Text;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;

namespace mt.geistlich.countapp.ymlui.Configuration
{
    [Export(typeof(IGeistlichComponents))]
    [Export(typeof(IConfigurable))]
    [InjectionBehavior(IsSingleton = true)]
    public class GeistlichComponents : ConfigurationStoreConfigurable<GeistlichConfiguration>, IGeistlichComponents
    {

        public GeistlichComponents(IConfigurationStore configurationStore, ISecurityService securityService, CompositionContainer compositionContainer, bool traceChanges = true)
            : base(configurationStore, securityService, compositionContainer, traceChanges)
        {
        }

        public override string ConfigurationSelector
        {
            get { return "GeistlichConfiguration"; }
        }

        public override string FriendlyName
        {
            get { return "Geistlich Configuration"; }
        }

    }
}
