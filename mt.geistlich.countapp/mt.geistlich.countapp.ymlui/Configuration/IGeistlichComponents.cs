﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MT.Singularity.Platform.Configuration;

namespace mt.geistlich.countapp.ymlui.Configuration
{
    public interface IGeistlichComponents : IConfigurable<GeistlichConfiguration>
    {
    }
}
