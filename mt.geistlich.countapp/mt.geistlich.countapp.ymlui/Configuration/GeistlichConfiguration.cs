﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using MT.Singularity.Collections;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;

namespace mt.geistlich.countapp.ymlui.Configuration
{
    [Component]
    public class GeistlichConfiguration : ComponentConfiguration
    {
        public GeistlichConfiguration()
        {
            KontrastItems = Apr331KonstrastItemStrings;
            WiderstandItems = Apr331WiderstandItemStrings;
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string TestResult
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }


        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(3)]
        public virtual int PrinterPort
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }



        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("gp Operator")]
        public virtual string GeistlichUserName
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("gp Admin")]
        public virtual string GeistlichAdminName
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(15)]
        public virtual int LogoutTime
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }


        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(2.0)]
        public virtual double StableWeightTime
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(1.0)]
        public virtual double MessageDuration
        {
            get { return (double)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("4")]
        public virtual string Apr331Kontrast
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(2)]
        public virtual int Apr331Widerstand
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(true)]
        public virtual bool PrintHeaderOnOrderResume
        {
            get { return (bool)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(true)]
        public virtual bool PlaySoundOutOfRange
        {
            get { return (bool)GetLocal(); }
            set { SetLocal(value); }
        }

        private IImmutableIndexable<String> _kontrastItems;
        public IImmutableIndexable<String> KontrastItems
        {
            get { return _kontrastItems; }
            set
            {
                if (!Equals(_kontrastItems, value))
                {
                    _kontrastItems = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private IImmutableIndexable<int> _widerstandItems;
        public IImmutableIndexable<int> WiderstandItems
        {
            get { return _widerstandItems; }
            set
            {
                if (!Equals(_widerstandItems, value))
                {
                    _widerstandItems = value;
                    NotifyPropertyChanged();

                }
            }
        }

        public static readonly IImmutableIndexable<string> Apr331KonstrastItemStrings = Indexable.ImmutableValues(
            "K0", "K1", "K2", "K3", "K4", "K5", "K6", "K7", "K8"
        );

        public static readonly IImmutableIndexable<int> Apr331WiderstandItemStrings = Indexable.ImmutableValues(
            0, 1, 2, 3, 4
        );
    }
}
