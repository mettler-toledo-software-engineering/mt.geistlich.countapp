﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.geistlich.countapp.application.Services.SystemUpdate;
using MT.Singularity.Collections;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Platform.Infrastructure;

namespace mt.geistlich.countapp.ymlui.Configuration
{
    [Export(typeof(CustomerGroupSetupMenuItem))]
    public class GeistlichSetupNode : CustomerGroupSetupMenuItem
    {
        private readonly ISystemUpdateService _systemUpdate;
        private readonly IPlatformEngine _engine;
        public GeistlichSetupNode(SetupMenuContext context, ISystemUpdateService systemUpdate, IPlatformEngine engine) : base(context,
            new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.GeistlichMainNode))
        {
            _systemUpdate = systemUpdate;
            _engine = engine;
        }
        public override async Task ShowChildrenAsync()
        {
            try
            {
                var geistlichComponent = _context.CompositionContainer.Resolve<IGeistlichComponents>();
                GeistlichConfiguration geistlichConfiguration = await geistlichComponent.GetConfigurationToChangeAsync();
                Children =
                    Indexable.ImmutableValues<SetupMenuItem>(
                        new GeneralSettingsSubNode(_context, geistlichComponent, geistlichConfiguration, _systemUpdate, _engine),
                        new PrinterSubNode(_context, geistlichComponent, geistlichConfiguration),
                        new SoundSetupNode(_context, geistlichComponent, geistlichConfiguration));

            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error BalancerSetupnode.ShowChildrenAsync", ex);
            }
        }

    }
}
